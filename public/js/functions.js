function Contatenar(dataArray,espacio)
{
    var string='';
    $.each(dataArray,function(index,value)
    {
        string+=(value   ==  null?'':value).toString()+(espacio?' ':'');
    })
    return string;
}
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function tableServerSide()
{
    $.fn.dataTableServerSide = function(options) 
    {
        var columns=[];
        var table =$(this).DataTable(); 
        table.destroy();
        table = $(this).DataTable({
            language:laguajedataTable(),
            processing: true,
            serverSide: true,
            ordering:   false,
            ajax:       options.url,
            columns:    options.columns,
        });

    }
}
function laguajedataTable()
{
    return {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    };
}
function table()
{
    $('#table').DataTable({language:laguajedataTable()});
}
function time()
{
    try 
    {
        $('.time').clockpicker({
            donetext: 'Guardar',
            placement: 'bottom',
            align: 'left',
            autoclose: true,
            twelvehour: true,
            vibrate: true
        });
    }
    catch (error) 
    {
            
    }
}
$(function()
{
    time();
    $('#'+$("form").attr('id')).validate();
    tableServerSide();
    table();
    $('.fecha').datepicker({ 
        dateFormat: 'yy-mm-dd' 
    });
})