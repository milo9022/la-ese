<?php

use Illuminate\Http\Request;

Route::get('edad','dataController@editEdad');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('captchakey', function(){return App\Http\Controllers\TblConfigController::GetData('captcha-public');});
Route::get('fechaActual',function(){return 
[
    'validate'=>true,
    'fecha'=>(((int)date('H'))>12?date('Y-m-d',strtotime ( '+2 day' , strtotime ( date('Y/m/d') ) )):date('Y-m-d',strtotime ( '+1 day' , strtotime ( date('Y/m/d') ) ))),
    'hora'=>date("H:i:s A")
];});
Route::post('citacodigo','TblCitasController@DatosporCodigo');
Route::post('citaCancelarUsuario','TblCitasController@citaCancelarUsuario');

Route::get('documentoTipo','dataController@documentoTipo');

Route::post('codigo','TblCitasController@crearCitaCodigo');
Route::get('citas/diasnohabiles','TblClientesController@DiasNoHabiles');
Route::get('paciente/get','TblClientesController@validarPaciente');
Route::post('validardocumento','TblClientesController@ValidarDocumento');
Route::get('mesaje/{name}','TblConfigController@Get');
Route::get('puntosatencion/all','TblConsultasController@puntosAtencion');
Route::post('municipios',        'dataController@municipios');
Route::post('Recapt','dataController@ValidateReCaptcha');
Route::group(['prefix' => 'programas'], function () 
{
    Route::post('CitasMaximas','TblConsultasController@fechasCitasMaximas');
    Route::post('puntoatencion','TblConsultasController@ConsultasXPuntos');
});
