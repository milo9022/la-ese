<?php
Auth::routes();

Route::get('info', function(){phpinfo();});
Route::get('/', 'DesabilitarController@paginaindex');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('role:admin');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/exit', 'UsersController@Cerrar_Logout')->name('exit');
Route::get('/fecha/{fecha}', 'dataController@textoFecha');
Route::get('/edad/{fecha}', 'dataController@edad');
Route::group(['prefix' => 'configuracion','middleware' => 'auth'], function () {
     Route::get('/enviaremail', 'HomeController@enviarEmailView');
     Route::post('/enviaremailSave', 'HomeController@enviarEmailSave');
});
Route::group(['prefix' => 'format','middleware' => 'auth'], function () {
     Route::post('sms','TblCitasController@formatTextSms');
     Route::post('email','TblCitasController@formatTextEmail');
});
Route::group(['prefix' => 'carga','middleware' => 'auth'], function () {
     Route::resource('', 'cargaPacientesController')->middleware('role:admin');
     Route::get('idload', 'cargaPacientesController@createIdCarga')->middleware('role:admin');
     Route::post('status', 'cargaPacientesController@estado')->middleware('role:admin');
     
});
Route::group(['prefix' => 'limpiar','middleware' => 'auth'], function() {
     Route::get('/',function(){
          Artisan::call('cache:clear');
          Artisan::call('config:cache');
          Artisan::call('config:clear');
          Artisan::call('view:clear');
          Artisan::call('route:clear');
          Artisan::call('clear-compiled');
          return response()->json(['validate'=>true,'msj'=>'clear success']);
     })->middleware('role:admin');
});
Route::group(['prefix' => 'citas','middleware' => 'auth'], function () {
     Route::post('detalle','TblCitasController@detalleCitas');
     Route::get('especialistas', 'TblCitasController@searchespecialistas')->name('tbl_citas.tbl_citas.especialistas');
     Route::get('/', 'TblCitasController@index')->name('tbl_citas.tbl_citas.index');//->middleware('role:admin,facturador');
     Route::post('/buscar','TblCitasController@show2')->name('tbl_citas.tbl_citas.buscar');
     Route::get('/buscar',function(){return redirect('citas/');});
     Route::get('All/{filtro}', 'TblCitasController@citasAll');
     Route::get('/create','TblCitasController@create')->name('tbl_citas.tbl_citas.create');
     Route::post('/cancelar/{id}','TblCitasController@CancelarCita')->name('tbl_citas.tbl_citas.cancelar')->where('id', '[0-9]+');
     Route::post('/asignar/{id}','TblCitasController@saveAsignacion')->name('tbl_citas.tbl_citas.edit')->where('id', '[0-9]+');
     Route::get('/asignar/{tblCita}','TblCitasController@show')->name('tbl_citas.tbl_citas.show')->where('id', '[0-9]+');
     Route::get('/{tblCita}/edit','TblCitasController@edit')->name('tbl_citas.tbl_citas.edit')->where('id', '[0-9]+');
     Route::post('/', 'TblCitasController@store')->name('tbl_citas.tbl_citas.store');
     Route::put('tbl_cita/{tblCita}', 'TblCitasController@update')->name('tbl_citas.tbl_citas.update')->where('id', '[0-9]+');
     Route::delete('/tbl_cita/{tblCita}','TblCitasController@destroy')->name('tbl_citas.tbl_citas.destroy')->where('id', '[0-9]+');
});
Route::group(['prefix' => 'puntos_atencion','middleware' => 'auth'], function () {
    Route::get('/', 'TblPuntosAtencionesController@index')
         ->name('tbl_puntos_atenciones.tbl_puntos_atencione.index')
         ->middleware('role:admin');
    Route::get('/create','TblPuntosAtencionesController@create')
         ->name('tbl_puntos_atenciones.tbl_puntos_atencione.create')
         ->middleware('role:admin');
    Route::get('/show/{tblPuntosAtencione}','TblPuntosAtencionesController@show')
         ->name('tbl_puntos_atenciones.tbl_puntos_atencione.show')->where('id', '[0-9]+')
         ->middleware('role:admin');
    Route::get('/{tblPuntosAtencione}/edit','TblPuntosAtencionesController@edit')
         ->name('tbl_puntos_atenciones.tbl_puntos_atencione.edit')->where('id', '[0-9]+')
         ->middleware('role:admin');
    Route::post('/', 'TblPuntosAtencionesController@store')
         ->name('tbl_puntos_atenciones.tbl_puntos_atencione.store')
         ->middleware('role:admin');
    Route::put('tbl_puntos_atencione/{tblPuntosAtencione}', 'TblPuntosAtencionesController@update')
         ->name('tbl_puntos_atenciones.tbl_puntos_atencione.update')->where('id', '[0-9]+')
         ->middleware('role:admin');
    Route::delete('/tbl_puntos_atencione/{tblPuntosAtencione}','TblPuntosAtencionesController@destroy')
         ->name('tbl_puntos_atenciones.tbl_puntos_atencione.destroy')->where('id', '[0-9]+')
         ->middleware('role:admin');
});
Route::group(['prefix' => 'informes','middleware' => 'auth'], function () {
     Route::get('/puntos', 'InformesController@informesPuntoAtencion')
     ->name('informepuntos')
     ->middleware('role:admin,supervision');
     Route::post('Verpuntos','InformesController@DwinformesPuntoAtencion');
});
Route::group(['prefix' => 'programas','middleware' => 'auth'], function () {
     Route::get('/', 'TblConsultasController@index')
          ->name('tbl_consultas.tbl_consultas.index')
          ->middleware('role:admin,supervision');
     Route::get('/create','TblConsultasController@create')
          ->name('tbl_consultas.tbl_consultas.create')
          ->middleware('role:admin,facturador');
     Route::get('/show/{tblConsultas}','TblConsultasController@show')
          ->name('tbl_consultas.tbl_consultas.show')->where('id', '[0-9]+')
          ->middleware('role:admin,facturador');
     Route::get('/{tblConsultas}/edit','TblConsultasController@edit')
          ->name('tbl_consultas.tbl_consultas.edit')->where('id', '[0-9]+')
          ->middleware('role:admin,facturador');
     Route::post('/', 'TblConsultasController@store')
          ->name('tbl_consultas.tbl_consultas.store')
          ->middleware('role:admin,facturador');
     Route::put('tbl_consultas/{tblConsultas}', 'TblConsultasController@update')
          ->name('tbl_consultas.tbl_consultas.update')->where('id', '[0-9]+')
          ->middleware('role:admin,facturador');
     Route::delete('/tbl_consultas/{tblConsultas}','TblConsultasController@destroy')
          ->name('tbl_consultas.tbl_consultas.destroy')->where('id', '[0-9]+')
          ->middleware('role:admin,facturador');
});
Route::group(['prefix' => 'clientes','middleware' => 'auth'], function () {
     Route::get('find','TblClientesController@search');
     Route::get('/', 'TblClientesController@index')
         ->name('tbl_clientes.tbl_clientes.index')
         ->middleware('role:admin,facturador,supervision');
     Route::get('All', 'TblClientesController@clientesAll');
     Route::get('/create','TblClientesController@create')
         ->name('tbl_clientes.tbl_clientes.create')
         ->middleware('role:admin,facturador,supervision');
     Route::get('/show/{tblClientes}','TblClientesController@show')
         ->name('tbl_clientes.tbl_clientes.show')->where('id', '[0-9]+')
         ->middleware('role:admin,facturador');
     Route::get('/{tblClientes}/edit','TblClientesController@edit')
         ->name('tbl_clientes.tbl_clientes.edit')->where('id', '[0-9]+')
         ->middleware('role:admin,facturador,supervision');
     Route::post('/', 'TblClientesController@store')
         ->name('tbl_clientes.tbl_clientes.store')
         ->middleware('role:admin,facturador,supervision');
     Route::put('tbl_clientes/{tblClientes}', 'TblClientesController@update')
         ->name('tbl_clientes.tbl_clientes.update')->where('id', '[0-9]+')
         ->middleware('role:admin,facturador,supervision');
     Route::delete('/tbl_clientes/{tblClientes}','TblClientesController@destroy')
         ->name('tbl_clientes.tbl_clientes.destroy')->where('id', '[0-9]+')
         ->middleware('role:admin,facturador');
});
Route::group(['prefix' => 'tbl_consultas_tipos','middleware' => 'auth'], function () {
     Route::get('/', 'TblConsultasTiposController@index')
          ->name('tbl_consultas_tipos.tbl_consultas_tipos.index')
          ->middleware('role:admin,facturador');
     Route::get('/create','TblConsultasTiposController@create')
          ->name('tbl_consultas_tipos.tbl_consultas_tipos.create')
          ->middleware('role:admin');
     Route::get('/show/{tblConsultasTipos}','TblConsultasTiposController@show')
          ->name('tbl_consultas_tipos.tbl_consultas_tipos.show')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::get('/{tblConsultasTipos}/edit','TblConsultasTiposController@edit')
          ->name('tbl_consultas_tipos.tbl_consultas_tipos.edit')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::post('/', 'TblConsultasTiposController@store')
          ->name('tbl_consultas_tipos.tbl_consultas_tipos.store')
          ->middleware('role:admin');
     Route::put('tbl_consultas_tipos/{tblConsultasTipos}', 'TblConsultasTiposController@update')
          ->name('tbl_consultas_tipos.tbl_consultas_tipos.update')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::delete('/tbl_consultas_tipos/{tblConsultasTipos}','TblConsultasTiposController@destroy')
          ->name('tbl_consultas_tipos.tbl_consultas_tipos.destroy')->where('id', '[0-9]+')
          ->middleware('role:admin');
});
Route::group(['prefix' => 'mensajes','middleware' => 'auth'], function () {
     Route::get('/', 'TblMensajesController@index')
          ->name('tbl_mensajes.tbl_mensajes.index')
          ->middleware('role:admin,supervision');
     Route::get('/show/{tblMensajes}','TblMensajesController@show')
          ->name('tbl_mensajes.tbl_mensajes.show')->where('id', '[0-9]+');
});
Route::group(['prefix' => 'tbl_citas_estados','middleware' => 'auth'], function () {
     Route::get('/', 'TblCitasEstadosController@index')
          ->name('tbl_citas_estados.tbl_citas_estados.index')
          ->middleware('role:admin,facturador');
     Route::get('/create','TblCitasEstadosController@create')
          ->name('tbl_citas_estados.tbl_citas_estados.create')
          ->middleware('role:admin');
     Route::get('/show/{tblCitasEstados}','TblCitasEstadosController@show')
          ->name('tbl_citas_estados.tbl_citas_estados.show')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::get('/{tblCitasEstados}/edit','TblCitasEstadosController@edit')
          ->name('tbl_citas_estados.tbl_citas_estados.edit')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::post('/', 'TblCitasEstadosController@store')
          ->name('tbl_citas_estados.tbl_citas_estados.store')
          ->middleware('role:admin');
     Route::put('tbl_citas_estados/{tblCitasEstados}', 'TblCitasEstadosController@update')
          ->name('tbl_citas_estados.tbl_citas_estados.update')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::delete('/tbl_citas_estados/{tblCitasEstados}','TblCitasEstadosController@destroy')
          ->name('tbl_citas_estados.tbl_citas_estados.destroy')->where('id', '[0-9]+')
          ->middleware('role:admin');
});
Route::group(['prefix' => 'usuarios','middleware' => 'auth'], function () {
     Route::get('/', 'UsersController@index')
          ->name('users.users.index')
          ->middleware('role:admin');
     Route::get('/cambiarpass', 'UsersController@changePass')
          ->name('users.users.change');
     Route::get('/create','UsersController@create')
          ->name('users.users.create');
     Route::get('/show/{users}','UsersController@show')
          ->name('users.users.show');
     Route::get('/{users}/edit','UsersController@edit')
          ->name('users.users.edit');
     Route::post('/', 'UsersController@store')
          ->name('users.users.store');
     Route::put('users/{users}', 'UsersController@update')
          ->name('users.users.update');
     Route::post('/changePass/{users}', 'UsersController@NewPass')
          ->name('users.users.changePass');
     Route::delete('/users/{users}','UsersController@destroy') 
          ->name('users.users.destroy')
          ->middleware('role:admin');
     Route::post('/restablecercontrasenna','UsersController@reload')
          ->name('users.users.reload');
});
Route::group(['prefix' => 'especialistas',], function () {
     Route::get('/', 'TblEspecialistasController@index')
          ->name('tbl_especialistas.tbl_especialistas.index')
          ->middleware('role:admin');
     Route::get('/create','TblEspecialistasController@create')
          ->name('tbl_especialistas.tbl_especialistas.create')
          ->middleware('role:admin');
     Route::get('/show/{tblEspecialistas}','TblEspecialistasController@show')
          ->name('tbl_especialistas.tbl_especialistas.show')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::get('/{tblEspecialistas}/edit','TblEspecialistasController@edit')
          ->name('tbl_especialistas.tbl_especialistas.edit')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::post('/', 'TblEspecialistasController@store')
          ->name('tbl_especialistas.tbl_especialistas.store')
          ->middleware('role:admin');
     Route::put('tbl_especialistas/{tblEspecialistas}', 'TblEspecialistasController@update')
          ->name('tbl_especialistas.tbl_especialistas.update')->where('id', '[0-9]+')
          ->middleware('role:admin');
     Route::delete('/tbl_especialistas/{tblEspecialistas}','TblEspecialistasController@destroy')
          ->name('tbl_especialistas.tbl_especialistas.destroy')->where('id', '[0-9]+')
          ->middleware('role:admin');
});
Route::group(['prefix' => 'diasnohabiles'], function (){
     Route::get('/', 'TblDiasNoHabilesController@index')
          ->name('tbl_dias_no_habiles.tbl_dias_no_habile.index')
          ->middleware('role:admin');
     Route::get('/create','TblDiasNoHabilesController@create')
          ->name('tbl_dias_no_habiles.tbl_dias_no_habile.create')
          ->middleware('role:admin');
     Route::get('/show/{tblDiasNoHabile}','TblDiasNoHabilesController@show')
          ->name('tbl_dias_no_habiles.tbl_dias_no_habile.show')
          ->middleware('role:admin');
     Route::get('/{tblDiasNoHabile}/edit','TblDiasNoHabilesController@edit')
          ->name('tbl_dias_no_habiles.tbl_dias_no_habile.edit')
          ->middleware('role:admin');
     Route::post('/', 'TblDiasNoHabilesController@store')
          ->name('tbl_dias_no_habiles.tbl_dias_no_habile.store')
          ->middleware('role:admin');
     Route::put('tbl_dias_no_habile/{tblDiasNoHabile}', 'TblDiasNoHabilesController@update')
          ->name('tbl_dias_no_habiles.tbl_dias_no_habile.update')
          ->middleware('role:admin');
     Route::delete('/tbl_dias_no_habile/{tblDiasNoHabile}','TblDiasNoHabilesController@destroy')
          ->name('tbl_dias_no_habiles.tbl_dias_no_habile.destroy')
          ->middleware('role:admin');
});
Route::group(['prefix' => 'desabilitar',], function () {
     Route::get('/', 'DesabilitarController@index')
          ->name('DesabilitarController.DesabilitarController.index')
          ->middleware('role:admin');
     Route::post('/save', 'DesabilitarController@store')
          ->name('DesabilitarController.DesabilitarController.store')
          ->middleware('role:admin');
});