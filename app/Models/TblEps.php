<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblEps extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_eps';
    protected $primaryKey = 'id';
    protected $fillable = [
        'descripcion'
    ];
}
