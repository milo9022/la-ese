<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblConsultorios extends Model
{
    protected $table = 'tbl_consultorios';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $dates = [];
    public function TblCitas()
    {
        return $this->hasOne('App\Models\TblCitas','id_consultorios','id');
    }
}
