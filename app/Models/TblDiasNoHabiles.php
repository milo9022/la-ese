<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblDiasNoHabiles extends Model
{
    protected $table = 'tbl_dias_no_habiles';
    protected $primaryKey = 'id';
    protected $fillable = ['fecha'];
}
