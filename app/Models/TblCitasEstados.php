<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblCitasEstados extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_citas_estados';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre',
                  'orden',
                  'color'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tblCita for this model.
     *
     * @return App\Models\TblCitum
     */
    public function tblCita()
    {
        return $this->hasOne('App\Models\TblCitum','id_citas_estado','id');
    }



}
