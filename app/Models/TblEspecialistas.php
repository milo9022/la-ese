<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblEspecialistas extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_especialistas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre_primero',
        'nombre_segundo',
        'apellido_primero',
        'apellido_segundo',
        'documento',
        'fecha_nacimiento',
        'id_documento_tipo',
        'id_eps',
        'celular1',
        'celular2',
        'email',
        'id_especialidad'
    ];
    public function TblDocumentoTipo()
    {
        return $this->belongsTo('App\Models\TblDocumentoTipos','id_documento_tipo','id');
    }
    public function TblEps()
    {
        return $this->belongsTo('App\Models\TblEps','id_eps','id');
    }
    public function tblespecialidad()
    {
        return $this->belongsTo('App\Models\TblEspecialidad','id_especialidad','id');
    }
}
