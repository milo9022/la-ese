<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class crons extends Model
{
    protected $table = 'crons';
    protected $primaryKey = 'id';
    protected $dates = [];
    protected $casts = [];
    protected $fillable = [
        'sql',
        'status',
        'error',
        'res'
    ];
}
