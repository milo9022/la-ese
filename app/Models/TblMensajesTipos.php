<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblMensajesTipos extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_mensajes_tipos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre',
                  'maximo',
                  'html'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tblMensaje for this model.
     *
     * @return App\Models\TblMensaje
     */
    public function tblMensaje()
    {
        return $this->hasOne('App\Models\TblMensaje','id_mensaje_tipo','id');
    }



}
