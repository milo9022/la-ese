<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblEspecialidad extends Model
{
    protected $table = 'tbl_especialidad';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre'
    ];
}
