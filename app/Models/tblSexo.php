<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tblSexo extends Model
{
    protected $table = 'tbl_sexo';
    protected $fillable = ['nombre','nombre_corto'];
}
