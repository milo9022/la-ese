<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Users extends Model
{
    use SoftDeletes;
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = [
                  'nombre_primero',
                  'nombre_segundo',
                  'apellido_primero',
                  'apellido_segundo',
                  'documento',
                  'id_punto_atencion',
                  'activo',
                  'email',
                  'email_verified_at',
                  'password',
                  'remember_token'
              ];
    protected $dates = ['deleted_at'];
    protected $casts = [];
    public function TblPuntosAtenciones()
    {
        return $this->belongsTo('App\Models\TblPuntosAtenciones','id_punto_atencion','id');
    }
    public function roles()
    {
        return $this->belongsToMany('App\Role')->withTimestamps();
    }
    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles))
        {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles))
        {
            foreach ($roles as $role)
            {
                if ($this->hasRole($role))
                {
                    return true;
                }
            }
        } 
        else
        {
            if ($this->hasRole($roles))
            {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first())
        {
            return true;
        }
        return false;
    }
    
}
