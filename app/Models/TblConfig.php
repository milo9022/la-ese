<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblConfig extends Model
{
    protected $table = 'tbl_config';
    protected $primaryKey = 'id';
    protected $fillable = [
                  'name',
                  'value'
              ];
}
