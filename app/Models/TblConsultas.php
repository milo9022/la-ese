<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblConsultas extends Model
{
    protected $table = 'tbl_consultas';
    protected $primaryKey = 'id';
    protected $fillable = [
                  'nombre',
                  'id_consulta_tipo'
              ];
    public function TblConsultasTipo()
    {
        return $this->belongsTo('App\Models\TblConsultasTipos','id_consulta_tipo','id');
    }
    public function tblCita()
    {
        return $this->hasOne('App\Models\TblCitas','id_consulta','id');
    }
    public function tblPuntosAtencionesXConsuta()
    {
        return $this->hasOne('App\Models\TblPuntosAtencionesXConsutas','id_consultas','id');
    }
}
