<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblProfesionalAsistencial extends Model
{
    protected $table = 'tbl_profesional_asistencial';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre_primero',
        'nombre_segundo',
        'apellido_primero',
        'apellido_segundo',
        'fecha_nacimiento',
        'id_documento_tipo',
        'id_eps',
        'documento',
        'celular1',
        'celular2',
        'email'
    ];
}
