<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPuntosAtencionesXConsutas extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_puntos_atenciones_x_consutas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'id_consultas',
                  'id_punto_atencion',
                  'maximocitas'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the TblConsulta for this model.
     *
     * @return App\Models\TblConsulta
     */
    public function TblConsulta()
    {
        return $this->belongsTo('App\Models\TblConsulta','id_consultas','id');
    }

    public function TblPuntosAtenciones()
    {
        return $this->belongsTo('App\Models\TblPuntosAtenciones','id_punto_atencion','id');
    }



}
