<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblCitas extends Model
{
    protected $table = 'tbl_citas';
    protected $primaryKey = 'id';
    protected $fillable = [
                  'fecha',
                  'hora',
                  'id_punto_atencion',
                  'id_consulta',
                  'id_cliente',
                  'id_citas_estado',
                  'id_facturacion_tipo',
                  'id_profesional_asistencial',
                  'id_consultorios',
                  'codigo',
                  'motivoconsulta',
                  'id_usuario'
                ];
    protected $hidden = ['updated_at'];
    protected $dates = [];
    protected $casts = [];
    public function TblPuntosAtenciones()
    {
        return $this->belongsTo('App\Models\TblPuntosAtenciones','id_punto_atencion','id');
    }
    public function TblFacturacionTipo()
    {
        return $this->belongsTo('App\Models\TblFacturacionTipo','id_facturacion_tipo','id');
    }
    public function TblConsultas()
    {
        return $this->belongsTo('App\Models\TblConsultas','id_consulta','id');
    }
    public function TblCliente()
    {
        return $this->belongsTo('App\Models\TblClientes','id_cliente','id');
    }
    public function TblCitasEstado()
    {
        return $this->belongsTo('App\Models\TblCitasEstados','id_citas_estado','id');
    }
    public function TblConsultorios()
    {
        return $this->belongsTo('App\Models\TblConsultorios','id_consultorios','id');
    }
    public function TblEspecialistas()
    {
        return $this->belongsTo('App\Models\TblEspecialistas','id_especialista','id');
    }
    public function TblMensajes()
    {
        return $this->belongsTo('App\Models\TblMensajes','id_cita','id');
    }
    public function Tblusuario()
    {
        return $this->belongsTo('App\Models\Users','id_usuario','id');
    }
    
    
}
