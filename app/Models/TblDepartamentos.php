<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblDepartamentos extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_departamentos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tblMunicipio for this model.
     *
     * @return App\Models\TblMunicipio
     */
    public function tblMunicipio()
    {
        return $this->hasOne('App\Models\TblMunicipio','id_departamento','id');
    }



}
