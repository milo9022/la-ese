<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblClientes extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_clientes';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nombre_primero',
        'nombre_segundo',
        'apellido_primero',
        'apellido_segundo',
        'fecha_nacimiento',
        'id_documento_tipo',
        'id_eps',
        'documento',
        'celular1',
        'celular2',
        'email'
    ];
    public function TblDocumentoTipo()
    {
        return $this->belongsTo('App\Models\TblDocumentoTipos','id_documento_tipo','id');
    }
    public function TblEps()
    {
        return $this->belongsTo('App\Models\TblEps','id_eps','id');
    }
    public function TblDocumentoTipos()
    {
        return $this->belongsTo('App\Models\TblDocumentoTipos','id_documento_tipo','id');
    }
    public function tblCita()
    {
        return $this->hasOne('App\Models\TblCitas','id_cliente','id');
    }
}
