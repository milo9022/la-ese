<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblFacturacionTipo extends Model
{
    protected $table = 'tbl_facturacion_tipo';
    protected $primaryKey = 'id';
    protected $fillable = [
                  'nombre'
              ];
}
