<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblMensajes extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_mensajes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                'id_cliente',
                'destino',
                'mensaje',
                'id_mensaje_tipo',
                'created_at',
                'updated_at',
                'id_usuario',
                'id_cita',
                'respuesta'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the TblMensajesTipo for this model.
     *
     * @return App\Models\TblMensajesTipo
     */
    public function TblMensajesTipo()
    {
        return $this->belongsTo('App\Models\TblMensajesTipos','id_mensaje_tipo','id');
    }

    public function TblCitas()
    {
        return $this->belongsTo('App\Models\TblCitas','id_cita','id');
    }
    public function Users()
    {
        return $this->belongsTo('App\Models\Users','id_usuario','id');
    }
    public function TblClientes()
    {
        return $this->belongsTo('App\Models\TblClientes','id_cliente','id');
    }

}
