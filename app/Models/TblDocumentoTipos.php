<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblDocumentoTipos extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_documento_tipos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre',
                  'nombre_corto'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the tblCliente for this model.
     *
     * @return App\Models\TblCliente
     */
    public function tblCliente()
    {
        return $this->hasOne('App\Models\TblCliente','id_documento_tipo','id');
    }



}
