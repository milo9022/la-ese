<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblMunicipios extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    //public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbl_municipios';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'nombre',
                  'id_departamento'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the TblDepartamento for this model.
     *
     * @return App\Models\TblDepartamento
     */
    public function TblDepartamento()
    {
        return $this->belongsTo('App\Models\TblDepartamento','id_departamento','id');
    }

    /**
     * Get the tblPuntosAtencione for this model.
     *
     * @return App\Models\TblPuntosAtencione
     */
    public function tblPuntosAtencione()
    {
        return $this->hasOne('App\Models\TblPuntosAtencione','id_municipio','id');
    }



}
