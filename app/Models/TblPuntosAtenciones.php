<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TblPuntosAtenciones extends Model
{
    
    protected $table = 'tbl_puntos_atenciones';

    protected $primaryKey = 'id';

    protected $hidden = ['created_at', 'updated_at'];
    
    protected $fillable = [
                  'nombre',
                  'id_municipio'
              ];

    protected $dates = [];
    
    protected $casts = [];
    
    public function TblMunicipio()
    {
        return $this->belongsTo('App\Models\TblMunicipios','id_municipio','id');
    }
    public function tblCita()
    {
        return $this->hasOne('App\Models\TblCitas','id_punto_atencion','id');
    }
    public function tblPuntosAtencionesXConsuta()
    {
        return $this->hasOne('App\Models\TblPuntosAtencionesXConsutas','id_punto_atencion','id');
    }
}
