<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblConsultasTipos;
use Illuminate\Http\Request;
use Exception;

class TblConsultasTiposController extends Controller
{

    /**
     * Display a listing of the tbl consultas tipos.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $tblConsultasTiposObjects = TblConsultasTipos::paginate(0);

        return view('tbl_consultas_tipos.index', compact('tblConsultasTiposObjects'));
    }

    /**
     * Show the form for creating a new tbl consultas tipos.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('tbl_consultas_tipos.create');
    }

    /**
     * Store a new tbl consultas tipos in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            TblConsultasTipos::create($data);

            return redirect()->route('tbl_consultas_tipos.tbl_consultas_tipos.index')
                ->with('success_message', 'Tbl Consultas Tipos was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified tbl consultas tipos.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblConsultasTipos = TblConsultasTipos::findOrFail($id);

        return view('tbl_consultas_tipos.show', compact('tblConsultasTipos'));
    }

    /**
     * Show the form for editing the specified tbl consultas tipos.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblConsultasTipos = TblConsultasTipos::findOrFail($id);
        

        return view('tbl_consultas_tipos.edit', compact('tblConsultasTipos'));
    }

    /**
     * Update the specified tbl consultas tipos in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblConsultasTipos = TblConsultasTipos::findOrFail($id);
            $tblConsultasTipos->update($data);

            return redirect()->route('tbl_consultas_tipos.tbl_consultas_tipos.index')
                ->with('success_message', 'Tbl Consultas Tipos ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl consultas tipos from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblConsultasTipos = TblConsultasTipos::findOrFail($id);
            $tblConsultasTipos->delete();

            return redirect()->route('tbl_consultas_tipos.tbl_consultas_tipos.index')
                ->with('success_message', 'Tbl Consultas Tipos was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'nullable|string|min:0|max:20', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
