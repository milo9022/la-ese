<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\TblConfigController;
use DB;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function enviarEmailView()
    {
        $data = TblConfigController::GetData('mensaje-email');
        $data2 = TblConfigController::GetData('mensaje-email-cancel');
        return view('enviarEmail',compact('data','data2'));
    }
    public function enviarEmailSave(Request $request)
    {
        TblConfigController::SetData('mensaje-email',$request->mensaje);
        TblConfigController::SetData('mensaje-email-cancel',$request->mensaje2);
        return redirect('configuracion/enviaremail');
    }
    
    public function index()
    {
        return redirect()->route('tbl_citas.tbl_citas.index');
    }
}
