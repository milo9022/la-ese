<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblConsultas;
use App\Models\TblConsultasTipos;
use App\Models\TblPuntosAtencionesXConsutas;
use App\Models\TblPuntosAtenciones;
use App\Models\TblCitas;
use App\Models\TblDiasNoHabile;

use DB;
use Illuminate\Http\Request;
use Exception;

class TblConsultasController extends Controller
{

    /**
     * Display a listing of the tbl consultas.
     *
     * @return Illuminate\View\View
     */
    public function fechasCitasMaximas(Request $request)
    {
        $dataFechas=[];
        $res =TblPuntosAtencionesXConsutas::
        where('id_consultas','=',$request->id_programa)->
        where('id_punto_atencion','=', $request->id_punto_atencion)->
        select('maximocitas')->
        first();

        $data=TblCitas::
        select('fecha',DB::raw('count(*) as total'))
        ->where(DB::raw('date(tbl_citas.fecha)'), '>', DB::raw('date(now())')) 
        ->where('tbl_citas.id_punto_atencion','=', $request->id_punto_atencion)  
        ->where('tbl_citas.id_consulta','=', $request->id_programa)
        ->where('tbl_citas.id_citas_estado','!=', 3)
        ->where('tbl_citas.id_citas_estado','!=', 4)

        ->groupBy('tbl_citas.fecha')
        ->get();
        $DiasNoHabiles=TblDiasNoHabile::whereDate('fecha','>',date('Y-m-d'))->get();
        foreach($data as $temp)
        {
            if($temp->total>=$res->maximocitas)
            {
                $dataFechas[]=$temp;
            }
        }
        foreach($DiasNoHabiles as $temp)
        {
            $temp2=array();
            $temp2['fecha'] = $temp->fecha;
            $temp2['total'] = -1;
            $dataFechas[] = (object)$temp2;
        }
        return ['validate'=>true,'fechas'=>$dataFechas];
    }
    public function all()
    {
        dd('Hola mundo');
        return TblConsultas::all();
    }
    public function ConsultasXPuntos(Request $request)
    {
        return TblPuntosAtencionesXConsutas::
        select(
            'tbl_consultas.nombre',
            'tbl_consultas.id_consulta_tipo',
            'tbl_puntos_atenciones.id_municipio',
            'tbl_puntos_atenciones_x_consutas.maximocitas',
            'tbl_consultas.id'
            )
        ->join('tbl_consultas','tbl_puntos_atenciones_x_consutas.id_consultas', '=','tbl_consultas.id')
        ->join('tbl_puntos_atenciones','tbl_puntos_atenciones_x_consutas.id_punto_atencion', '=','tbl_puntos_atenciones.id')
        ->where('id_punto_atencion','=',$request->id_punto_atencion)
        ->get();
    }
    public function puntosAtencion()
    {
        return TblPuntosAtenciones::orderBy('nombre')->get();
    }
    public function index()
    {
        $tblConsultasObjects = TblConsultas::with('tblconsultastipo')->paginate(0);

        return view('tbl_consultas.index', compact('tblConsultasObjects'));
    }

    /**
     * Show the form for creating a new tbl consultas.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $TblConsultasTipos = TblConsultasTipos::pluck('nombre','id')->all();
        return view('tbl_consultas.create', compact('TblConsultasTipos'));
    }

    /**
     * Store a new tbl consultas in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            TblConsultas::create($data);

            return redirect()->route('tbl_consultas.tbl_consultas.index')
                ->with('success_message', 'Tbl Consultas was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified tbl consultas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblConsultas = TblConsultas::with('tblconsultastipo')->findOrFail($id);

        return view('tbl_consultas.show', compact('tblConsultas'));
    }

    /**
     * Show the form for editing the specified tbl consultas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblConsultas = TblConsultas::findOrFail($id);
        $TblConsultasTipos = TblConsultasTipos::pluck('nombre','id')->all();

        return view('tbl_consultas.edit', compact('tblConsultas','TblConsultasTipos'));
    }

    /**
     * Update the specified tbl consultas in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblConsultas = TblConsultas::findOrFail($id);
            $tblConsultas->update($data);

            return redirect()->route('tbl_consultas.tbl_consultas.index')
                ->with('success_message', 'Tbl Consultas ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl consultas from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblConsultas = TblConsultas::findOrFail($id);
            $tblConsultas->delete();

            return redirect()->route('tbl_consultas.tbl_consultas.index')
                ->with('success_message', 'Tbl Consultas was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'required|string|min:1|max:200',
            'id_consulta_tipo' => 'required', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
