<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblCitasEstados;
use Illuminate\Http\Request;
use Exception;

class TblCitasEstadosController extends Controller
{

    /**
     * Display a listing of the tbl citas estados.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $tblCitasEstadosObjects = TblCitasEstados::paginate(0);

        return view('tbl_citas_estados.index', compact('tblCitasEstadosObjects'));
    }

    /**
     * Show the form for creating a new tbl citas estados.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('tbl_citas_estados.create');
    }

    /**
     * Store a new tbl citas estados in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            TblCitasEstados::create($data);

            return redirect()->route('tbl_citas_estados.tbl_citas_estados.index')
                ->with('success_message', 'Tbl Citas Estados was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified tbl citas estados.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblCitasEstados = TblCitasEstados::findOrFail($id);

        return view('tbl_citas_estados.show', compact('tblCitasEstados'));
    }

    /**
     * Show the form for editing the specified tbl citas estados.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblCitasEstados = TblCitasEstados::findOrFail($id);
        

        return view('tbl_citas_estados.edit', compact('tblCitasEstados'));
    }

    /**
     * Update the specified tbl citas estados in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblCitasEstados = TblCitasEstados::findOrFail($id);
            $tblCitasEstados->update($data);

            return redirect()->route('tbl_citas_estados.tbl_citas_estados.index')
                ->with('success_message', 'Tbl Citas Estados ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl citas estados from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblCitasEstados = TblCitasEstados::findOrFail($id);
            $tblCitasEstados->delete();

            return redirect()->route('tbl_citas_estados.tbl_citas_estados.index')
                ->with('success_message', 'Tbl Citas Estados was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'nullable|string|min:0|max:200',
            'orden' => 'nullable|numeric|min:-2147483648|max:2147483647',
            'color' => 'nullable|string|min:0|max:20', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
