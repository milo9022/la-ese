<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblMunicipios;
use App\Models\TblPuntosAtenciones;
use App\Models\TblPuntosAtencionesXConsutas;
use App\Models\TblConsultas;
use App\Models\TblDepartamentos;
use App\Models\TblCitas;
use Illuminate\Http\Request;
use Exception;
use DB;

class TblPuntosAtencionesController extends Controller
{

    /**
     * Display a listing of the tbl puntos atenciones.
     *
     * @return Illuminate\View\View
     */
    private function consultas($id_punto_atencion)
    {
        $TblConsultas =TblPuntosAtencionesXConsutas::
        select('tbl_consultas.nombre')
        ->join('tbl_consultas','tbl_puntos_atenciones_x_consutas.id_consultas', '=', 'tbl_consultas.id')
        ->where('tbl_puntos_atenciones_x_consutas.id_punto_atencion','=',$id_punto_atencion)
        ->get();
        $string='';
        $temp1=[];
        foreach($TblConsultas as $temp)
        {
            $temp1[]=$temp->nombre;
        }
        $string=implode(',',$temp1);
        return $string;
    }
    public function index()
    {
        $tblPuntosAtenciones = TblPuntosAtenciones::
        select(
            'tbl_puntos_atenciones.id',
            'tbl_puntos_atenciones.nombre',
            'tbl_municipios.nombre AS municipio_nombre'
        )
        ->leftJoin('tbl_municipios','tbl_puntos_atenciones.id_municipio', '=', 'tbl_municipios.id')
        ->paginate(0);
        foreach($tblPuntosAtenciones as $key => $temp)
        {
            $tblPuntosAtenciones[$key]->consulta_nombres=$this->consultas($temp->id);
        }
        return view('tbl_puntos_atenciones.index', compact('tblPuntosAtenciones'));
    }

    /**
     * Show the form for creating a new tbl puntos atencione.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $TblMunicipios = [];
        $tblPuntosAtencione=[];
        $TblDepartamentos = TblDepartamentos::pluck('nombre','id')->all();
        $tblConsultas  = TblConsultas::pluck('nombre','id')->all();
        return view('tbl_puntos_atenciones.create', compact('TblMunicipios','tblConsultas','TblDepartamentos','tblPuntosAtencione'));
    }

    /**
     * Store a new tbl puntos atencione in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            $id=TblPuntosAtenciones::create($data);
            $servicios=(array_keys ($request->servicios));
            foreach($servicios as $temp)
            {
                $res                    = new TblPuntosAtencionesXConsutas();
                $res->id_consultas      = $temp;
                $res->maximocitas       = $request->serviciosCantidad[$temp];
                $res->id_punto_atencion = $id->id;
                $res->save();
            }
            return redirect()->route('tbl_puntos_atenciones.tbl_puntos_atencione.index')
                ->with('success_message', 'Puntos Atencion ha sido creado correctamente.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => $exception->getMessage()]);
        }
    }

    /**
     * Display the specified tbl puntos atencione.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblPuntosAtencione = TblPuntosAtenciones::with('tblmunicipio')->findOrFail($id);

        return view('tbl_puntos_atenciones.show', compact('tblPuntosAtencione'));
    }

    /**
     * Show the form for editing the specified tbl puntos atencione.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {

        $tblPuntosAtencione = TblPuntosAtenciones::with('TblMunicipio')->findOrFail($id);
        $TblMunicipios = TblMunicipios::pluck('nombre','id')->all();
        $TblDepartamentos = TblDepartamentos::pluck('nombre','id')->all();
        $tblConsultas  = TblConsultas::pluck('nombre','id')->all();
        $TblPuntosAtencionesXConsuta = TblPuntosAtencionesXConsutas::where('id_punto_atencion','=',$id)->pluck('id_consultas','id_consultas','maximocitas')->all();
        $TblPuntosAtencionesXConsutaMaximo = TblPuntosAtencionesXConsutas::where('id_punto_atencion','=',$id)->pluck('maximocitas','id_consultas')->all();
        return view('tbl_puntos_atenciones.edit', compact('TblDepartamentos','tblPuntosAtencione','TblMunicipios','tblConsultas','TblPuntosAtencionesXConsuta','TblPuntosAtencionesXConsutaMaximo'));
    }

    /**
     * Update the specified tbl puntos atencione in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblPuntosAtencione = TblPuntosAtenciones::findOrFail($id);
            $tblPuntosAtencione->update($data);
            $deletedRows = TblPuntosAtencionesXConsutas::
            where('id_punto_atencion','=', $id)->
            delete();
            $servicios=$request->serviciosCantidad;
            foreach($servicios as $key=>$temp)
            {
                if(!is_null($temp))
                {

                    $res = new TblPuntosAtencionesXConsutas();
                    $res->id_consultas      = $key;
                    $res->id_punto_atencion = $id;
                    $res->maximocitas       = $temp;
                    $res->save();
                }
            }
            return redirect()->route('tbl_puntos_atenciones.tbl_puntos_atencione.index')
                ->with('success_message', 'Tbl Puntos Atencione ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request:'.$exception->getMessage()]);
        }
    }

    /**
     * Remove the specified tbl puntos atencione from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblPuntosAtencione = TblPuntosAtenciones::findOrFail($id);
            $tblPuntosAtencione->delete();

            return redirect()->route('tbl_puntos_atenciones.tbl_puntos_atencione.index')
                ->with('success_message', 'Tbl Puntos Atencione was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre' => 'required|string|min:1|max:200',
                'id_municipio' => 'required', 
        ];
        $data = $request->validate($rules);
        return $data;
    }

}
