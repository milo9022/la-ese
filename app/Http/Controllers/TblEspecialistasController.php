<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblDocumentoTipos;
use App\Models\TblEspecialidad;
use App\Models\TblEspecialistas;
use Illuminate\Http\Request;
use Exception;

class TblEspecialistasController extends Controller
{

    /**
     * Display a listing of the tbl especialistas.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $tblEspecialistasObjects = TblEspecialistas::with('TblDocumentoTipo','tblespecialidad')->get();
        return view('tbl_especialistas.index', compact('tblEspecialistasObjects'));
    }

    /**
     * Show the form for creating a new tbl especialistas.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $TblDocumentoTipos = TblDocumentoTipos::orderBy('nombre')->pluck('nombre','id')->all();
        $TblEspecialidads = TblEspecialidad::orderBy('nombre')->pluck('nombre','id');
        
        return view('tbl_especialistas.create', compact('TblDocumentoTipos','TblEspecialidads'));
    }

    /**
     * Store a new tbl especialistas in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            TblEspecialistas::create($data);

            return redirect()->route('tbl_especialistas.tbl_especialistas.index')
                ->with('success_message', 'Especialistas ha sido creado.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified tbl especialistas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblEspecialistas = TblEspecialistas::with('tbldocumentotipos','tblespecialidad')->findOrFail($id);

        return view('tbl_especialistas.show', compact('tblEspecialistas'));
    }

    /**
     * Show the form for editing the specified tbl especialistas.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblEspecialistas = TblEspecialistas::findOrFail($id);
        $TblDocumentoTipos = TblDocumentoTipos::orderBy('nombre')->pluck('nombre','id')->all();
        $TblEspecialidads = TblEspecialidad::orderBy('nombre')->pluck('nombre','id')->all();
        return view('tbl_especialistas.edit', compact('tblEspecialistas','TblDocumentoTipos','TblEspecialidads'));
    }

    /**
     * Update the specified tbl especialistas in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblEspecialistas = TblEspecialistas::findOrFail($id);
            $tblEspecialistas->update($data);

            return redirect()->route('tbl_especialistas.tbl_especialistas.index')
                ->with('success_message', 'Especialistas ha sido actualizado con éxito');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl especialistas from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblEspecialistas = TblEspecialistas::findOrFail($id);
            $tblEspecialistas->delete();

            return redirect()->route('tbl_especialistas.tbl_especialistas.index')
                ->with('success_message', 'Especialistas ha sido borrado.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre_primero' => 'required|string|min:1|max:191',
            'nombre_segundo' => 'nullable|string|min:0|max:191',
            'apellido_primero' => 'required|string|min:1|max:191',
            'apellido_segundo' => 'nullable|string|min:0|max:191',
            'fecha_nacimiento' => 'nullable|date_format:j/n/Y g:i A',
            'documento' => 'required|string|min:1|max:191',
            'id_documento_tipo' => 'nullable',
            'celular1' => 'nullable|string|min:0|max:191',
            'celular2' => 'nullable|string|min:0|max:191',
            'email' => 'nullable|string|min:0|max:191',
            'codigo' => 'nullable|string|min:0|max:191',
            'id_especialidad' => 'nullable', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
