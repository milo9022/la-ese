<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblDiasNoHabile;
use Illuminate\Http\Request;
use Exception;

class TblDiasNoHabilesController extends Controller
{

    /**
     * Display a listing of the tbl dias no habiles.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $tblDiasNoHabiles = TblDiasNoHabile::whereDate('fecha', '>=', date('Y-m-d'))->paginate(25);
        return view('tbl_dias_no_habiles.index', compact('tblDiasNoHabiles'));
    }

    /**
     * Show the form for creating a new tbl dias no habile.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('tbl_dias_no_habiles.create');
    }

    /**
     * Store a new tbl dias no habile in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            TblDiasNoHabile::create($data);

            return redirect()->route('tbl_dias_no_habiles.tbl_dias_no_habile.index')
                ->with('success_message', 'Dia No Habil ha sido creado con éxito.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request:'.$exception->getMessage()]);
        }
    }

    /**
     * Display the specified tbl dias no habile.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblDiasNoHabile = TblDiasNoHabile::findOrFail($id);

        return view('tbl_dias_no_habiles.show', compact('tblDiasNoHabile'));
    }

    /**
     * Show the form for editing the specified tbl dias no habile.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblDiasNoHabile = TblDiasNoHabile::findOrFail($id);
        

        return view('tbl_dias_no_habiles.edit', compact('tblDiasNoHabile'));
    }

    /**
     * Update the specified tbl dias no habile in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblDiasNoHabile = TblDiasNoHabile::findOrFail($id);
            $tblDiasNoHabile->update($data);

            return redirect()->route('tbl_dias_no_habiles.tbl_dias_no_habile.index')
                ->with('success_message', 'Dia No Habiles ha sido actualizado.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl dias no habile from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblDiasNoHabile = TblDiasNoHabile::findOrFail($id);
            $tblDiasNoHabile->delete();

            return redirect()->route('tbl_dias_no_habiles.tbl_dias_no_habile.index')
                ->with('success_message', 'Dias No Ha sido eliminado.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'fecha' => 'required', 
        ];
        
        $data = $request->validate($rules);


        return $data;
    }

}
