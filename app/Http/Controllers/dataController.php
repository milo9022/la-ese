<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblMunicipios;
use App\Models\TblDocumentoTipos;
use App\Models\crons;
use App\Http\Controllers\libs;
use Mailgun\Mailgun;
use DB;
use App\Http\Controllers\TblConfigController;

class dataController extends Controller
{
    public function editEdad()
    {
        $script='UPDATE 
            `tbl_clientes` 
            SET id_documento_tipo=3 
            WHERE
            TIMESTAMPDIFF(YEAR,tbl_clientes.fecha_nacimiento,CURDATE())>=18
            AND
        tbl_clientes.id_documento_tipo=2';
        try 
        {
            $res=DB::select($script);
            DB::table('crons')
            ->insert(
                [
                    'sql'   => $script,
                    'status'=>'success',
                    'res'   => json_encode($res,128),
                    'error' => NULL
                ]
            );
            return ['status'=>'success'];
        } 
        catch (\Exception $th) 
        {
            DB::table('crons')
            ->insert(
                [
                    'sql'   => $script,
                    'status'=>'failed',
                    'res'   => NULL,
                    'error' => $th->getMessage()
                ]
            );
            return ['status'=>'failed'];
        }
    }
    public function edad($fecha)
    {
            $fecha_nac = new \DateTime(date('Y/m/d',strtotime($fecha)));
            $fecha_hoy =  new \DateTime(date('Y/m/d',time()));
            $edad = date_diff($fecha_hoy,$fecha_nac); 
            return 
            [
                'y'=>$edad->format('%Y'),
                'm'=>$edad->format('%m'),
                'd'=>$edad->format('%d')
            ];
             
    }
    public static function EnviarMail($remitente,$email,$asusnto,$html,$file=null)
    {
        $res=array();
        try {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
            {
                $to = $email;
                $subject = $asusnto;
                $txt = $html;
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers .= "From: {$remitente}" . "\r\n".'X-Mailer: PHP/' . phpversion();
                $respose=mail($to,$subject,$txt,$headers);

                $res = ['validate'=>$respose,'msj'=>$respose?'email enviado':'No se envio el email'];
            }
            else{
                $res = ['validate'=>false,'msj'=>'email invalido'];
            }
        } catch (\Exception $e) {
            $res = ['validate'=>false,'msj'=>$e->getMessage()];
        }
        return (object) $res;
    }
    public function documentoTipo()
    {
        return TblDocumentoTipos::orderBy('nombre')->get();
    }
    public static function EnviarSms($destino,$mensaje)
    {
        ini_set('soap.wsdl_cache_enabled',0);
        ini_set('soap.wsdl_cache_ttl',0);
        $id                 = TblConfigController::idsms();
        $wsdl               = TblConfigController::GetData('smsWsdl');
        $username           = TblConfigController::GetData('smsLogin');
        $password           = TblConfigController::GetData('smsPassword');
        $function           = TblConfigController::GetData('smsFunction');
        $codeSms            = TblConfigController::GetData('smsNumber');
        $parameters         = [
            'subscriber'    => '57'.$destino, 
            'sender'        => $codeSms, 
            'requestId'     => (int)$id, 
            'receiptRequest'=> '0',
            'dataCoding'    => '0',
            'message'       => $mensaje,
        ];
        $client = new \SoapClient($wsdl,array(
            'login' => $username,
            'password' => $password
        ));
        $client->__setLocation($wsdl);
        $res    = $client->__soapCall($function,array('Parameters' => $parameters));
        return $res;
    }
    public function municipios(Request $request)
    {
        return TblMunicipios::where('id_departamento','=',$request->id_departamento)->orderBy('nombre')->get();
    }
    public static function formatHora($time)
    {
        if(strpos($time,'PM'))
        {
            $time=explode(':',$time);
            $time[0]=$time[0]=='12'?'12':$time[0]+12;
            $time=$time[0].':'.$time[1];
        }
        else{
            $time=explode(':',$time);
            $time[0]=$time[0]=='12'?'00':$time[0];
            $time=$time[0].':'.$time[1];
        }
        return str_replace(['AM','PM'],['',''],$time);
    }
    public static function textoFecha($date)
    {
        $meses['01']        = 'Enero';
        $meses['02']        = 'Febrero';
        $meses['03']        = 'Marzo';
        $meses['04']        = 'Abril';
        $meses['05']        = 'Mayo';
        $meses['06']        = 'Junio';
        $meses['07']        = 'Julio';
        $meses['08']        = 'Agosto';
        $meses['09']        = 'Septiembre';
        $meses['10']        = 'Octubre';
        $meses['11']        = 'Noviembre';
        $meses['12']        = 'Diciembre';
        return date('d',strtotime($date)) . ' de ' . $meses[date('m',strtotime($date))] . ' de ' .  date('Y',strtotime($date));
    }
    public static function textoFechaMes($date)
    {
        $meses['01']        = 'Enero';
        $meses['02']        = 'Febrero';
        $meses['03']        = 'Marzo';
        $meses['04']        = 'Abril';
        $meses['05']        = 'Mayo';
        $meses['06']        = 'Junio';
        $meses['07']        = 'Julio';
        $meses['08']        = 'Agosto';
        $meses['09']        = 'Septiembre';
        $meses['10']        = 'Octubre';
        $meses['11']        = 'Noviembre';
        $meses['12']        = 'Diciembre';
        return $meses[date('m',strtotime($date))];
    }
    
}
