<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblMensajes;
use App\Models\TblMensajesTipo;
use Illuminate\Http\Request;
use Exception;

class TblMensajesController extends Controller
{

    /**
     * Display a listing of the tbl mensajes.
     *
     * @return Illuminate\View\View
     */
    public function index(Request $request)
    {
        if(($request->tipo)!='')
        {
            $tblMensajesObjects = TblMensajes::with('TblMensajesTipo','TblCitas','Users','TblClientes')
            ->orderBy('created_at','DESC')
            ->where('id_mensaje_tipo',$request->tipo)
            ->paginate(25)
            ->appends($request->tipo);
            return view('tbl_mensajes.index', compact('tblMensajesObjects'));
        }
        else
        {
            $tblMensajesObjects = TblMensajes::with('TblMensajesTipo','TblCitas','Users','TblClientes')
            ->orderBy('created_at','DESC')
            ->paginate(25);
            return view('tbl_mensajes.index', compact('tblMensajesObjects'));
        }
    }

    /**
     * Show the form for creating a new tbl mensajes.
     *
     * @return Illuminate\View\View
     */
    public function create()
    {
        $TblMensajesTipos = TblMensajesTipo::pluck('id','id')->all();
        
        return view('tbl_mensajes.create', compact('TblMensajesTipos'));
    }

    /**
     * Store a new tbl mensajes in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            TblMensajes::create($data);

            return redirect()->route('tbl_mensajes.tbl_mensajes.index')
                ->with('success_message', 'Tbl Mensajes was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified tbl mensajes.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblMensajes = TblMensajes::with('TblMensajesTipo','TblCitas','Users','TblClientes')->findOrFail($id);
        $tblMensajes->respuesta=json_decode($tblMensajes->respuesta);
        //dd($tblMensajes);
        return view('tbl_mensajes.show', compact('tblMensajes'));
    }

    /**
     * Show the form for editing the specified tbl mensajes.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblMensajes = TblMensajes::findOrFail($id);
        $TblMensajesTipos = TblMensajesTipo::pluck('id','id')->all();

        return view('tbl_mensajes.edit', compact('tblMensajes','TblMensajesTipos'));
    }

    /**
     * Update the specified tbl mensajes in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblMensajes = TblMensajes::findOrFail($id);
            $tblMensajes->update($data);

            return redirect()->route('tbl_mensajes.tbl_mensajes.index')
                ->with('success_message', 'Tbl Mensajes ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tbl mensajes from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblMensajes = TblMensajes::findOrFail($id);
            $tblMensajes->delete();

            return redirect()->route('tbl_mensajes.tbl_mensajes.index')
                ->with('success_message', 'Tbl Mensajes was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'id_cliente' => 'nullable|numeric|min:-2147483648|max:2147483647',
            'destino' => 'nullable|string|min:0|max:200',
            'mensaje' => 'nullable',
            'id_mensaje_tipo' => 'nullable', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
