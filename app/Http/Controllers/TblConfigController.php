<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\TblConfig;

class TblConfigController extends Controller
{
    public function Get($name)
    {
        return TblConfig::select('value')->where('name','=',$name)->first();
    }
    public static function GetData($name)
    {
        $data = TblConfig::select('value')->where('name','=',$name)->first();
        return $data->value; 
    }
    public static function idsms()
    {
        $data = TblConfig::select('value','id')
        ->where('name','=','smsRequestId')
        ->first();
        $id=$data->value;
        $datas = TblConfig::firstOrNew(['id' => $data->id]);
        $datas->value    = $data->value+1;
        $datas->save();
        return $id; 
    }
    public static function SetData($name, $value)
    {
        $data = TblConfig::firstOrNew(['name' => $name]);
        $data->name     = $name;
        $data->value    = $value;
        $data->save();
        return (object)['validate'=>true];
    }
    
    public function Set($name, $value)
    {
        $data = new TblConfig();
        $data->name     = $name;
        $data->value    = $value;
        $data->save();
        return ['validate'=>true];
    }
}
