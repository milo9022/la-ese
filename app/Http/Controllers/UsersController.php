<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Users;
use App\UserRole;
use App\Role;
use Illuminate\Support\Facades\Auth;
use App\Models\TblPuntosAtenciones;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

    /**
     * Display a listing of the users.
     *
     * @return Illuminate\View\View
     */
    public function index()
    {
        $usersObjects = UserRole::
        select
        (
            'roles.name as roles_name',
            'roles.description as roles_description',
            'users.*',
            'tbl_puntos_atenciones.nombre as nombre_punto_atencion'
        )->
        leftJoin('roles','role_user.role_id', '=', 'roles.id')->
        leftJoin('users','role_user.user_id', '=', 'users.id')->
        leftJoin('tbl_puntos_atenciones','users.id_punto_atencion', '=', 'tbl_puntos_atenciones.id')->
        where('users.id','!=',Auth::id())->
        where('users.id','!=','17')->
        whereNull('users.deleted_at')->
        get();
        $usersObjects=(json_decode($usersObjects));
        return view('users.index', compact('usersObjects'));
    }

    /**
     * Show the form for creating a new users.
     *
     * @return Illuminate\View\View
     */
    public function reload(Request $request)
    {
        return ['data'=>true];
    }
    public function create()
    {
        $TblRoles            = Role::pluck('description','id')->all();
        $TblPuntosAtenciones = TblPuntosAtenciones::pluck('nombre','id')->all();
        return view('users.create', compact('TblPuntosAtenciones','TblRoles'));
    }

    /**
     * Store a new users in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {
            $data = $this->getData($request);
            $data['password']=bcrypt($data['documento']);
            $user = Users::insertGetId($data);
            UserRole::create(['user_id'=>$user,'role_id'=>$request->id_rol]);
            return redirect()->route('users.users.index')
                ->with('success_message', 'Se ha creado un nuevo usuario. Recuerde que su contraseña por defecto es el numero de documento');
        } catch (Exception $exception) {

            return back()
                ->withInput()
                ->withErrors(['unexpected_error' => 
                'Unexpected error occurred while trying to process your request:'.$exception->getMessage()]);
        }
    }

    /**
     * Display the specified users.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $users = Users::findOrFail($id);

        return view('users.show', compact('users'));
    }

    /**
     * Show the form for editing the specified users.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function Cerrar_Logout()
    {
        Auth::logout();
        return redirect()->route('home')
        ->with('msg', 'Gracias por visitarnos!.');
    }
    public function changePass()
    {
        $user=Users::find(Auth::id());
        return view('users.changepass',compact('user'));
    }
    public function NewPass(Request $request)
    {
        if (Hash::check($request->old_pass, Auth::user()->password))
        {
            if($request->new_pass_1===$request->new_pass_2)
            {
                $user = Users::find(Auth::user()->id);
                $user->password=bcrypt($request->new_pass_1);
                $user->save();
                return redirect()->route('users.users.change')
                ->with('success_message', 'Su contraseña ha sido modificada con éxito');
            }
            else{
                return back()->withInput()
                ->withErrors(['unexpected_error' => 'Las contraseñas nuevas no coinciden']);                
            }
        }
        else{
            return back()->withInput()
            ->withErrors(['unexpected_error' => 'La contraseña actual no coincide']);
        }
        $user=Users::find(Auth::id());
        return view('users.changepass',compact('user'));
    }
    
    
    public function edit($id)
    {
        $users               = Users::findOrFail($id);
        $users->id_rol       = (UserRole::where('user_id',$id)->first())->role_id;
        $TblRoles            = Role::pluck('description','id')->all();
        $TblPuntosAtenciones = TblPuntosAtenciones::pluck('nombre','id')->all();
        return view('users.edit', compact('users','TblPuntosAtenciones','TblRoles'));
    }

    /**
     * Update the specified users in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $users = Users::findOrFail($id);
            $users->update($data);

            return redirect()->route('users.users.index')
                ->with('success_message', 'Este usuario ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified users from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $users = Users::findOrFail($id);
            $users->delete();

            return redirect()->route('users.users.index')
                ->with('success_message', 'Users was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre_primero' => 'required|string|min:1|max:191',
                'nombre_segundo' => 'required|string|min:1|max:191',
                'apellido_primero' => 'required|string|min:1|max:191',
                'apellido_segundo' => 'required|string|min:1|max:191',
                'documento' => 'required|string|min:1|max:191',
                'id_punto_atencion' => 'required|numeric|min:-2147483648|max:2147483647',
                'activo' => 'nullable|numeric|min:-2147483648|max:2147483647',
                'email' => 'required|string|min:1|max:191',
                'email_verified_at' => 'nullable|date_format:j/n/Y g:i A',
                'remember_token' => 'nullable|string|min:0|max:100', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
