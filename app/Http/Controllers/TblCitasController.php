<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TblConfigController;
use App\Http\Controllers\ReCaptcha;
use App\Models\TblCitas;
use App\Models\TblCitasEstados;
use App\Models\TblClientes;
use App\Models\TblConsultas;
use App\Models\TblPuntosAtenciones;
use App\Models\TblFacturacionTipo;
use App\Models\TblConsultorios;
use App\Models\TblEspecialistas;
use App\Models\TblMensajes;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Http\Request;
use Exception;

class TblCitasController extends Controller
{

    /**
     * Display a listing of the tbl citas.
     *
     * @return Illuminate\View\View
     */
    public function citasAll($filtro,Request $request)
    {
        $res=array();
        $dataFinish=TblCitas::
        select(
         'tbl_citas.id',
         DB::raw('
         DATE_FORMAT(tbl_citas.created_at, "%y-%m-%d %h:%I %p") as created
         '),
         'tbl_citas.codigo',
         'tbl_puntos_atenciones.nombre as punto_atencion',
         'tbl_citas_estados.id as id_citas_estado',
         'tbl_citas.fecha',
         DB::raw('TIME_FORMAT(tbl_citas.hora,"%h:%i %p") as hora'),
         'tbl_consultas.nombre as consulta',
         'tbl_clientes.id as cliente_id',
         'tbl_documento_tipos.nombre as documento_tipo_nombre',
         'tbl_documento_tipos.nombre_corto as documento_tipo_nombre_corto',
         'tbl_clientes.documento',
         DB::raw(
         "CONCAT_WS(' ',
         tbl_clientes.nombre_primero,
         tbl_clientes.nombre_segundo,
         tbl_clientes.apellido_primero,
         tbl_clientes.apellido_segundo) as cliente"
         ),
         DB::raw(
            "CONCAT_WS(' ',   
         tbl_especialistas.nombre_primero,
         tbl_especialistas.nombre_segundo,
         tbl_especialistas.apellido_primero,
         tbl_especialistas.apellido_segundo) as especialista"
         ),
         'tbl_citas.motivoconsulta',
         'tbl_facturacion_tipo.nombre as facturacion_tipo',
         'tbl_citas_estados.nombre as cita_estado',
         'tbl_citas_estados.color as cita_estado_color'
         )
        ->join('tbl_puntos_atenciones','tbl_citas.id_punto_atencion', '=', 'tbl_puntos_atenciones.id') 
        ->join('tbl_consultas','tbl_citas.id_consulta', '=', 'tbl_consultas.id') 
        ->join('tbl_clientes','tbl_clientes.id', '=', 'tbl_citas.id_cliente') 
        ->leftJoin('tbl_especialistas','tbl_citas.id_especialista', '=', 'tbl_especialistas.id') 
        ->leftJoin('tbl_facturacion_tipo','tbl_citas.id_facturacion_tipo', '=', 'tbl_facturacion_tipo.id') 
        ->join('tbl_citas_estados','tbl_citas_estados.id', '=', 'tbl_citas.id_citas_estado')
        ->join('tbl_documento_tipos','tbl_clientes.id_documento_tipo', '=', 'tbl_documento_tipos.id')
        ->orderBy('fecha','ASC');
        $sql='';
        if(trim($request->search['value'])!='')
        {
            $val=trim($request->search['value']);
            $sql='
            (
                tbl_clientes.documento like \'%'.$val.'%\'
                OR
                tbl_citas.codigo like \'%'.$val.'%\'
                OR
                tbl_clientes.nombre_primero like  \'%'.trim($request->search['value']).'%\'
                OR
                tbl_clientes.nombre_segundo like  \'%'.trim($request->search['value']).'%\'
                OR
                tbl_clientes.apellido_primero like  \'%'.trim($request->search['value']).'%\'
                OR
                tbl_clientes.apellido_segundo like  \'%'.trim($request->search['value']).'%\'
            )';
            $tempArray=[];
            $dat1=explode(' ',$val);
            foreach($dat1 as $temp)
            {

                $tempArray[]='(
                    tbl_clientes.nombre_primero like  \'%'.$temp.'%\'
                OR
                    tbl_clientes.nombre_segundo like  \'%'.$temp.'%\'
                OR
                    tbl_clientes.apellido_primero like  \'%'.$temp.'%\'
                OR
                    tbl_clientes.apellido_segundo like  \'%'.$temp.'%\'
                )';
            }
            if(count($tempArray)>0)
            {
                $txt=implode(' OR ',$tempArray);
                $sql.=' OR '.$txt;
            }
            $dataFinish=$dataFinish
            ->whereRaw($sql);
            $sql=' AND '.$sql;
        }
        if(Auth::user()->roles[0]->name=='facturador')
        {
            $dataFinish=$dataFinish
            ->whereRaw('(tbl_citas.id_punto_atencion = ? AND tbl_citas.id_citas_estado =?)'
            . $sql, [Auth::user()->id_punto_atencion,1]);
        }
        else
        {
            if($filtro!='-1')
            {
                $dataFinish=$dataFinish->whereRaw('(tbl_citas.id_citas_estado = ? )'
                . $sql, [$filtro]);
            }
        }
        $res['recordsTotal']    =   $dataFinish->count();
        $res['recordsFiltered'] =   $dataFinish->count();
        $res["draw"]            =   $request->draw;
        $dataFinish             =   $dataFinish->limit($request->length)->skip($request->start);
        $res['data']            =   $dataFinish->get();
        return $res;
    }
    public function index()
    {
        $TblCitasEstado = TblCitasEstados::all();
        return view('tbl_citas.index', compact('TblCitasEstado'));
    }

    /**
     * Show the form for creating a new tbl citas.
     *
     * @return Illuminate\View\View
     */
    public function BuscarPaciente(Request $request)
    {
        $cliente = TblClientes::firstOrNew(['documento' => $request->datos_personales['documento']]);
        $cliente->id_documento_tipo =   $request->datos_personales['documentoTipo'];
        $cliente->documento         =   $request->datos_personales['documento'];
        $cliente->celular1          =   $request->datos_personales['celular1'];
        $cliente->celular2          =   $request->datos_personales['celular2'];
        $cliente->email             =   $request->datos_personales['email'];
        $cliente->save();
        return $cliente->id;
    }
    private function validateCaptcha($dataCaptcha)
    {
        if(!is_null($dataCaptcha))
        {

            $secret          = TblConfigController::GetData('captcha-private');
            $url             = 'https://www.google.com/recaptcha/api/siteverify';
            $data            = array('secret' => $secret,'response' => $dataCaptcha);
            $options = array
            (
                'http' => array 
                (
                    'method' => 'POST',
                    'content' => http_build_query($data),
                    'header' => 'Content-Type: application/x-www-form-urlencoded'
                )
            );
            $context         = stream_context_create($options);
            $verify          = file_get_contents($url, false, $context);
            $captcha_success = json_decode($verify);
            return ($captcha_success->success);
        }
        else{
            return false;
        }
    }
    public function crearCitaCodigo(Request $request)
    {
        if($this->validateCaptcha($request->input("g-recaptcha-response")))
        {
            $data = new TblCitas();
            $data->id_punto_atencion    = $request->datos_personales['id_punto_atencion'];
            $data->id_cliente           = $this->BuscarPaciente($request);
            $data->id_citas_estado      = 1;
            $data->id_facturacion_tipo  = 1;
            $data->id_consulta          = $request->id_programa;
            $data->fecha                = $request->fecha_cita;
            $data->motivoconsulta       = $request->motivoconsulta;
            $data->save();        
            $codigo                     = TblConfigController::GetData('codigo_cita');
            $data->codigo               = 'COD'.str_pad($codigo, 6, "0", STR_PAD_LEFT);
            $data->save(); 
            TblConfigController::SetData('codigo_cita',($codigo+1));
            return ['validate'=>true,'codigo'=>$data->codigo];
        }
        else{
            return ['validate'=>false,'codigo'=>null];
        }

    }
    public function create()
    {
        $TblPuntosAtenciones = TblPuntosAtenciones::pluck('nombre','id')->all();
        $TblClientes = array();
        $TblCitasEstados = TblCitasEstados::pluck('nombre','id')->all();
        $TblConsultas = TblConsultas::pluck('nombre','id')->all();
        return view('tbl_citas.create', compact('TblPuntosAtenciones','TblConsultas','TblClientes','TblCitasEstados'));
    }
    public function store(Request $request)
    {
        try 
        {    
            $request->hora=dataController::formatHora($request->hora);
            $request['hora']=$request->hora;
            $data = $this->getData($request);   
            $id=TblCitas::create($data);
            $data = TblCitas::find($id->id);
            $codigo         = TblConfigController::GetData('codigo_cita');
            $data->codigo   = 'COD'.str_pad($codigo, 6, "0", STR_PAD_LEFT);
            $data->save(); 
            TblConfigController::SetData('codigo_cita',($codigo+1));
            
            return redirect()->route('tbl_citas.tbl_citas.index')
                ->with('success_message', 'Cita ha sido creada satisfactoriamente con el codigo '.$data->codigo.'.');
        }
        catch (Exception $exception)
        {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.'.$exception->getMessage()]);
        }
    }
    public function searchespecialistas(Request $request)
    {
        $term=trim(strtolower($request->term));
        $data = TblEspecialistas::
        select('*',DB::raw('CONCAT_WS(" ",
        TRIM(replace(nombre_primero,\'\\t\',\' \')),
        TRIM(replace(nombre_segundo,\'\\t\',\' \')),
        TRIM(replace(apellido_primero,\'\\t\',\' \')),
        TRIM(replace(apellido_segundo,\'\\t\',\' \'))
        ) as value'))
        ->where    (DB::raw('LOWER(nombre_primero)'  ), 'like', '%'.$term.'%')
        ->orWhere(DB::raw(  'LOWER(nombre_segundo)'  ),'like',  '%'.$term.'%')
        ->orWhere(DB::raw(  'LOWER(apellido_primero)'),'like',  '%'.$term.'%')
        ->orWhere(DB::raw(  'LOWER(apellido_segundo)'),'like',  '%'.$term.'%')
        ->orWhere(DB::raw(  'LOWER(documento)'       ),'like',  '%'.$term.'%')
        ->orderBy(DB::raw('trim(nombre_primero)'))
        ->orderBy(DB::raw('trim(nombre_segundo)'))
        ->orderBy(DB::raw('trim(apellido_primero)'))
        ->orderBy(DB::raw('trim(apellido_segundo)'))        
        ->get();
        return $data;
    }
    public function formatTextEmail(Request $request)
    {
        $string = TblConfigController::GetData('mensaje-email');
        $res = $this->mensajeFormat($request->id,$string,$request->hora,$request->medico,$request->fecha);
        return json_encode(['validate'=>true,'data'=>$res]);

    }
    public function formatTextSms(Request $request)
    {
        $string = TblConfigController::GetData('mensaje-sms');
        $res = $this->mensajeFormat($request->id,$string,$request->hora,$request->medico,$request->fecha);
        return json_encode(['validate'=>true,'data'=>$res]);
    }
    private function mensajeFormat($id,$String='',$hora='',$medico='',$fecha='')
    {
        $tblCitas = TblCitas::with('TblPuntosAtenciones','TblConsultas','TblCliente.TblDocumentoTipos','TblCliente','TblCliente.TblEps','tblcitasestado')->findOrFail($id);
        $fecha    = ($fecha=='')?(isset($tblCitas->fecha)?$tblCitas->fecha:''):$fecha;
        $hora     = ($hora=='')?(isset($tblCitas->hora)?$tblCitas->hora:''):$hora;
        $hora     = date('h:i A',strtotime($hora));
        $replace=[
            '{nombre_completo}',
            '{nombre_primero}',
            '{nombre_segundo}',
            '{apellido_primero}',
            '{apellido_segundo}',
            '{fecha_nacimiento}',
            '{documento}',
            '{celular1}',
            '{celular2}',
            '{email}',
            '{punto_atencion}',
            '{documento_tipo}',
            '{documento_tipo_nombre}',
            '{eps}',
            '{fecha_cita}',
            '{hora}',
            '{medico}',
            '{fecha_texto}',
            '{fecha_dia}',
            '{fecha_mes}',
            '{fecha_mes_nombre}',
            '{fecha_anno}',
            '{codigo}',
        ];
        $replaceData=[
            /*nombre_completo*/       $tblCitas->TblCliente->nombre_primero.' '.$tblCitas->TblCliente->nombre_segundo.' '.$tblCitas->TblCliente->apellido_primero.' '.$tblCitas->TblCliente->apellido_segundo,
            /*nombre_primero*/        (isset($tblCitas->TblCliente->nombre_primero)?$tblCitas->TblCliente->nombre_primero:''),
            /*nombre_segundo*/        (isset($tblCitas->TblCliente->nombre_segundo)?$tblCitas->TblCliente->nombre_segundo:''),
            /*apellido_primero*/      (isset($tblCitas->TblCliente->apellido_primero)?$tblCitas->TblCliente->apellido_primero:''),
            /*apellido_segundo*/      (isset($tblCitas->TblCliente->apellido_segundo)?$tblCitas->TblCliente->apellido_segundo:''),
            /*fecha_nacimiento*/      (isset($tblCitas->TblCliente->fecha_nacimiento)?$tblCitas->TblCliente->fecha_nacimiento:''),
            /*documento*/             (isset($tblCitas->TblCliente->documento)?$tblCitas->TblCliente->documento:''),
            /*celular1*/              (isset($tblCitas->TblCliente->celular1)?$tblCitas->TblCliente->celular1:''),
            /*celular2*/              (isset($tblCitas->TblCliente->celular2)?$tblCitas->TblCliente->celular2:''),
            /*email*/                 (isset($tblCitas->TblCliente->email)?$tblCitas->TblCliente->email:''),
            /*punto_atencion */       (isset($tblCitas->TblPuntosAtenciones->nombre)?$tblCitas->TblPuntosAtenciones->nombre:''),
            
            /*documento_tipo*/        (isset($tblCitas->TblCliente->TblDocumentoTipos->nombre)?$tblCitas->TblCliente->TblDocumentoTipos->nombre:''),
            /*documento_tipo_nombre*/ (isset($tblCitas->TblCliente->TblDocumentoTipos->nombre_corto)?$tblCitas->TblCliente->TblDocumentoTipos->nombre_corto:''),
            /*eps*/                   (isset($tblCitas->TblCliente->TblEps->descripcion)?$tblCitas->TblCliente->TblEps->descripcion:''),
            
            /*fecha_cita*/            $fecha,
            /*hora*/                  $hora,
            /*medico*/                $medico,
            /*fecha_texto*/           dataController::textoFecha($fecha),
            /*fecha_dia*/             date('d',strtotime($fecha)),
            /*fecha_mes*/             date('m',strtotime($fecha)),
            /*fecha_mes_nombre*/      dataController::textoFechaMes($fecha),
            /*fecha_anno*/            date('Y',strtotime($fecha)),
            /*codigo*/                isset($tblCitas->codigo)?$tblCitas->codigo:'',
        ];
        $data = str_replace($replace,$replaceData,$String);
        return $data;
    }
    public function mensajeSMS($id)
    {
        $sms = TblConfigController::GetData('mensaje-sms');
        return $this->mensajeFormat($id,$sms);
    }
    public function mensajeSMSCancel($id)
    {
        $sms = TblConfigController::GetData('mensaje-sms-cancel');
        return $this->mensajeFormat($id,$sms);
    }
    public function mensajeEmailCancel($id)
    {
        $email = TblConfigController::GetData('mensaje-email-cancel');
        return $this->mensajeFormat($id,$email);
    }
    public function mensajeEmail($id)
    {
        $email = TblConfigController::GetData('mensaje-email');
        return $this->mensajeFormat($id,$email);
    }
    public function ShowmensajeSMS($id)
    {
        return TblMensajes::where('id_cita','=',$id)->where('id_mensaje_tipo','=','1')->get();
    }
    public function ShowmensajeEmail($id)
    {
        return TblMensajes::where('id_cita','=',$id)->where('id_mensaje_tipo','=','2')->get();
    }
    
    public function envarmensajes($mensaje,$tipo,$id_cliente,$id_cita)//email(2),sms(1)
    {
        $emailRemitente=TblConfigController::GetData('email');
        $cliente                = TblClientes::find($id_cliente);

        $data                   = new TblMensajes();
        $data->id_cliente       = $id_cliente;
        $data->destino          = $tipo==1?$cliente->celular1:$cliente->email;
        $data->mensaje          = $mensaje;
        $data->id_usuario       = Auth::user()->id;
        $data->id_mensaje_tipo  = $tipo;
        $data->id_cita          = $id_cita;
        $data->save();
        if($tipo==1)
        {
            $res = dataController::EnviarSms($cliente->celular1,$mensaje);
            $data->respuesta=json_encode($res);
            $data->save();
        }
        else if($tipo==2)
        {
            $asusnto='Información de su cita médica';
            $res = dataController::EnviarMail($emailRemitente,$cliente->email,$asusnto,$mensaje);
            $data->respuesta=json_encode($res);
            $data->save();
        }
    }
    public function saveAsignacion($id,Request $request)
    {
        $data = TblCitas::with('Tblusuario')->with('Tblusuario.TblPuntosAtenciones')->where('id','=',$id)->firstOrFail();
        try 
        {
            if($data->id_citas_estado!=1)
            {
                return redirect('citas/')
                ->with('danger', 'Esta cita, con código '.$data->codigo.' ya habia sido asignada el dia '.$data->updated_at->format('Y-m-d').' a las '.$data->updated_at->format('h:i:s A').', por el usuario '.($data->Tblusuario->nombre_primero).' '.($data->Tblusuario->apellido_primero).' del punto de atención de "'.$data->Tblusuario->TblPuntosAtenciones->nombre.'"');
            }
            else{
                
                $hora=dataController::formatHora($request->hora);
                $data->fecha                        = $request->fecha;
                $data->hora                         = $hora;
                $data->id_especialista              = $request->id_especialistas;
                $data->id_consultorios              = $request->id_consultorios;
                $data->id_facturacion_tipo          = $request->id_facturacion_tipo;
                $data->id_citas_estado              = 2;
                $data->id_usuario                   = Auth::user()->id;
                $this->envarmensajes($request->mensajeemail,2,$data->id_cliente,$id);
                $this->envarmensajes($request->mensajesms,1,$data->id_cliente,$id);
        
                $data->save();
                return 
                redirect('citas/')
                ->with('success', 'La cita fue asignada con éxito!');
            }
        } catch (\Throwable $th)
        {
            Log::error(['Error:'=>$th->getMessage(),'data'=>json_encode($data),'Request:'=>json_encode($request->all())]);
            return 
                redirect('citas/')
                ->with('danger', $th->getMessage());
        }
    }
    public function CancelarCita($id,Request $request)
    {
        $data = TblCitas::find($id);
        $data->id_citas_estado = 4;
        $data->cancelacion_motivo=$request->mensajeCancelar;
        $data->save();
        $this->envarmensajes($request->mensajeemail,2,$data->id_cliente,$id);
        $this->envarmensajes($request->mensajesmscancel,1,$data->id_cliente,$id);
        return redirect()->route('tbl_citas.tbl_citas.index')
        ->with('success_message', 'La cita fue cancelada');
    }
    public function show($id)
    {   
        $tblConsultorios    = TblConsultorios::all();
        $tblFacturacionTipo = TblFacturacionTipo::all();
        $tblCitas           = TblCitas::with('TblPuntosAtenciones','TblConsultas','TblCliente.TblDocumentoTipos','TblCliente','TblCliente.TblEps','tblcitasestado')->findOrFail($id);
        if(is_null($tblCitas->TblCliente->nombre_primero))
        {
            return redirect()->route('tbl_clientes.tbl_clientes.edit', ['id' => $tblCitas->TblCliente->id]);
        }
        else
        {
            if($tblCitas->id_citas_estado=='1')
            {
                $tblCitas->hora       = date('h:i A',strtotime($tblCitas->hora));
                $sms                  = $this->mensajeSMS($id);
                $email                = $this->mensajeEmail($id);
                $emailcancel          = $this->mensajeEmailCancel($id);
                $smscancel            = $this->mensajeSMSCancel($id);
                $totalSMS             = TblConfigController::GetData('smsTotal');
                $smsCaracteresValidos = TblConfigController::GetData('smsCaracteresValidos');
                return view('tbl_citas.show', compact('tblCitas','id','tblFacturacionTipo','tblConsultorios','sms','email','emailcancel','smscancel','totalSMS','smsCaracteresValidos'));
            }
            else
            {
                return redirect()->route('tbl_citas.tbl_citas.index');
            }
        }
    }
    public function show2(Request $request)
    {   
        try {
        $tblConsultorios    = TblConsultorios::all();
        $tblFacturacionTipo = TblFacturacionTipo::all();
        $tblCitass           = TblCitas::with(
            'TblPuntosAtenciones',
            'TblConsultas',
            'TblConsultorios',
            'TblEspecialistas',
            'TblFacturacionTipo',
            'TblCliente',
            'TblCliente.TblDocumentoTipos',
            'TblCliente.TblEps',
            'tblcitasestado')->
        join('tbl_clientes', 'tbl_citas.id_cliente', '=', 'tbl_clientes.id')->
        where('tbl_citas.codigo','=',$request->codigo)->
        orWhere('tbl_clientes.documento','=',$request->codigo)->
        orderBy('tbl_citas.created_at')->
        get();
        if(is_null($tblCitass))
        {
            return back()->withInput()->withErrors(['unexpected_error' => 'No se ha encontrado paciente ni cita con esa busqueda']);
        }
        else
        {
                $sms                = $this->ShowmensajeSMS($tblCitass[0]->id);
                $email              = $this->ShowmensajeEmail($tblCitass[0]->id);
                return view('tbl_citas.detail', compact('tblCitass','tblFacturacionTipo','tblConsultorios','sms','email'));   
        }
        
        //code...
    } catch (\Exception $e) {
            return back()->withInput()->withErrors(['unexpected_error' => 'No se ha encontrado paciente ni cita con esa busqueda: '.$e->getMessage()]);
        }
    }
    public function DatosporCodigo(Request $request)
    {
        $res=TblCitas::
        select(
            'tbl_puntos_atenciones.nombre as punto_atencion',
            'tbl_clientes.nombre_primero',
            'tbl_clientes.nombre_segundo',
            'tbl_clientes.apellido_primero',
            'tbl_clientes.apellido_segundo',
            'tbl_clientes.documento',
            'tbl_clientes.celular1',
            'tbl_clientes.email',
            'tbl_citas.fecha',
            'tbl_citas.hora',
            'tbl_citas.codigo',
            'tbl_citas_estados.nombre as cita_estado',
            'tbl_citas_estados.id as id_cita_estado',
            DB::raw(
                'CONCAT_WS(\' \',
                tbl_especialistas.nombre_primero,
                tbl_especialistas.apellido_primero,
                tbl_especialistas.nombre_segundo,
                tbl_especialistas.apellido_segundo) as especialista'
            )
        )
        ->join('tbl_puntos_atenciones','tbl_citas.id_punto_atencion', '=', 'tbl_puntos_atenciones.id') 
        ->join('tbl_clientes','tbl_citas.id_cliente', '=', 'tbl_clientes.id') 
        ->join('tbl_citas_estados','tbl_citas.id_citas_estado', '=', 'tbl_citas_estados.id')
        ->leftJoin('tbl_especialistas','tbl_citas.id_especialista', '=', 'tbl_especialistas.id')
        ->whereRaw('tbl_citas_estados.id < ? and (tbl_citas.codigo = ? or `tbl_clientes`.`documento` = ?)',[3,strtoupper($request->codigo),strtoupper($request->codigo)])
        //->where('tbl_citas_estados.id','<',4)
        //->where('tbl_citas.codigo','=',strtoupper($request->codigo))
        //->orWhere('tbl_clientes.documento','=',strtoupper($request->codigo))
        ->orderBy('tbl_citas_estados.id','ASC')
        ->orderBy('tbl_citas.fecha','DESC')
        ->get();
        return ['validate'=>true,'data'=>$res];
    }
    public function citaCancelarUsuario(Request $request)
    {
        try 
        {
            $res=TblCitas::
            where('codigo',$request->codigo)
            ->update
            ([
                'usuario_cancela_motivo' => $request->motivo,
                'usuario_cancela_fecha' => date('Y-m-d H:i:s'),
                'id_citas_estado'        => 5
            ]);
            return ['validate'=>true,'msj'=>'Su cita ha sido cancelada'];
        }
        catch (\Throwable $th) 
        {
            return ['validate'=>false,'msj'=>'Se presento un error al intentar cancelar su cita'];
        }
    }
    public function detalleCitas(Request $request)
    {
        return TblCitas::
        select(
         'tbl_puntos_atenciones.nombre as punto_atencion',
         'tbl_consultas.nombre as consulta',
         'tbl_citas_estados.nombre as cita_estado',
         'tbl_facturacion_tipo.nombre as facturacion_tipo',
         DB::raw('CONCAT_WS(\' \',
         tbl_especialistas.nombre_primero,
         tbl_especialistas.nombre_segundo,
         tbl_especialistas.apellido_primero,
         tbl_especialistas.apellido_segundo) as especialista
         '),
         'tbl_especialistas.fecha_nacimiento',
         'tbl_especialistas.documento as especialista_documento',
         'tbl_consultorios.nombre as consultorio',
         'tbl_citas.codigo',
         'tbl_citas.motivoconsulta',
         DB::raw('CONCAT_WS(\' \',
         tbl_clientes.nombre_primero,
         tbl_clientes.nombre_segundo,
         tbl_clientes.apellido_primero,
         tbl_clientes.apellido_segundo) as cliente'),
         'tbl_clientes.fecha_nacimiento as cliente_fecha_nacimiento',
         'tbl_clientes.documento as cliente_documento',
         'tbl_clientes.celular1 as cliente_celular1',
         'tbl_clientes.celular2 as cliente_celular2',
         'tbl_clientes.email as cliente_email',
         'tbl_eps.descripcion as eps',
         'tbl_citas.cancelacion_motivo'
        )
        ->leftJoin('tbl_puntos_atenciones','tbl_citas.id_punto_atencion', '=', 'tbl_puntos_atenciones.id') 
        ->leftJoin('tbl_consultas','tbl_citas.id_consulta', '=', 'tbl_consultas.id') 
        ->leftJoin('tbl_citas_estados','tbl_citas.id_citas_estado', '=', 'tbl_citas_estados.id') 
        ->leftJoin('tbl_facturacion_tipo','tbl_citas.id_facturacion_tipo', '=', 'tbl_facturacion_tipo.id') 
        ->leftJoin('tbl_consultorios','tbl_citas.id_consultorios', '=', 'tbl_consultorios.id')
        ->leftJoin('tbl_especialistas','tbl_especialistas.id', '=', 'tbl_citas.id_especialista')
        ->leftJoin('tbl_clientes', 'tbl_clientes.id', '=', 'tbl_citas.id_cliente') 
        ->leftJoin('tbl_eps', 'tbl_eps.id', '=', 'tbl_clientes.id_eps') 
        ->where('tbl_citas.id','=',$request->id)
        ->first(); 
    }
    public function edit($id)
    {
        $tblCitas = TblCitas::findOrFail($id);
        $nombre=TblCitas::select(DB::raw(
            "CONCAT_WS(' ',
            tbl_clientes.nombre_primero,
            tbl_clientes.nombre_segundo,
            tbl_clientes.apellido_primero,
            tbl_clientes.apellido_segundo) as cliente"
            ))
            ->join('tbl_clientes','tbl_clientes.id', '=', 'tbl_citas.id_cliente') 
            ->where('tbl_citas.id','=',$id)
            ->first();
        $tblCitas->nombre=$nombre->cliente;
        $TblPuntosAtenciones = TblPuntosAtenciones::pluck('nombre','id')->all();
        $TblClientes = TblClientes::pluck('id','id')->all();
        $TblConsultas = TblConsultas::pluck('nombre','id')->all();
        $TblCitasEstados = TblCitasEstados::pluck('nombre','id')->all();
        return view('tbl_citas.edit', compact('tblCitas','TblPuntosAtenciones','TblConsultas','TblClientes','TblCitasEstados'));
    }

    /**
     * Update the specified tbl citas in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            $request->hora=dataController::formatHora($request->hora);
            $request['hora']=$request->hora;
            $data = $this->getData($request);
            
            $tblCitas = TblCitas::findOrFail($id);
            $tblCitas->update($data);

            return redirect()->route('tbl_citas.tbl_citas.index')
                ->with('success_message', 'Tbl Citas ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request:'.$exception->getMessage()]);
        }        
    }

    /**
     * Remove the specified tbl citas from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblCitas = TblCitas::findOrFail($id);
            $tblCitas->delete();

            return redirect()->route('tbl_citas.tbl_citas.index')
                ->with('success_message', 'La cita ha sido eliminada.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'fecha' => 'nullable',
            'hora' => 'nullable',
            'id_punto_atencion' => 'required',
            'id_consulta' => 'required',
            'id_cliente' => 'required',
            'id_citas_estado' => 'required', 
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
