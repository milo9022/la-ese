<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblClientes;
use App\Models\tblSexo;
use App\Models\TblDocumentoTipos;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\TblClientesCollectionImport;
class cargaPacientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('carga.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
        {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #region functios load
        require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'excel' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'PHPExcel.php';
        function paciente($IDE_PAC,$AP1_PAC,$AP2_PAC,$NM1_PAC,$NM2_PAC,$FNC_PAC,$id_sexo,$id_tipo_documento)
        {
            $data = TblClientes::where('documento','=',$IDE_PAC)->first();
            if(is_null($data))
            {
                $data = new TblClientes();
                $data->documento				= $IDE_PAC;
                $data->apellido_primero			= $AP1_PAC;
                $data->apellido_segundo			= $AP2_PAC;
                $data->nombre_primero			= $NM1_PAC;
                $data->nombre_segundo			= $NM2_PAC;
                $data->fecha_nacimiento			= $FNC_PAC;
                $data->id_sexo					= $id_sexo;
                $data->id_documento_tipo		= $id_tipo_documento;
                $data->Save();
            }
            return $data->id;
        }
        function sexo($nombre_corto)
        {
            $data	= tblSexo::where('nombre_corto','=',trim($nombre_corto))->first();
            return $data->id;
        }
        function tipo_documento($nombre_corto)
        {
            $data	= TblDocumentoTipos::where('nombre_corto','=',strtoupper($nombre_corto))->first();
            if(is_null($data))
            {
                $data	= new TblDocumentoTipos();
                $data->nombre_corto=strtoupper($nombre_corto);
                $data->nombre=strtoupper($nombre_corto);
                $data->save();
            }
            return $data->id;
        }
        /*
        function formatearArchivos($archivo)
        {
            $data          = array();
            $inputFileType = \PHPExcel_IOFactory::identify($archivo);
            $objReader     = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel   = $objReader->load($archivo);
            foreach ($objPHPExcel->setActiveSheetIndex(0)->getRowIterator() as $row)
            {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $temp         = array();
                foreach ($cellIterator as $cell)
                {
                    if (!is_null($cell))
                    {
                        $value  = $cell->getCalculatedValue();
                        $temp[] = $value;
                    }
                }
                $data[] = $temp;
            }
            return $data;
        }
        */
        #endregion
            $Resultado = $this->formatearArchivos($_FILES['file']['tmp_name']);
            @session_start();
            $id=$request->idload;
            $_SESSION[$id]['total'] =count($Resultado);
            foreach($Resultado as $key=>$temp)
            {
                if($key>0)
                {
                    $id_tipo_documento	= tipo_documento($temp[1]);
                    $fecha 				= date('d-m-Y', \PHPExcel_Shared_Date::ExcelToPHP($temp[7]));
                    $fecha 				= date("Y-m-d",strtotime($fecha."+ 1 days"));
                    $id_sexo			= sexo($temp[8]);
                    $IDE_PAC            = $temp['2'];
                    $AP1_PAC            = $temp['3'];
                    $AP2_PAC            = $temp['4'];
                    $NM1_PAC            = $temp['5'];
                    $NM2_PAC            = $temp['6'];
                    paciente($IDE_PAC,$AP1_PAC,$AP2_PAC,$NM1_PAC,$NM2_PAC,$fecha,$id_sexo,$id_tipo_documento);
                    $_SESSION[$id]['procesados']=$key;
                }
            }
            $_SESSION[$id]['finish']=true;
            return ['validate'=>true,'msj'=>'Proceso terminado'];
        
    }
    public function formatearArchivos($filepath)
    {
        $tamano = $_FILES["file"]['size'];
        $tipo = $_FILES["file"]['type'];
        $archivo = $_FILES["file"]['name'];
        $data=date('YmdHis').rand();
        $nombreModificado = $data.$archivo;
        $destino =  public_path('temp'.DIRECTORY_SEPARATOR.$nombreModificado);
        copy($_FILES['file']['tmp_name'],$destino);
        $return=(Excel::toArray(new TblClientesCollectionImport, $destino));
        return $return;
    }
    public function estado(Request $request)
    {
        @session_start();
        $id=$request->idload;
        return $_SESSION[$id];
    }
    public function createIdCarga()
    {
        @session_start();
        $id=date('YmdHis').rand();
        $_SESSION[$id]['finish']=false;
        $_SESSION[$id]['procesados']=0;
        $_SESSION[$id]['total']=0;
        return ['id'=>$id];
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
