<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TblDocumentoTipos;
use App\Models\TblClientes;
use App\Models\TblCitas;
use App\Models\TblEps;
use Illuminate\Http\Request;
use App\Http\Controllers\TblConfigController;
use DB;
use Exception;

class TblClientesController extends Controller
{

    public function clientesAll(Request $request)
    {
        $res=array();
        $dataFinish=TblClientes::with('TblDocumentoTipos');
        if(trim($request->search['value'])!='')
        {
            $val=trim($request->search['value']);
            $dataFinish=$dataFinish
            ->where('nombre_primero','LIKE','%'.$val.'%')
            ->orWhere('nombre_segundo','LIKE','%'.$val.'%')
            ->orWhere('apellido_primero','LIKE','%'.$val.'%')
            ->orWhere('apellido_segundo','LIKE','%'.$val.'%')
            ->orWhere('documento','LIKE','%'.$val.'%');
        }
        $res['recordsTotal']    =   $dataFinish->count();
        $res['recordsFiltered'] =   $dataFinish->count();
        $res["draw"]            =   $request->draw;
        $dataFinish             =   $dataFinish->limit($request->length)->skip($request->start);
        $res['data']            =   $dataFinish->get();
        return $res;
    }
    public function search(Request $request)
    {
        if(trim($request->term)!='')
        {
            $val=trim(strtolower($request->term));
            $dataFinish=TblClientes
            ::select(
                'id',
                DB::raw('CONCAT_WS(\' \',
                TRIM(replace(nombre_primero,\'\\t\',\' \')),
                TRIM(replace(nombre_segundo,\'\\t\',\' \')),
                TRIM(replace(apellido_primero,\'\\t\',\' \')),
                TRIM(replace(apellido_segundo,\'\\t\',\' \'))
                ) as value'),
                'documento'
            )
            ->with('TblDocumentoTipos')
            ->where(  DB::raw('LOWER(nombre_primero)'),'LIKE','%'.$val.'%')
            ->orWhere(DB::raw('LOWER(nombre_segundo)'),'LIKE','%'.$val.'%')
            ->orWhere(DB::raw('LOWER(apellido_primero)'),'LIKE','%'.$val.'%')
            ->orWhere(DB::raw('LOWER(apellido_segundo)'),'LIKE','%'.$val.'%')
            ->orWhere('documento','LIKE','%'.$val.'%')
            ->orderBy(DB::raw('2')) 
            ->limit(10);
            $data=$dataFinish->get();
            return  $data;
        }
        return [-1];
    }
    public function index()
    {
        $tblClientesObjects = TblClientes::with('TblDocumentoTipos')->paginate(25);

        return view('tbl_clientes.index', compact('tblClientesObjects'));
    }
    public function validarPaciente(Request $request)
    {
        $key=md5('citas'.date('Ymd'));
        if($key==$request->key)
        {
            return TblClientes::where('documento',$request->documento)->first();
        }
        else{
            return false;
        }
    }
    public function create()
    {
        $TblDocumentoTipos = TblDocumentoTipos::pluck('nombre','id')->all();
        $TblEps = TblEps::pluck('descripcion','id')->all();
        return view('tbl_clientes.create', compact('TblDocumentoTipos','TblEps'));
    }

    public function DiasNoHabiles(Request $request)
    {
        $total=10;
        $res=TblCitas::
        select(
            DB::raw('date(tbl_citas.fecha) as fecha'),
            DB::raw('count(*) as count_fecha')
        )
        ->having('count_fecha', '>' , $total)
        ->whereNotIn('tbl_citas.id_citas_estado',[3,4])
        ->groupBy(DB::raw('tbl_citas.fecha'))
        ->get();
        return $res;
    }
    private function validarCitasPendientes($documento)
    {
        $data = 
        TblClientes::
        join('tbl_citas',        'tbl_clientes.id',           '=', 'tbl_citas.id_cliente')->
        join('tbl_citas_estados','tbl_citas.id_citas_estado', '=', 'tbl_citas_estados.id')->
        where('tbl_clientes.documento','=',$documento)->
        where('tbl_citas_estados.id','=','1')->
        count();
        return($data===0);
    }
    public function ValidarDocumento(Request $request)
    {
        $data = [
            'celular1'=>null,
            'celular2'=>null,
            'email'=>null,
            'documento'=>'',
        ];
        $datas = TblClientes::
         select('documento')
         ->where('documento','=',$request->documento)
         ->where('id_documento_tipo','=',$request->documentoTipo)
         ->first();
         $data['documento']=(is_null($datas)?'':$datas->documento);
        return ['validate'=>($data['documento']!==''),'PermiteSacarCitas'=>$this->validarCitasPendientes($request->documento),'data'=>$data,'PermiteRegistro'=>TblConfigController::GetData('PermiteRegistro')];
    }

    public function store(Request $request)
    {
        try {
            $data = $this->getData($request);
            
            TblClientes::create($data);
            return redirect()->route('tbl_clientes.tbl_clientes.index')
                ->with('success_message', 'Cliente '.$data['nombre_primero'].' '.$data['nombre_segundo'].' '.$data['apellido_primero'].' '.$data['apellido_segundo'].' con documento '.$data['documento'].' ha sido creado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.',
                'msj'=>$exception->getMessage()]);
        }
    }

    /**
     * Display the specified tbl clientes.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $tblClientes = TblClientes::with('TblDocumentoTipos')->findOrFail($id);
        $TblEps = TblEps::pluck('descripcion','id')->all();
        return view('tbl_clientes.show', compact('tblClientes','TblEps'));
    }

    /**
     * Show the form for editing the specified tbl clientes.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function edit($id)
    {
        $tblClientes = TblClientes::findOrFail($id);
        $TblEps = TblEps::pluck('descripcion','id')->all();
        $TblDocumentoTipos = TblDocumentoTipos::pluck('nombre','id')->all();
        return view('tbl_clientes.edit', compact('tblClientes','TblDocumentoTipos','TblEps'));
    }

    /**
     * Update the specified tbl clientes in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {
            
            $data = $this->getData($request);
            
            $tblClientes = TblClientes::findOrFail($id);
            $tblClientes->update($data);

            return redirect()->route('tbl_clientes.tbl_clientes.index')
            ->with('success_message', 'Cliente '.$data['nombre_primero'].' '.$data['nombre_segundo'].' '.$data['apellido_primero'].' '.$data['apellido_segundo'].' con documento '.$data['documento'].' ha sido actualizado');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.'.$exception->getMessage()]);
        }        
    }

    /**
     * Remove the specified tbl clientes from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $tblClientes = TblClientes::findOrFail($id);
            $tblClientes->delete();

            return redirect()->route('TblClientes.TblClientes.index')
                ->with('success_message', 'Tbl Clientes was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    
    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
                'nombre_primero' => 'required|string|min:1|max:200',
            'nombre_segundo'    => 'nullable|string|min:0|max:200',
            'apellido_primero'  => 'required|string|min:1|max:200',
            'apellido_segundo'  => 'nullable|string|min:0|max:200',
            'fecha_nacimiento'  => 'nullable|string|min:0|max:50',
            'id_documento_tipo' => 'nullable',
            'documento'         => 'required|string|min:1|max:200', 
            'id_eps'            => 'nullable',
            'celular1'          => 'nullable|string|min:1|max:200',
            'celular2'          => 'nullable|string|min:1|max:200',
            'email'             => 'nullable|string|min:1|max:200'
        ];

        
        $data = $request->validate($rules);




        return $data;
    }

}
