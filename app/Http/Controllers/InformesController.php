<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TblPuntosAtenciones;
use App\Models\TblCitas;
use DB;


class InformesController extends Controller
{
    public function informesPuntoAtencion()
    {
        $puntos=TblPuntosAtenciones::orderBy('nombre')->get();
        return view('informes.punto_atencion',compact('puntos'));
    }
    private function generateCSV($data,$cabecera=[])
    {
        $name="informe.csv";
        if(count($data)>0) 
        {
            $nombre_archivo = public_path().DIRECTORY_SEPARATOR.'informe'.DIRECTORY_SEPARATOR.$name; 
            $archivo = fopen($nombre_archivo, "w+");
            foreach($cabecera as $key=>$temp1)
            {
                $cabecera[$key] = mb_convert_encoding($temp1, "Windows-1252", "UTF-8" );
            }
            fwrite($archivo,implode(';',$cabecera). "\n");
            foreach($data as $key=>$temp)
            {
                $temp=(json_decode(json_encode($temp)));
                $res=[];
                foreach($temp as $key1=>$temp1)
                {
                    $res[] = mb_convert_encoding($temp1, "Windows-1252", "UTF-8" );
                }
                fwrite($archivo,implode(';',$res). "\n");
            }
            return ['validate'=>true,'name'=>$name,'url'=>url("/informe/{$name}")];
            
        }
        return ['validate'=>false,'name'=>$name,'url'=>null];
    }
    public function DwinformesPuntoAtencion(Request $request)
    {

        $encabezado=[
            'PUNTO DE ATENCION',	
            'ESTADO	',
            'CODIGO	',
            'NOMBRE COMPLETO	',
            'TIPO DOCUMENTO	',
            'DOCUMENTO	',
            'FECHA DE NACIMIENTO',
            'EDAD',
            'FECHA DE CREACION	',
            'FECHA DE ASIGNACION	',
            'HORA DE ASIGNACION	',
            'PROFESIONAL ASIGNADO',
            'TIPO DE CONSULTA',	
            'PROGRAMA',
            'MOTIVO CONSULTA	',
            'EPS	',
            'TIPO DE FACTURACIÓN',
            'CELULAR	',
            'CORREO	 ',
            'USUARIO QUE ASIGNO O CANCELO LA CITA'


        ];



        $res=TblCitas::
        select(
            'tbl_puntos_atenciones.nombre AS punto_atencion',
            'tbl_citas_estados.nombre AS cita_estado',
            'tbl_citas.codigo',
            DB::raw("CONCAT_WS(' ',
                tbl_clientes.nombre_primero,
                tbl_clientes.nombre_segundo,
                tbl_clientes.apellido_primero,
                tbl_clientes.apellido_segundo) as cliente"
            ),
            'tbl_documento_tipos.nombre_corto AS documento_tipo',
            'tbl_clientes.documento',
            'tbl_clientes.fecha_nacimiento',
            DB::raw('0 as edad'),
            'tbl_citas.created_at AS fecha_creacion',
            'tbl_citas.fecha AS fecha_cita',
            'tbl_citas.hora AS hora_cita',
            DB::raw("CONCAT_WS(' ',
            tbl_profesional_asistencial.nombre_primero,
            tbl_profesional_asistencial.nombre_segundo,
            tbl_profesional_asistencial.apellido_primero,
            tbl_profesional_asistencial.apellido_segundo
            ) as profesional"
            ),
            'tbl_consultas_tipos.nombre as nombre_consulta_tipos',
            'tbl_consultas.nombre as nombre_consultas',
            'tbl_citas.motivoconsulta',
            'tbl_eps.descripcion as eps',
            'tbl_facturacion_tipo.nombre as nombre_facturacion_tipo',
            'tbl_clientes.celular1',
            'tbl_clientes.email',
            DB::raw('CONCAT_WS(
            \' \',
            `users`.`nombre_primero`,
            `users`.`nombre_segundo`,
            `users`.`apellido_primero`,
            `users`.`documento`,
            `users`.`apellido_segundo`) as usuario')
        )
        ->leftJoin('tbl_clientes','tbl_citas.id_cliente', '=', 'tbl_clientes.id') 
        ->leftJoin('tbl_puntos_atenciones','tbl_citas.id_punto_atencion', '=', 'tbl_puntos_atenciones.id') 
        ->leftJoin('tbl_citas_estados','tbl_citas.id_citas_estado', '=', 'tbl_citas_estados.id') 
        ->leftJoin('tbl_documento_tipos','tbl_documento_tipos.id', '=', 'tbl_clientes.id_documento_tipo') 
        ->leftJoin('tbl_profesional_asistencial','tbl_citas.id_especialista', '=', 'tbl_profesional_asistencial.id') 
        ->leftJoin('tbl_consultas','tbl_citas.id_consulta', '=', 'tbl_consultas.id') 
        ->leftJoin('tbl_consultas_tipos','tbl_consultas_tipos.id', '=', 'tbl_consultas.id_consulta_tipo') 
        ->leftJoin('tbl_eps','tbl_clientes.id_eps', '=', 'tbl_eps.id') 
        ->leftJoin('users','tbl_citas.id_usuario', '=', 'users.id')
        ->leftJoin('tbl_facturacion_tipo','tbl_citas.id_facturacion_tipo', '=', 'tbl_facturacion_tipo.id') 
        ->whereBetween('tbl_citas.created_at', [$request->fecha_inicio, $request->fecha_fin])
        ->where('tbl_puntos_atenciones.id','=',$request->id_puntos_atencion)
        ->get();
        return ( $this->generateCSV($res,$encabezado));
    }
}
