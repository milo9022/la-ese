<?php

namespace App\Http\Controllers;
use App\Http\Controllers\TblPuntosAtenciones;
use App\Http\Controllers\TblConfigController;
use Illuminate\Http\Request;

class DesabilitarController extends Controller
{
    public function paginaindex()
    {
        $BajarPagina=TblConfigController::GetData('desabilitado')=='1';
        if($BajarPagina)
        return view('debug');
        else
        return view('welcome');
    }
    public function index()
    {
        $estado = TblConfigController::GetData('desabilitado');
        return view('desabilitarForm.index',compact('estado'));
    }
    public function store(Request $request)
    {
        try {
            $estado = TblConfigController::SetData('desabilitado',$request->desabilitar);
            return redirect()->route('tbl_citas_estados.tbl_citas_estados.index')
            ->with('success_message', 'Se ha modificado con exito');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Se ha presentado un error inesperado']);
        }
    }
}
