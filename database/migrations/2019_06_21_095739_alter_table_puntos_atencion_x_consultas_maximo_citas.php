<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePuntosAtencionXConsultasMaximoCitas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_puntos_atenciones_x_consutas', function (Blueprint $table) {
            $table->integer('maximocitas')->nullable()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_puntos_atenciones', function (Blueprint $table) {
           // $table->dropColumn('maximocitas');
        });
    }
}
