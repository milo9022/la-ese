<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPuntosAtencionesXConsutasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tbl_puntos_atenciones_x_consutas';

    /**
     * Run the migrations.
     * @table tbl_puntos_atenciones_x_consutas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_consultas');
            $table->integer('id_punto_atencion');

            $table->index(["id_punto_atencion"], 'id_punto_atencion');

            $table->index(["id_consultas"], 'id_consultas');

            $table->unique(["id_consultas", "id_punto_atencion"], 'tbl_puntos_atencion_x_consutas_idx1');

/*
            $table->foreign('id_consultas', 'tbl_puntos_atencion_x_consutas_idx1')
                ->references('id')->on('tbl_consultas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_punto_atencion', 'id_punto_atencion')
                ->references('id')->on('tbl_puntos_atenciones')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
