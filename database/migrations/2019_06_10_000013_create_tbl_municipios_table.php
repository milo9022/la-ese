<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMunicipiosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tbl_municipios';

    /**
     * Run the migrations.
     * @table tbl_municipios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre', 200)->nullable()->default(null);
            $table->integer('id_departamento')->nullable()->default(null);

            $table->index(["id_departamento"], 'id_departamento');
            $table->nullableTimestamps();

/*
            $table->foreign('id_departamento', 'id_departamento')
                ->references('id')->on('tbl_departamentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
