<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPuntosAtencionesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tbl_puntos_atenciones';

    /**
     * Run the migrations.
     * @table tbl_puntos_atenciones
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre', 200);
            $table->integer('id_municipio');
            $table->integer('maximocitas')->nullable()->default(null);

            $table->index(["id_municipio"], 'id_municipio');
            $table->nullableTimestamps();

/*
            $table->foreign('id_municipio', 'id_municipio')
                ->references('id')->on('tbl_municipios')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
