<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblProfesionalAsistencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_profesional_asistencial', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_primero');
            $table->string('nombre_segundo');
            $table->string('apellido_primero');
            $table->string('apellido_segundo');
            $table->string('documento');
            $table->nullableTimestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_profesional_asistencial');
    }
}
