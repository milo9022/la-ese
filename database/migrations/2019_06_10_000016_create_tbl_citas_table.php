<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCitasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tbl_citas';

    /**
     * Run the migrations.
     * @table tbl_citas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('fecha')->nullable()->default(null);
            $table->time('hora')->nullable()->default(null);
            $table->integer('id_punto_atencion');
            $table->integer('id_consulta');
            $table->integer('id_cliente');
            $table->integer('id_citas_estado');

            $table->index(["id_consulta"], 'id_consulta');

            $table->index(["id_cliente"], 'id_cliente');

            $table->index(["id_punto_atencion"], 'id_punto_atencion');

            $table->index(["id_citas_estado"], 'id_cita_estado');
            $table->nullableTimestamps();

/*
            $table->foreign('id_punto_atencion', 'id_punto_atencion')
                ->references('id')->on('tbl_puntos_atenciones')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_consulta', 'id_consulta')
                ->references('id')->on('tbl_consultas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_cliente', 'id_cliente')
                ->references('id')->on('tbl_clientes')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_citas_estado', 'id_cita_estado')
                ->references('id')->on('tbl_citas_estados')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
