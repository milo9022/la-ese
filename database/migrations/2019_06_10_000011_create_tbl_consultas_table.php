<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblConsultasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tbl_consultas';

    /**
     * Run the migrations.
     * @table tbl_consultas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre', 200);
            $table->integer('id_consulta_tipo');

            $table->index(["id_consulta_tipo"], 'id_consulta_tipo');
            $table->nullableTimestamps();

/*
            $table->foreign('id_consulta_tipo', 'id_consulta_tipo')
                ->references('id')->on('tbl_consultas_tipos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
