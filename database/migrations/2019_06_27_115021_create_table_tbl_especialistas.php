<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblEspecialistas extends Migration
{
    public function up()
    {
        Schema::create('tbl_especialistas', function (Blueprint $table) {
            $this->down();
            $table->increments('id');
            $table->string('nombre_primero');
            $table->string('nombre_segundo')->nullable()->default(null);
            $table->string('apellido_primero');
            $table->string('apellido_segundo')->nullable()->default(null);
            $table->date('fecha_nacimiento')->nullable()->default(null);
            $table->string('documento'); 
            $table->integer('id_documento_tipo')->nullable()->default(null);
            $table->string('celular1')->nullable()->default(null);
            $table->string('celular2')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_especialistas');
    }
}
