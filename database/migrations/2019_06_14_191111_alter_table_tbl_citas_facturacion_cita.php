<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTblCitasFacturacionCita extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_citas', function (Blueprint $table) {
            $table->integer('id_facturacion_tipo')->nullable();
            $table->integer('id_profesional_asistencial')->nullable();
            $table->integer('id_consultorios')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('tbl_citas');
    }
}
