<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblClientesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tbl_clientes';

    /**
     * Run the migrations.
     * @table tbl_clientes
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre_primero');
            $table->string('nombre_segundo')->nullable()->default(null);
            $table->string('apellido_primero');
            $table->string('apellido_segundo')->nullable()->default(null);
            $table->date('fecha_nacimiento')->nullable()->default(null);
            $table->integer('id_documento_tipo')->nullable()->default(null);
            $table->string('documento');
            $table->integer('id_eps')->nullable()->default(null);
            $table->string('celular1')->nullable()->default(null);
            $table->string('celular2')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);

            $table->index(["id_documento_tipo"], 'id_documento_tipo');

            $table->index(["id_eps"], 'id_eps');
            $table->nullableTimestamps();

/*
            $table->foreign('id_documento_tipo', 'id_documento_tipo')
                ->references('id')->on('tbl_documento_tipos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_eps', 'id_eps')
                ->references('id')->on('tbl_eps')
                ->onDelete('restrict')
                ->onUpdate('restrict');
*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
