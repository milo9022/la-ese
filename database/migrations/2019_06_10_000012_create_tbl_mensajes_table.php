<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMensajesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'tbl_mensajes';

    /**
     * Run the migrations.
     * @table tbl_mensajes
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_cliente')->nullable()->default(null);
            $table->string('destino', 200)->nullable()->default(null)->comment('puede ser el numero telefonico o el email');
            $table->text('mensaje')->nullable()->default(null);
            $table->integer('id_mensaje_tipo')->nullable()->default(null);

            $table->index(["id_mensaje_tipo"], 'id_mensaje_tipo');
            $table->nullableTimestamps();

/*
            $table->foreign('id_mensaje_tipo', 'id_mensaje_tipo')
                ->references('id')->on('tbl_mensajes_tipos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
                */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
