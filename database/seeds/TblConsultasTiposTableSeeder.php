<?php

use Illuminate\Database\Seeder;

class TblConsultasTiposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_consultas_tipos')->delete();
        
        \DB::table('tbl_consultas_tipos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Consultas',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Programas',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}