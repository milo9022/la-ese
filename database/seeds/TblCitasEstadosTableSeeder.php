<?php

use Illuminate\Database\Seeder;

class TblCitasEstadosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_citas_estados')->delete();
        
        \DB::table('tbl_citas_estados')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Pendiente',
                'orden' => 1,
                'color' => '#777777',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Confirmada',
                'orden' => NULL,
                'color' => '#337ab7',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Rechazada',
                'orden' => NULL,
                'color' => '#f0ad4e',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Canceladas',
                'orden' => NULL,
                'color' => '#d9534f',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}