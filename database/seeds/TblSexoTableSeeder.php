<?php

use Illuminate\Database\Seeder;

class TblSexoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_sexo')->delete();
        
        \DB::table('tbl_sexo')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Masculino',
                'nombre_corto' => 'M',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Femenino',
                'nombre_corto' => 'F',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}