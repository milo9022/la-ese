<?php

use Illuminate\Database\Seeder;

class TblConsultasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_consultas')->delete();
        
        \DB::table('tbl_consultas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Consulta externa',
                'id_consulta_tipo' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Odontología',
                'id_consulta_tipo' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Medicamentos NO-POS',
                'id_consulta_tipo' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Insumos',
                'id_consulta_tipo' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Tutelas',
                'id_consulta_tipo' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Crecimiento Y desarrollo',
                'id_consulta_tipo' => 1,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'Programa HTA',
                'id_consulta_tipo' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'Programa gestantes',
                'id_consulta_tipo' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'Programa planificación familiar',
                'id_consulta_tipo' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'Programa adulto mayor',
                'id_consulta_tipo' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'Programa joven sano',
                'id_consulta_tipo' => 2,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => '1232',
                'id_consulta_tipo' => 1,
                'created_at' => '2019-06-13 12:45:56',
                'updated_at' => '2019-06-13 12:46:08',
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => '34',
                'id_consulta_tipo' => 2,
                'created_at' => '2019-06-13 13:01:25',
                'updated_at' => '2019-06-13 13:01:25',
            ),
        ));
        
        
    }
}