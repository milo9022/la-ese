<?php

use Illuminate\Database\Seeder;

class TblEspecialidadTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_especialidad')->delete();
        
        \DB::table('tbl_especialidad')->insert(array (
            0 => 
            array (
                'id' => 78,
                'nombre' => 'GINECOLOGIA Y OBSTETRICIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 82,
                'nombre' => 'LABORATORIO CLINICO ',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 176,
                'nombre' => 'MEDICO GENERAL',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 177,
                'nombre' => 'ODONTOLOGO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 178,
                'nombre' => 'ENFERMERA JEFE ',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 179,
                'nombre' => 'AUXILIAR ENFERMERIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 180,
                'nombre' => 'BACTERIOLOGO',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 182,
                'nombre' => 'AUXILIAR VACUNACION',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 183,
                'nombre' => 'AUXILIAR ODONTOLOGIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 184,
                'nombre' => 'HIGIENISTA_ORAL',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 191,
                'nombre' => 'PSICOTERAPITA INDIVIDUAL POR PSICOLOGIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 193,
                'nombre' => 'PSICOTERAPIA PSICOLOGICA FAMILIAR',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 196,
                'nombre' => 'FISIOTERAPIA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 199,
                'nombre' => 'HISTORIACLINICA',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}