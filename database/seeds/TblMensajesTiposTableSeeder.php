<?php

use Illuminate\Database\Seeder;

class TblMensajesTiposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_mensajes_tipos')->delete();
        
        \DB::table('tbl_mensajes_tipos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'sms',
                'maximo' => 200,
                'html' => 'false',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'email',
                'maximo' => NULL,
                'html' => 'true',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}