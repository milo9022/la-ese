<?php

use Illuminate\Database\Seeder;

class TblFacturacionTipoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_facturacion_tipo')->delete();
        
        \DB::table('tbl_facturacion_tipo')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'PyP',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Recuperación',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Otros',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}