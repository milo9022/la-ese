<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(TblClientesTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TblCitasEstadosTableSeeder::class);
        $this->call(TblConfigTableSeeder::class);
        $this->call(TblConsultasTiposTableSeeder::class);
        $this->call(TblConsultasTableSeeder::class);
        $this->call(TblDepartamentosTableSeeder::class);
        $this->call(TblDocumentoTiposTableSeeder::class);
        $this->call(TblFacturacionTipoTableSeeder::class);
        $this->call(TblMensajesTiposTableSeeder::class);
        $this->call(TblMunicipiosTableSeeder::class);
        $this->call(TblSexoTableSeeder::class);
        $this->call(TblEspecialidadTableSeeder::class);
        $this->call(TblPuntosAtencionesTableSeeder::class);
        $this->call(TblEspecialistasTableSeeder::class);
    }
}
