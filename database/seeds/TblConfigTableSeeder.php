<?php

use Illuminate\Database\Seeder;

class TblConfigTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_config')->delete();
        
        \DB::table('tbl_config')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'mensaje-programas',
                'value' => 'Los usuarios que pertenecen a los programas por favor acercarse a su punto de atencion para solicitar su cita medica.',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'codigo_cita',
                'value' => '7710',
                'created_at' => NULL,
                'updated_at' => '2019-12-17 13:34:21',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'mensaje-sms',
                'value' => '',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'mensaje-email',
            'value' => '<p><strong>Usuario(a) </strong></p>

<p>{nombre_completo}</p>

<p>&nbsp;</p>

<p><strong>ASUNTO: ASIGNACION DE CITA</strong></p>

<p>&nbsp;</p>

<p>Le informamos que hemos recibido una solicitud con los siguientes datos:</p>

<p>&nbsp;</p>

<p>C&radic;&ge;digo de solicitud: COD______</p>

<p>Tipo de documento: {documento_tipo_nombre}</p>

<p>N&radic;&int;mero de documento:{documento}</p>

<p>Tipo de consulta: __________________</p>

<p>Fecha deseada para asignaci&radic;&ge;n de cita:{fecha_cita}</p>

<p>&nbsp;</p>

<p>Su cita ha sido asignada para el d&radic;&ne;a {fecha_cita} a la(s) _________ en el punto de atenci&radic;&ge;n {punto_atencion}, con el profesional asistencial ____________, por favor presentarse 30 minutos antes en facturaci&radic;&ge;n con documento original. En caso de querer cancelar la cita informar al 3234821334 con m&radic;&ne;nimo 2 horas de anticipaci&radic;&ge;n.</p>

<p>&nbsp;</p>

<p>Si tiene alguna inquietud respecto a la informaci&radic;&ge;n contenida en este correo, debe acercarse personalmente al centro de salud de su atenci&radic;&ge;n.</p>

<p>&nbsp;</p>

<p>Atentamente;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p><img src="../../img/logo.jpg" style="height:179px; width:179px" /></p>

<p><strong>ASIGNACION DE CITAS</strong></p>

<p><strong>Empresa Social del Estado Popayan ESE</strong></p>

<p>Te: 8333000 &sbquo;&Auml;&igrave; Ext. 1 Cel: 3234821334</p>

<p>Correo electr&radic;&ge;nico: citasesepopayan@gmail.com</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>',
                'created_at' => NULL,
                'updated_at' => '2019-12-16 16:09:52',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'captcha-public',
                'value' => '6LeRtawUAAAAAIOwcDnkZEqwrbhLwL35nolNNYwj',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'captcha-private',
                'value' => '6LeRtawUAAAAAGPThiRwC3nvvY3wLGxQr5Cr33Ld',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'email',
                'value' => 'citas@esepopayan.gov.co',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'PermiteRegistro',
                'value' => 'si',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'desabilitado',
                'value' => '0',
                'created_at' => NULL,
                'updated_at' => '2019-11-05 11:51:17',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'smsTotal',
                'value' => '160',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'smsCaracteresValidos',
            'value' => '[" ","!","\\"","#","$","%","&","\'","(",")","*","+",",","-",".","\\/","0","1","2","3","4","5","6","7","8","9",":",";","<","=",">","?","@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","[","\\\\","]","^","_","`","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","{","|","}","~","\\n"]',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}