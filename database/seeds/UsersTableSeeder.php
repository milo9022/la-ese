<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $user = new User();
        $user->id = 1;
        $user->nombre_primero = 'admin';
        $user->nombre_segundo = 'admin';
        $user->apellido_primero = 'admin';
        $user->apellido_segundo = 'admin';
        $user->documento = '1';
        $user->id_punto_atencion = 1;
        $user->activo = 1;
        $user->email = 'admin@admin.com';
        $user->password = bcrypt('admin');
        $user->created_at = date('Y-m-d h:i:s');
        $user->save();
        $user->roles()->attach(Role::where('name', 'admin')->first()); 
    }
}