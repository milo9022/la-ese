<?php

use Illuminate\Database\Seeder;

class TblPuntosAtencionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_puntos_atenciones')->delete();
        
        \DB::table('tbl_puntos_atenciones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'CENTRO DE SALUD SUR OCCIDENTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'HOSPITAL MARIA OCCIDENTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'CENTRO DE SALUD LOMA DE LA VIRGEN',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'CENTRO DE SALUD SURORIENTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'CENTRO DE SALUD YANACONAS',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'CENTRO DE SALUD 31 MARZO',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'HOSPITAL DEL NORTE TORIBIO MAYA',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'CENTRO DE SALUD PUEBLILLO',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'CENTRO DE SALUD BELLO HORIZONTE',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'GRUPO EXTRAMURAL1',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'GRUPO EXTRAMURAL2',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'UNIDAD MOVIL ODONTOLOGICA',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'GRUPO EXTRAMURAL3',
                'id_municipio' => 236,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}