@extends('layouts.app')
@section('content')
<div class="panel panel-default">

<div class="panel-heading clearfix">
    
    <span class="pull-left">
        <h4 >Carga de pacientes</h4>
    </span>

    <div class="btn-group btn-group-sm pull-right" role="group">
        <a href="{{ route('tbl_citas.tbl_citas.index') }}" class="btn btn-primary" title="Show All Citas">
            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
        </a>
    </div>

</div>

<div class="panel-body">

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form method="POST" class="form-horizontal" id="formuploadajax">
        <div class="col-md-12">
        <label>Cargas pacientes</label>
        <label for="">Archivo</label>
        <h4>Orden de los datos</h4>
        0 => "ID_PACIENTE"
        <br>1 => "COD_TID"
        <br>2 => "IDE_PAC"
        <br>3 => "AP1_PAC"
        <br>4 => "AP2_PAC"
        <br>5 => "NM1_PAC"
        <br>6 => "NM2_PAC"
        <br>7 => "FNC_PAC"
        <br>8 => "SEX_PAC"(M,F)
        <input type="file" name="file" id="file" class="form form-control">
        <input type="hidden" id="idload" name="idload">
        <button class="btn btn-success">Enviar</button>
        </div>
    </form>

</div>
<script>
var cicle=false;
$(function()
{
    $('form').submit(function(e){
        e.preventDefault();
        $.ajax({
            url:base+'/carga/idload',
            dataType:'json',
            success:function(response)
            {
                $('#idload').val(response.id);
                var formData = new FormData(document.getElementById("formuploadajax"));
                //cicle=true;
                setInterval(()=>{
                    if(cicle)
                    {
                        $.ajax({
                            url:base+'/carga/status',
                            dataType:'json',
                            type:'POST',
                            data:{idload:response.id},
                            success:function(response)
                            {
                                console.clear();
                                console.log(response);
                            }
                        });
                    }
                },3000);
                $.ajax({
                    url:base+'/carga',
                    type:'POST',
                    dataType: "json",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success:function(response)
                    {
                        cicle=false;
                        swal({title: "Éxito",text: "Se ha subido con éxito"})
                    },
                    error:function(response){
                        swal({title: "Error",text: "Error"})
                        console.log(response);
                        cicle=false;
                    }
                })

            }
        })
    })

})
</script>
</div>
@endsection