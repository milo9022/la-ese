@extends('layouts.app')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.12.1/ckeditor.js"></script>
<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 >Mensaje de email</h4>
        </span>

        <div class="pull-right">
        <a href="{{ route('tbl_mensajes.tbl_mensajes.index') }}" class="btn btn-primary" title="Show All Tbl Mensajes">
            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
        </a>
        </div>

    </div>

    <div class="panel-body">
    <form action="{{url('/configuracion/enviaremailSave')}}" method="post" >
    <div class="col-md-6">
        <h2>Mensaje de asignacion</h2>
        <textarea  class="form form-control" name="mensaje" id="mensaje" rows="30">{{$data}}</textarea>
    </div>
    <div class="col-md-6">
        <h2>Mensaje de cancelacion</h2>
        <textarea  class="form form-control" name="mensaje2" id="mensaje2" rows="30">{{$data2}}</textarea>
    </div>
        {{ csrf_field() }}
        <br>
        <button class="btn btn-success">
            Guardar cambios
        </button>
    </form>
</div>
<script>
CKEDITOR.replace('mensaje', {height: 650, language: 'es'});
CKEDITOR.replace('mensaje2', {height: 650, language: 'es'});
</script>
@endsection