@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 >{{ isset($title) ? $title : 'Mensaje' }}</h4>
        </span>

        <div class="pull-right">
        <a href="{{ route('tbl_mensajes.tbl_mensajes.index') }}" class="btn btn-primary" title="Show All Tbl Mensajes">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>
        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Cliente</dt>
            <dd>
            {{$tblMensajes->TblClientes->apellido_primero}}
            {{$tblMensajes->TblClientes->apellido_segundo}}
            {{$tblMensajes->TblClientes->nombre_primero}}
            {{$tblMensajes->TblClientes->nombre_segundo}}
            </dd>
            <dt>Destino</dt>
            <dd>{{ $tblMensajes->destino }}</dd>

            <dt>Usuario que envia</dt>
            <dd>
            {{$tblMensajes->Users->apellido_primero}}
            {{$tblMensajes->Users->apellido_segundo}}
            {{$tblMensajes->Users->nombre_primero}}
            {{$tblMensajes->Users->nombre_segundo}}
            </dd>
            <dt>Mensaje Tipo</dt>
            <dd>{{ optional($tblMensajes->TblMensajesTipo)->nombre }}</dd>
            
            <dt>Fecha de envio</dt>
            <dd>{{ date('Y-m-d h:i:s A',strtotime($tblMensajes->created_at)) }}</dd>
            @if(isset($tblMensajes->respuesta->validate))
            <dt>Respuesta del<br>servidor</dt>
            <dd>
            <label class="{{!$tblMensajes->respuesta->validate?'label label-danger':''}}">{!!trim($tblMensajes->respuesta->msj)!!}</label>
            </dd>
            @endif
            @if($tblMensajes->id_mensaje_tipo==1)
            <dt>Mensaje</dt>
            <dd>{{ $tblMensajes->mensaje }}</dd>
            <dt>Estado</dt>
            <dd>
            <?php
                $code='';
                if(isset($tblMensajes->respuesta->resultCode))
                {
                    if($tblMensajes->respuesta->resultCode=='0')
                    {
                        $code='<span class="label label-success">Mensaje enviado con &#233;xito</span>';
                    }
                    else{
                        
                        $code='<span class="label label-danger">No se ha podido enviar el mensaje</span>';
                    }
                }
                else{
                    $code='<span class="label label-warning">Se present&#243 un error. No se sabe el estado del mensaje.</span>';
                }
                echo $code;
            ?>
            </dd>


            @endif

        </dl>
            @if($tblMensajes->id_mensaje_tipo==2)
                <strong>Mensaje</strong><br/>
                {!! $tblMensajes->mensaje !!}
            @endif
    </div>
</div>

@endsection