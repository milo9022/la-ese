@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >Mensajes</h4>
            </div>
        </div>
        
        @if(count($tblMensajesObjects) == 0)
            <div class="panel-body text-center">
                <h4>No se han enviado mensajes.</h4>
            </div>
        @else
        <div>
        <div class="panel-body panel-body-with-table">
            <form action="">
                <div class="row">
                    <div class="col-md-3">
                        <label>Tipo de mensaje</label>
                        <select class="form form-control" name="tipo" id="tipo">
                            <option value=""{{  app('request')->input('tipo')== ''?' selected':''}}>Todos</option>
                            <option value="2"{{ app('request')->input('tipo')=='2'?' selected':''}}>Email</option>
                            <option value="1"{{ app('request')->input('tipo')=='1'?' selected':''}}>SMS</option>
                        </select>
                    </div>
                    <div class="col-md-9">
                    <br>
                    <button class="btn btn-primary">Filtrar</button>
                    </div>
                </div>
            </form>

        </div>
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>usuario</th>
                            <th>Tipo de mensaje</th>
                            <th>Destino</th>
                            <th>fecha de envio</th>
                            <th>Ver mensaje</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tblMensajesObjects as $tblMensajes)
                        <tr>
                            <td>
                            {{optional($tblMensajes->TblClientes)->apellido_primero}}
                            {{optional($tblMensajes->TblClientes)->apellido_segundo}}
                            {{optional($tblMensajes->TblClientes)->nombre_primero}}
                            {{optional($tblMensajes->TblClientes)->nombre_segundo}}
                            
                            </td>
                            <td>
                            {{optional($tblMensajes->Users)->apellido_primero}}
                            {{optional($tblMensajes->Users)->apellido_segundo}}
                            {{optional($tblMensajes->Users)->nombre_primero}}
                            {{optional($tblMensajes->Users)->nombre_segundo}}
                            </td>
                            <td>{{ $tblMensajes->TblMensajesTipo->nombre }}</td>
                            <td>{{ $tblMensajes->destino }}</td>
                            <td>{{ $tblMensajes->created_at }}</td>
                            <td>
                                    <?php
                                        $code='btn-info';
                                        if($tblMensajes->TblMensajesTipo->id==1)
                                        {
                                            if(isset(json_decode($tblMensajes->respuesta)->resultCode))
                                            {
                                                if(json_decode($tblMensajes->respuesta)->resultCode!='0')
                                                {
                                                    $code='btn btn-danger';
                                                }
                                            }
                                            else{
                                                $code='btn btn-default';
                                            }
                                        }
                                        ?>
                                    <a href="{{ route('tbl_mensajes.tbl_mensajes.show', $tblMensajes->id ) }}" class="btn {{$code}}" title="Show Tbl Mensajes">
                                    @if($tblMensajes->TblMensajesTipo->id==1)
                                        <i class="fas fa-sms"></i>
                                    @else
                                        <i class="fas fa-envelope-square"></i>
                                    @endif
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                {{ $tblMensajesObjects->appends(request()->except('page'))->links() }}
	</div>
            </div>
        </div>        
        @endif
    
    </div>
@endsection