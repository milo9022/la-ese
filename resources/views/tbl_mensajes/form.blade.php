
<div class="form-group {{ $errors->has('id_cliente') ? 'has-error' : '' }}">
    <label for="id_cliente" class="col-md-2 control-label">Id Cliente</label>
    <div class="col-md-10">
        <input class="form-control" name="id_cliente" type="number" id="id_cliente" value="{{ old('id_cliente', optional($tblMensajes)->id_cliente) }}" min="-2147483648" max="2147483647" "Enter id cliente here...">
        {!! $errors->first('id_cliente', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('destino') ? 'has-error' : '' }}">
    <label for="destino" class="col-md-2 control-label">Destino</label>
    <div class="col-md-10">
        <input class="form-control" name="destino" type="text" id="destino" value="{{ old('destino', optional($tblMensajes)->destino) }}" maxlength="200" "Enter destino here...">
        {!! $errors->first('destino', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('mensaje') ? 'has-error' : '' }}">
    <label for="mensaje" class="col-md-2 control-label">Mensaje</label>
    <div class="col-md-10">
        <textarea class="form-control" name="mensaje" cols="50" rows="10" id="mensaje" "Enter mensaje here...">{{ old('mensaje', optional($tblMensajes)->mensaje) }}</textarea>
        {!! $errors->first('mensaje', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_mensaje_tipo') ? 'has-error' : '' }}">
    <label for="id_mensaje_tipo" class="col-md-2 control-label">Id Mensaje Tipo</label>
    <div class="col-md-10">
        <select class="form-control" id="id_mensaje_tipo" name="id_mensaje_tipo">
        	    <option value="" style="display: none;" {{ old('id_mensaje_tipo', optional($tblMensajes)->id_mensaje_tipo ?: '') == '' ? 'selected' : '' }} disabled selected>Enter id mensaje tipo here...</option>
        	@foreach ($TblMensajesTipos as $key => $TblMensajesTipo)
			    <option value="{{ $key }}" {{ old('id_mensaje_tipo', optional($tblMensajes)->id_mensaje_tipo) == $key ? 'selected' : '' }}>
			    	{{ $TblMensajesTipo }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_mensaje_tipo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

