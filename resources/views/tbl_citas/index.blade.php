@extends('layouts.app')
@section('content')
<script>
function addCommasæ(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function detalle(id)
{	
	$.ajax({
		url:base+'/citas/detalle',
		type:'POST',
		data:{id:id},
		dataType:'json',
		success:function(data)
		{
			if(data.cancelacion_motivo==null)
			{
				$('#cancelacion_motivo_item').hide();
			}
			else
			{
				$('#cancelacion_motivo_item').show();
			}
			$.each(data,function(index,value)
			{
				$('#'+index).html($.trim(value)==''?'<i class="label label-danger">sin registro</label>':value);
			})
			if($.trim($('#cliente_fecha_nacimiento').val())!='')
			{
			$.ajax({
				url:base+'/edad/'+$('#cliente_fecha_nacimiento').val(),
				dataType:'json',
				success:function(data)
				{
					$('#edad').html(
						data.y+' años'+
						data.m+' meses'+
						data.d+' dias'

					)
				}
			})
			}
			else{
				$('#edad').html('<i class="label label-danger">sin registro</label>');
			}
			$('#myModal').modal('show');
		}
	})
}
function buscar()
{
	$('#table').dataTableServerSide({
		url:base+'/citas/All/'+$('#filtro').val()+'/',
		columns:[
				{ 
					data: 'id',
					"render": function(data,data2,row) 
					{
						var html ='<div class="dropdown">';
						html +='	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">';
						html +='Opciones';
						html +='<span class="caret"></span>';
						html +='</button>';
						html +='<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
						if(row.id_citas_estado==1)
						{
							//console.log({user:JSON.parse("{!!Auth::user()->hasRole('supervision')!!}")})
							@if(Auth::user()->hasRole('admin') ||Auth::user()->hasRole('facturador'))
							html +='	<li>';
							html +='		<a href="'+base+'/citas/'+data+'/edit">';
							html +='			<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar';
							html +='		</a>';
							html +='	</li>';
							@endif
							console.log({value:"{!! Auth::user()->hasRole('admin') || Auth::user()->hasRole('facturador') || Auth::user()->hasRole('supervision') !!}"})
							console.log($.trim(row.cliente))							
							if($.trim(row.cliente)=='')
							{
								@if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('facturador') || Auth::user()->hasRole('supervision'))
								console.log({mostrar:row})
								html +='	<li>';
								html +='		<a href="'+base+'/clientes/'+row.cliente_id+'/edit">';
								html +='			<i class="fas fa-user-plus"></i> Registrar cliente';
								html +='		</a>';
								html +='	</li>';

								@endif
							}
							else
							{
								
								html +='	<li>';
								html +='		<a href="'+base+'/citas/asignar/'+data+'">';
								html +='			<i class="far fa-calendar-alt"></i> Asignar cita';
								html +='		</a>';
								html +='	</li>';
							}
						}
						

						html +='	<li>';
						html +='		<a href="javascript:detalle('+data+')" style="width: 100%;text-align: -webkit-left;padding-left: 20px;background-color: transparent;border-color: transparent;" >';
						html +='		<i class="fas fa-plus"></i> Ver detalles';
						html +='		</a>';
						html +='	</li>';
						html +='</ul>';
						html +='</div>';
						return html;
					}
				},
				{ data: "codigo"},
                { data: "punto_atencion" },
				{ data: "fecha" },
                { data: "hora" },
				@if(Auth::user()->hasRole('admin') ||Auth::user()->hasRole('supervision'))
								
                { data: "created",render:function(data,data2,row)
	                {
	                	return (data);
	                }
	            },
				@endif

                { data: "consulta"},
                { data: "cliente","render": function(data,data2,row) 
					{
						var html=row.documento_tipo_nombre_corto+':'+(row.documento)+'<br>';
						if($.trim(data)=='')
						{
							@if(Auth::user()->hasRole('admin') ||Auth::user()->hasRole('facturador'))
							html+= '<a href="'+base+'/clientes/'+row.cliente_id+'/edit" class="label label-danger">Cliente sin registrar</a>'
							@else
							@endif
							
						}
						else
						{
							html+= data;
						}
						return html;

					}
				},
                { data: "especialista" ,"render": function(data) 
					{
						if($.trim(data)=='')
						{
							html= '<label class="label label-danger">Especialista no registrado</label>'
						}
						else
						{
							html= data;
						}
						return html;

					} },
                { data: "facturacion_tipo" ,render: function(data,row,raw) 
					{
						return data
					}
				},
                { data: "cita_estado",render: function(data,row,raw) 
					{
						return '<label class="label" style="background-color:'+raw.cita_estado_color+'">'+data+'</label>'
					}
				},
            ]
	});
}
$(function()
{
	buscar();
	$('#filtro').change(function()
	{
		buscar();
	});
});
</script>
@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
@if (session('danger'))
    <div class="alert alert-danger">
        {{ session('danger') }}
    </div>
@endif


<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<div class="pull-left">
			<h4 >Citas</h4>
		</div>
		@if(Auth::user()->hasRole('admin') ||Auth::user()->hasRole('supervision'))
		<div class="btn-group btn-group-sm pull-right" role="group">
			<a href="{{ route('tbl_citas.tbl_citas.create') }}" class="btn btn-success" title="Crear  Citas">
			<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			</a>
		</div>
		@endif
	</div>
	<div class="panel-body panel-body-with-table">
		@if(Auth::user()->hasRole('facturador'))
		<input type="hidden" id="filtro" value="1">
		@else
		<div class="row">
			<div class="col-md-3">
				<label for="">Seleccione un estado</label>
				<select class="form-control" id="filtro">
					<option value="-1">Todos</option>
					@foreach($TblCitasEstado as $key=> $TblCitasEstados)
					<option value="{{$TblCitasEstados->id}}" {{($key=='0' && Auth::user()->hasRole('facturador'))?'selected':''}}>{{$TblCitasEstados->nombre}}</option>
					@endforeach
				</select>
			</div>
		</div>
		@endif
		<div>
			<table id="table" class="table table-striped ">
				<thead>
					<tr>
						<th>Opciones</th>
						<th>Codigo</th>
						<th>Punto de atenci&#243n</th>
						<th>Fecha de solicitud de cita</th>
						<th>Hora de la cita</th>
						@if(Auth::user()->hasRole('admin') ||Auth::user()->hasRole('supervision'))
						<th>Fecha de creacion</th>
						@endif
						<th>Consulta</th>
						<th>Cliente</th>
						<th>Profesional<br>asistencial</th>
						<th>Tipo facturaci&#243n</th>
						<th>Estado</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Detalle</h4>
			</div>
			<div class="modal-body">
				<table>
					<tr>
						<td>Estado de la cita</td>
						<td><label id="cita_estado"></label></td>
					</tr>
					<tr>
						<td>codigo</td>
						<td><label id="codigo"></label></td>
					</tr>
					<tr>
						<td>Cliente</td>
						<td><label id="cliente"></label></td>
					</tr>
					<tr>
						<td>Celular 1</td>
						<td><label id="cliente_celular1"></label></td>
					</tr>
					<tr>
						<td>Celular 2</td>
						<td><label id="cliente_celular2"></label></td>
					</tr>
					<tr>
						<td>Documento</td>
						<td><label id="cliente_documento"></label></td>
					</tr>
					<tr>
						<td>Email</td>
						<td><label id="cliente_email"></label></td>
					</tr>
					<tr>
						<td>Fecha de nacimiento</td>
						<td><label id="cliente_fecha_nacimiento"></label></td>
					</tr>
					<tr>
						<td>Edad</td>
						<td><label id="edad"></label></td>
					</tr>
					<tr>
						<td>Consulta</td>
						<td><label id="consulta"></label></td>
					</tr>
					<tr>
						<td>Consultorio</td>
						<td><label id="consultorio"></label></td>
					</tr>
					<tr>
						<td>EPS</td>
						<td><label id="eps"></label></td>
					</tr>
					<tr>
						<td>Especialista</td>
						<td><label id="especialista"></label></td>
					</tr>
					<tr>
						<td>Documento del especialista</td>
						<td><label id="especialista_documento"></label></td>
					</tr>
					<tr>
						<td>Tipo de facturacion</td>
						<td><label id="facturacion_tipo"></label></td>
					</tr>
					<tr>
						<td>Motivo de consulta</td>
						<td><label id="motivoconsulta"></label></td>
					</tr>
					<tr>
						<td>Punto de atenci&#243n</td>
						<td><label id="punto_atencion"></label></td>
					</tr>
					<tr id="cancelacion_motivo_item">
						<td>Motivo de cancelaci&#243n</td>
						<td><label id="cancelacion_motivo"></label></td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>
@endsection