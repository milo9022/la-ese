@extends('layouts.app')
@section('content')

@foreach($tblCitass as $tblCitas)

<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<span class="pull-left">
			<h4 >{{ isset($title) ? $title : 'Citas' }} {{$tblCitas->codigo}} </h4>
		</span>
		<div class="pull-right">
				<input name="_method" value="DELETE" type="hidden">
				{{ csrf_field() }}
		</div>
	</div>
        {{ csrf_field() }}
	<div class="panel-body">
        <div class="col-md-6">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4">
						<label>Cliente</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->tblcliente->nombre_primero}}
						{{$tblCitas->tblcliente->nombre_segundo}}
						{{$tblCitas->tblcliente->apellido_primero}}
						{{$tblCitas->tblcliente->apellido_segundo}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Tipo de documento</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->TblCliente->TblDocumentoTipos->nombre}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Documento</label>
					</div>
					<div class="col-md-8">
						{{($tblCitas->tblcliente->documento)}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Fecha de nacimiento</label>
					</div>
					<div class="col-md-8">
						@if(is_null($tblCitas->tblcliente->fecha_nacimiento))
						<label class="label label-danger">No registrado</label>
						@else
						{{$tblCitas->tblcliente->fecha_nacimiento}} - {{((new DateTime())->diff(new DateTime($tblCitas->tblcliente->fecha_nacimiento)))->y}} años
					</div>
					@endif
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Numero de contacto</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->tblcliente->celular1}} {{$tblCitas->tblcliente->celular2}}
					</div>
				</div>
				@if(!is_null($tblCitas->tblcliente->email))
				<div class="row">
					<div class="col-md-4">
						<label>Correo electronico</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->tblcliente->email}}
					</div>
				</div>
				@endif
				<div class="row">
					<div class="col-md-4">
						<label>EPS</label>
					</div>
					<div class="col-md-8">
						{{isset($tblCitas->TblCliente->TblEps->descripcion)?$tblCitas->TblCliente->TblEps->descripcion:'No registrado'}}
					</div>
				</div>
			</div>
		</div>
		
        <div class="col-md-6">
			<?php //echo '<pre>';var_dump($tblCitas);echo '</pre>';?>
			<div class="container-fluid">
				<div class="row">
					<label class="col-md-4">C&#243digo</label>
					<div class="col-md-8">
						{{$tblCitas->codigo}}
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Motivo de la consulta</label>
					<div class="col-md-8">
						@if(trim($tblCitas->motivoconsulta)=='')
						<label class="label label-danger">Motivo no registrado</label>
						@else
						{{$tblCitas->motivoconsulta}}
						@endif
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Consulta</label>
					<div class="col-md-8">
						{{ $tblCitas->TblConsultas->nombre }}
					</div>
				</div>
                <div class="row">
					<label class="col-md-4">Estado de la cita</label>
					<div class="col-md-8"><label class="label" style="background-color:{{$tblCitas->tblcitasestado->color}}">{{ $tblCitas->tblcitasestado->nombre }}</label></div>
				</div>
				<div class="row">
					<label class="col-md-4">Fecha</label>
					<div class="col-md-8">
						<strong>{{ $tblCitas->fecha }}</strong>
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Hora</label>
					<div class="col-md-8">
						<strong>{{$tblCitas->hora}}</strong>
					</div>
				</div>
                <div class="row">
                    <label class="col-md-4">Profesional</label>
                    <div class="col-md-8">
						<strong>
						{{  optional($tblCitas->TblEspecialistas)->nombre_primero }}
						{{  optional($tblCitas->TblEspecialistas)->nombre_segundo }}
						{{  optional($tblCitas->TblEspecialistas)->apellido_primero }}
						{{  optional($tblCitas->TblEspecialistas)->apellido_segundo }}
						</strong>
                    </div>
                </div>
                <div class="row">
					<label class="col-md-4">Consultorios</label>
					<div class="col-md-8">
						<strong>
						{{old('TblConsultorios',optional($tblCitas->TblConsultorios)->nombre) }}
						</strong>
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Tipo de facturacion</label>
					<div class="col-md-8">
						<strong>
						{{  optional($tblCitas->TblFacturacionTipo)->nombre }}
						</strong>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-6">
            <label for="">Mensaje de texto (SMS)</label><br/>
			@foreach($sms as $temp)
            <strong>{!! $temp->mensaje !!}</strong><br>
			@endforeach
        </div>
        <div class="col-md-12">
            <label for="">Enviar correo electronico</label><br/>
			@foreach($email as $temp)
            {!! $temp->mensaje !!}<hr/>
			@endforeach
        </div>
	</div>
</div>
@endforeach


@endsection