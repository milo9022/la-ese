@extends('layouts.app')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.12.1/ckeditor.js"></script>
<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<span class="pull-left">
			<h4 >{{ isset($title) ? $title : 'Citas' }}</h4>
		</span>
		<div class="pull-right">
			<form method="POST" action="{!! route('tbl_citas.tbl_citas.destroy', $tblCitas->id) !!}" accept-charset="UTF-8">
				<input name="_method" value="DELETE" type="hidden">
				{{ csrf_field() }}
				<div class="btn-group btn-group-sm" role="group">
					<a href="{{ route('tbl_citas.tbl_citas.index') }}" class="btn btn-primary" title="Show All Citas">
					<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
					</a>
					<a href="{{ route('tbl_citas.tbl_citas.create') }}" class="btn btn-success" title="Crear  Citas">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
					</a>
					<a href="{{ route('tbl_citas.tbl_citas.edit', $tblCitas->id ) }}" class="btn btn-primary" title="Edit Citas">
					<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
					</a>
				</div>
			</form>
		</div>
	</div>
		<form action="{{url('citas/asignar')}}/{{$tblCitas->id}}" method="post">
        {{ csrf_field() }}
	<div class="panel-body">
        <div class="col-md-6">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-4">
						<label>Cliente</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->tblcliente->nombre_primero}}
						{{$tblCitas->tblcliente->nombre_segundo}}
						{{$tblCitas->tblcliente->apellido_primero}}
						{{$tblCitas->tblcliente->apellido_segundo}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Tipo de documento</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->TblCliente->TblDocumentoTipos->nombre}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Documento</label>
					</div>
					<div class="col-md-8">
						{{($tblCitas->tblcliente->documento)}}
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Fecha de nacimiento</label>
					</div>
					<div class="col-md-8">
						@if(is_null($tblCitas->tblcliente->fecha_nacimiento))
						<label class="label label-danger">No registrado</label>
						@else
						{{$tblCitas->tblcliente->fecha_nacimiento}} - {{((new DateTime())->diff(new DateTime($tblCitas->tblcliente->fecha_nacimiento)))->y}} a&#241;os
						@endif
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label>Numero de contacto</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->tblcliente->celular1}} {{$tblCitas->tblcliente->celular2}}
					</div>
				</div>
				@if(!is_null($tblCitas->tblcliente->email))
				<div class="row">
					<div class="col-md-4">
						<label>Correo electronico</label>
					</div>
					<div class="col-md-8">
						{{$tblCitas->tblcliente->email}}
					</div>
				</div>
				@endif
				<div class="row">
					<div class="col-md-4">
						<label>EPS</label>
					</div>
					<div class="col-md-8">
						{{isset($tblCitas->TblCliente->TblEps->descripcion)?$tblCitas->TblCliente->TblEps->descripcion:'No registrado'}}
					</div>
				</div>
			</div>
		</div>
		
        <div class="col-md-6">
			<?php //echo '<pre>';var_dump($tblCitas);echo '</pre>';?>
			<div class="container-fluid">
				<div class="row">
					<label class="col-md-4">C&#243digo</label>
					<div class="col-md-8">
						{{$tblCitas->codigo}}
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Motivo de la consulta</label>
					<div class="col-md-8">
						@if(trim($tblCitas->motivoconsulta)=='')
						<label class="label label-danger">Motivo no registrado</label>
						@else
						{{$tblCitas->motivoconsulta}}
						@endif
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Consulta</label>
					<div class="col-md-8">
						{{ $tblCitas->TblConsultas->nombre }}
					</div>
				</div>
                <div class="row">
					<label class="col-md-4">Estado de la cita</label>
					<div class="col-md-8"><label class="label" style="background-color:{{$tblCitas->tblcitasestado->color}}">{{ $tblCitas->tblcitasestado->nombre }}</label></div>
				</div>
				<div class="row">
					<label class="col-md-4">Fecha</label>
					<div class="col-md-8">
						<input autocomplete="off" required type="text" name="fecha" id="fecha" class="mensajesChange fecha form form-control" value="{{ $tblCitas->fecha }}">
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Hora</label>
					<div class="col-md-8">
						<input required autocomplete="off" type="text" name="hora" id="hora" class="mensajesChange form form-control time" value="{{$tblCitas->hora}}" placeholder="{{ trim($tblCitas->hora)==''?'No asignada':'' }}">
					</div>
				</div>
                <div class="row">
                    <label class="col-md-4">Profesional</label>
                    <div class="col-md-8">
                        <input required type="text" id="especialistas" class="mensajesChange form-control" placeholder="documento o nombre">
                        <input type="hidden" id="id_especialistas" name="id_especialistas">
                    </div>
                </div>
                <div class="row">
					<label class="col-md-4">Consultorios</label>
					<div class="col-md-8">
						<select required name="id_consultorios" id="id_consultorios" class="form form-control">
							<option value="" style="display: none;" disabled selected>Seleccione un consultorio</option>
							@foreach($tblConsultorios as $tblConsultorio)
							<option value="{{$tblConsultorio->id}}" {{$tblCitas->id_consultorios==$tblConsultorio->id?'selected':''}}>{{$tblConsultorio->nombre}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<label class="col-md-4">Tipo de facturacion</label>
					<div class="col-md-8">
						<select required class="form form-control" name="id_facturacion_tipo" id="id_facturacion_tipo">
							<option value="" style="display: none;" disabled selected>Seleccione un tipo de facturacion</option>
							@foreach($tblFacturacionTipo as $tblFacturacionTipos)
							<option value="{{$tblFacturacionTipos->id}}" {{$tblCitas->id_facturacion_tipo==$tblFacturacionTipos->id?'selected':''}}>{{$tblFacturacionTipos->nombre}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
		</div>
        <div class="col-md-6">
            <label for=""> <button id="reloadsms" type="button" class="btn btn-success"><i class="fas fa-sync"></i></button> Mensaje de texto (SMS) Maximo {{$totalSMS}}</label>
			<span class="label label-danger" id="totalSms"></span>
            <textarea rows="5" max="{{$totalSMS}}" name="mensajesms" id="mensajesms" class="form form-control">{{(strip_tags($sms))}}</textarea>
        </div>
        <div class="col-md-12">
            <label for=""><button id="reloademail" type="button" class="btn btn-success"><i class="fas fa-sync"></i></button> Enviar correo electronico</label>
            <textarea id="enviarCorreo" COLS="10" name="mensajeemail" class="form form-control">{{$email}}</textarea>
        </div>
        

	</div>
    <div class="panel-footer">
        <button class="btn btn-primary" id="btn_guardar">
        	<i class="fas fa-save"></i> Guardar
        </button>
		<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal">
			<i class="fas fa-calendar-times"></i> Cancelar cita
		</button>
    </div>
        </form>
</div>
<!-- CANCELANDO CITA-->
<form action="{{url('citas/cancelar')}}/{{$tblCitas->id}}" method="post">
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Va a cancelar la cita</h4>
		</div>
		
			<div class="modal-body">
			<div class="container-fluid">
			<div class="col-md-12">
			{{ csrf_field() }}
			<label for="">Por favor ingrese el motivo de la cancelacion</label>
			<textarea name="mensajeCancelar" id="mensajeCancelar" class="form form-control" cols="30" rows="3" required="true"></textarea>
			</div>
			<div class="col-md-12">
				<label for=""> Mensaje de texto (SMS) Maximo {{$totalSMS}}</label>
				<span class="label label-danger" id="totalSmsCancel"></span>
				<textarea rows="5" max="{{$totalSMS}}" name="mensajesmscancel" id="mensajesmscancel" class="form form-control">{{(strip_tags($smscancel))}}</textarea>
			</div>
			<div class="col-md-12">
				<label for="">Enviar correo electronico</label>
				<textarea id="enviarCorreo2" COLS="10" name="mensajeemail" class="form form-control">{{$emailcancel}}</textarea>
			</div>
			</div>
			</div>
			<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar ventana</button>
			<button class="btn btn-danger" id="cancelButtonSubmit">Cancelar cita</button>
			</div>

		</div>
	</div>
	</div>
</form>
<!-- CANCELANDO CITA-->

<script>
CKEDITOR.replace('enviarCorreo', {height: 650, language: 'es'});
CKEDITOR.replace('enviarCorreo2', {height: 650, language: 'es'});
function countChars(idx,idxTotal,idxbutton)
{
	console.log({idx:idx,idxTotal:idxTotal})
	console.log({
		max:$(idx).attr('max'),
		val:$(idx).val(),
		//$(idx).val($(idx).val().substr(0,$(idx).attr('max')))
	});
	$(idx).val($(idx).val().substr(0,$(idx).attr('max')));
		$(idxTotal).show();
		var total = ($(idx).attr('max'))-$(idx).val().length;
		if(total<0)
		{
			total=' A superado el limite m&#225;ximo de caracteres permitidos por '+(total*-1)+' caracteres';
			$(idxTotal).addClass('label-danger');
			$(idxTotal).removeClass('label-primary');
		}
		else
		{
			$(idxTotal).removeClass('label-danger');
			$(idxTotal).addClass('label-primary');
		}
		$(idx).val($(idx).val().substr(0,$(idx).attr('max')));
		//VALIDANDO CARACTERES 
		var caracteres={!!$smsCaracteresValidos!!};
		var text=$(idx).val();
		function in_array( valid, val ) 
		{
			var invalid=[];
			var retorno = true;
			for(var i =0 ;i<val.length;i++)
			{
				if($.inArray(val[i],valid)===-1)
				{
					retorno = false;
					if($.inArray(val[i],invalid)===-1)
					{
						invalid.push(val[i]);
					}
				}
			}
			return {validate:retorno,car:invalid}; 
		}
		var siValido=in_array(caracteres,text);
		$(idxTotal).html(total+(!siValido.validate?'. Elimine los caracteres '+siValido.car.join(','):''));
		if(!siValido.validate)
		{
			$(idxTotal).addClass('label-danger');
			$(idxTotal).removeClass('label-primary');
		}

		if($(idxTotal).hasClass("label-danger"))
		{
			$(idxbutton).attr("disabled",true);
		}
		else{
			$(idxbutton).removeAttr("disabled");
		}
}
function loademail()
{
	$.ajax({
		url:base+'/format/email',
		type:'POST',
		dataType:'json',
		data:
		{
			id: {{ $id}},
			hora:$('#hora').val(),
			medico:$('#especialistas').val(),
			fecha:$('#fecha').val()
		},
		success:function(response)
		{
			if(response.validate)
			{
				var editor = CKEDITOR.instances['enviarCorreo'];
				editor.setData(response.data);
			}
		}
	})
}
function loadsms()
{
	$.ajax({
		url:base+'/format/sms',
		type:'POST',
		dataType:'json',
		data:
		{
			id: {{ $id}},
			hora:$('#hora').val(),
			medico:$('#especialistas').val(),
			fecha:$('#fecha').val()
		},
		success:function(response)
		{
			if(response.validate)
			{
				$('#mensajesms').val(response.data)
			}
			countChars('#mensajesms','#totalSms')
		}
	})
}
function changeMensaje()
{
	loademail();
	loadsms();
}
$(function()
{
	countChars('#mensajesms','#totalSms','#btn_guardar');
	countChars('#mensajesmscancel','#totalSmsCancel','#cancelButtonSubmit')
	$('.mensajesChange').change(function(){
		changeMensaje()
	});
	$('#reloadsms').click(function(){
		loadsms()
	})
	$('#reloademail').click(function(){
		loademail();
	})
	$('#mensajesms').keyup(function(){
		countChars('#mensajesms','#totalSms','#btn_guardar');
	});
	$('#mensajesmscancel').keyup(function(){
		countChars('#mensajesmscancel','#totalSmsCancel','#cancelButtonSubmit');
	});
	
    $("#especialistas").autocomplete({
        source: base+"/citas/especialistas",
        select: function (e, data)
        {
			$('#id_especialistas').val(data.item.id);
			changeMensaje();        
        }
    });
});
</script>
@endsection