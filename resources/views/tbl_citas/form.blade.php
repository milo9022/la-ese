<script>
$(function()
{
    $('#name').autocomplete({
        source: base+"/clientes/find",
        minLength: 2,
        select: function( event, ui ) 
        {
            $('#id_cliente').val(ui.item.id);
            $('#documento').html('Documento No: <strong>'+ui.item.documento+'</strong>');
            console.log(ui.item.documento)
        }
    });
})
</script>
<div class="form-group {{ $errors->has('fecha') ? 'has-error' : '' }}">
    <label for="fecha" class="col-md-2 control-label">Fecha</label>
    <div class="col-md-10">
        <input class="form-control fecha" name="fecha" type="text" id="fecha" value="{{ old('fecha', optional($tblCitas)->fecha) }}">
        {!! $errors->first('fecha', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('hora') ? 'has-error' : '' }}">
    <label for="hora" class="col-md-2 control-label">Hora</label>
    <div class="col-md-10">
        <input class="form-control time" name="hora" type="text" id="hora" value="{{ old('hora', optional($tblCitas)->hora) }}">
        {!! $errors->first('hora', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_consulta') ? 'has-error' : '' }}">
    <label for="id_consulta" class="col-md-2 control-label">Consulta</label>
    <div class="col-md-10">
        <select class="form-control" id="id_consulta" name="id_consulta" required="true">
        	    <option value="" style="display: none;" {{ old('id_consulta', optional($tblCitas)->id_consulta ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese el motivo de la consulta</option>
        	@foreach ($TblConsultas as $key => $TblConsultum)
			    <option value="{{ $key }}" {{ old('id_consulta', optional($tblCitas)->id_consulta) == $key ? 'selected' : '' }}>
			    	{{ $TblConsultum }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_consulta', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_cliente') ? 'has-error' : '' }}">
    <label for="id_cliente" class="col-md-2 control-label">Cliente</label>
    <div class="col-md-10">
    <input type="text" id="name" name="name" class="form form-control" value="{{old('nombre', optional($tblCitas)->nombre)}}" {{ (old('id_cliente', optional($tblCitas)->id_cliente)>0&&trim(old('nombre', optional($tblCitas)->nombre)!=''))?'readonly="readonly"':''}}>
    <span id="documento"></span>
		<input type="hidden"  id="id_cliente" name="id_cliente" value="{{ old('id_cliente', optional($tblCitas)->id_cliente)}}">            
        {!! $errors->first('id_cliente', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_citas_estado') ? 'has-error' : '' }}">
    <label for="id_citas_estado" class="col-md-2 control-label">Estado de la cita</label>
    <div class="col-md-10">
        <select class="form-control" id="id_citas_estado" name="id_citas_estado" required="true">
        	    <option value="" style="display: none;" {{ old('id_citas_estado', optional($tblCitas)->id_citas_estado ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese el estado de la cita</option>
        	@foreach ($TblCitasEstados as $key => $TblCitasEstado)
			    <option value="{{ $key }}" {{ old('id_citas_estado', optional($tblCitas)->id_citas_estado) == $key ? 'selected' : '' }}>
			    	{{ $TblCitasEstado }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_citas_estado', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('id_punto_atencion') ? 'has-error' : '' }}">
    <label for="id_punto_atencion" class="col-md-2 control-label">Punto de atenci&#243n</label>
    <div class="col-md-10">
        <select class="form-control" id="id_punto_atencion" name="id_punto_atencion" required="true">
        	    <option value="" style="display: none;" {{ old('id_punto_atencion', optional($tblCitas)->id_punto_atencion ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese el punto de atenci&#243n</option>
        	@foreach ($TblPuntosAtenciones as $key => $TblPuntosAtencion)
			    <option value="{{ $key }}" {{ old('id_punto_atencion', optional($tblCitas)->id_punto_atencion) == $key ? 'selected' : '' }}>
			    	{{ $TblPuntosAtencion }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_citas_estado', '<p class="help-block">:message</p>') !!}
    </div>
</div>

