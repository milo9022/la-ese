@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Tbl Especialistas' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tbl_especialistas.tbl_especialistas.destroy', $tblEspecialistas->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tbl_especialistas.tbl_especialistas.index') }}" class="btn btn-primary" title="Show All Tbl Especialistas">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tbl_especialistas.tbl_especialistas.create') }}" class="btn btn-success" title="Create New Tbl Especialistas">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tbl_especialistas.tbl_especialistas.edit', $tblEspecialistas->id ) }}" class="btn btn-primary" title="Edit Tbl Especialistas">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Tbl Especialistas" onclick="return confirm(&quot;Click Ok to delete Tbl Especialistas.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre Primero</dt>
            <dd>{{ $tblEspecialistas->nombre_primero }}</dd>
            <dt>Nombre Segundo</dt>
            <dd>{{ $tblEspecialistas->nombre_segundo }}</dd>
            <dt>Apellido Primero</dt>
            <dd>{{ $tblEspecialistas->apellido_primero }}</dd>
            <dt>Apellido Segundo</dt>
            <dd>{{ $tblEspecialistas->apellido_segundo }}</dd>
            <dt>Fecha Nacimiento</dt>
            <dd>{{ $tblEspecialistas->fecha_nacimiento }}</dd>
            <dt>Documento</dt>
            <dd>{{ $tblEspecialistas->documento }}</dd>
            <dt>Id Documento Tipo</dt>
            <dd>{{ optional($tblEspecialistas->TblDocumentoTipo)->nombre }}</dd>
            <dt>Celular1</dt>
            <dd>{{ $tblEspecialistas->celular1 }}</dd>
            <dt>Celular2</dt>
            <dd>{{ $tblEspecialistas->celular2 }}</dd>
            <dt>Email</dt>
            <dd>{{ $tblEspecialistas->email }}</dd>
            <dt>Created At</dt>
            <dd>{{ $tblEspecialistas->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $tblEspecialistas->updated_at }}</dd>
            <dt>Codigo</dt>
            <dd>{{ $tblEspecialistas->codigo }}</dd>
            <dt>Id Especialidad</dt>
            <dd>{{ optional($tblEspecialistas->TblEspecialidad)->id }}</dd>

        </dl>

    </div>
</div>

@endsection