
<div class="form-group {{ $errors->has('nombre_primero') ? 'has-error' : '' }}">
    <label for="nombre_primero" class="col-md-2 control-label">Nombre Primero</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre_primero" type="text" id="nombre_primero" value="{{ old('nombre_primero', optional($tblEspecialistas)->nombre_primero) }}" minlength="1" maxlength="191" required="true" "Enter nombre primero here...">
        {!! $errors->first('nombre_primero', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('nombre_segundo') ? 'has-error' : '' }}">
    <label for="nombre_segundo" class="col-md-2 control-label">Nombre Segundo</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre_segundo" type="text" id="nombre_segundo" value="{{ old('nombre_segundo', optional($tblEspecialistas)->nombre_segundo) }}" maxlength="191" "Enter nombre segundo here...">
        {!! $errors->first('nombre_segundo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellido_primero') ? 'has-error' : '' }}">
    <label for="apellido_primero" class="col-md-2 control-label">Apellido Primero</label>
    <div class="col-md-10">
        <input class="form-control" name="apellido_primero" type="text" id="apellido_primero" value="{{ old('apellido_primero', optional($tblEspecialistas)->apellido_primero) }}" minlength="1" maxlength="191" required="true" "Enter apellido primero here...">
        {!! $errors->first('apellido_primero', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellido_segundo') ? 'has-error' : '' }}">
    <label for="apellido_segundo" class="col-md-2 control-label">Apellido Segundo</label>
    <div class="col-md-10">
        <input class="form-control" name="apellido_segundo" type="text" id="apellido_segundo" value="{{ old('apellido_segundo', optional($tblEspecialistas)->apellido_segundo) }}" maxlength="191" "Enter apellido segundo here...">
        {!! $errors->first('apellido_segundo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fecha_nacimiento') ? 'has-error' : '' }}">
    <label for="fecha_nacimiento" class="col-md-2 control-label">Fecha Nacimiento</label>
    <div class="col-md-10">
        <input class="form-control" name="fecha_nacimiento" type="text" id="fecha_nacimiento" value="{{ old('fecha_nacimiento', optional($tblEspecialistas)->fecha_nacimiento) }}" "Enter fecha nacimiento here...">
        {!! $errors->first('fecha_nacimiento', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('documento') ? 'has-error' : '' }}">
    <label for="documento" class="col-md-2 control-label">Documento</label>
    <div class="col-md-10">
        <input class="form-control" name="documento" type="text" id="documento" value="{{ old('documento', optional($tblEspecialistas)->documento) }}" minlength="1" maxlength="191" required="true" "Enter documento here...">
        {!! $errors->first('documento', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_documento_tipo') ? 'has-error' : '' }}">
    <label for="id_documento_tipo" class="col-md-2 control-label">Documento Tipo</label>
    <div class="col-md-10">
        <select class="form-control" id="id_documento_tipo" name="id_documento_tipo">
        	    <option value="" style="display: none;" {{ old('id_documento_tipo', optional($tblEspecialistas)->id_documento_tipo ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese el tipo de documento</option>
        	@foreach ($TblDocumentoTipos as $key => $TblDocumentoTipo)
			    <option value="{{ $key }}" {{ old('id_documento_tipo', optional($tblEspecialistas)->id_documento_tipo) == $key ? 'selected' : '' }}>
			    	{{ $TblDocumentoTipo }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_documento_tipo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('celular1') ? 'has-error' : '' }}">
    <label for="celular1" class="col-md-2 control-label">Celular1</label>
    <div class="col-md-10">
        <input class="form-control" name="celular1" type="text" id="celular1" value="{{ old('celular1', optional($tblEspecialistas)->celular1) }}" maxlength="191" "Enter celular1 here...">
        {!! $errors->first('celular1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('celular2') ? 'has-error' : '' }}">
    <label for="celular2" class="col-md-2 control-label">Celular2</label>
    <div class="col-md-10">
        <input class="form-control" name="celular2" type="text" id="celular2" value="{{ old('celular2', optional($tblEspecialistas)->celular2) }}" maxlength="191" "Enter celular2 here...">
        {!! $errors->first('celular2', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">Email</label>
    <div class="col-md-10">
        <input class="form-control" name="email" type="text" id="email" value="{{ old('email', optional($tblEspecialistas)->email) }}" maxlength="191" "Enter email here...">
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('codigo') ? 'has-error' : '' }}">
    <label for="codigo" class="col-md-2 control-label">Codigo</label>
    <div class="col-md-10">
        <input class="form-control" name="codigo" type="text" id="codigo" value="{{ old('codigo', optional($tblEspecialistas)->codigo) }}" maxlength="191" "Enter codigo here...">
        {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_especialidad') ? 'has-error' : '' }}">
    <label for="id_especialidad" class="col-md-2 control-label">Especialidad</label>
    <div class="col-md-10">
        <select class="form-control" id="id_especialidad" name="id_especialidad" required>
        	    <option value="" style="display: none;" {{ old('id_especialidad', optional($tblEspecialistas)->id_especialidad ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese la especialidad</option>
        	@foreach ($TblEspecialidads as $key => $TblEspecialidad)
			    <option value="{{ $key }}" {{ old('id_especialidad', optional($tblEspecialistas)->id_especialidad) == $key ? 'selected' : '' }}>
			    	{{ $TblEspecialidad }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_especialidad', '<p class="help-block">:message</p>') !!}
    </div>
</div>

