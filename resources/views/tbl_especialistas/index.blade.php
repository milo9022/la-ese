@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Especialistas</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_especialistas.tbl_especialistas.create') }}" class="btn btn-success" title="Create New Tbl Especialistas">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($tblEspecialistasObjects) == 0)
            <div class="panel-body text-center">
                <h4>No se encontraron especialistas.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table id="table" class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Especialista</th>
                            <th>Documento</th>
                            <th>Celular</th>
                            <th>Email</th>
                            <th>Codigo</th>
                            <th>Especialidad</th>

                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tblEspecialistasObjects as $tblEspecialistas)
                        <tr>
                            <td>
                                {{ $tblEspecialistas->nombre_primero }}
                                {{ $tblEspecialistas->nombre_segundo }}
                                {{ $tblEspecialistas->apellido_primero }}
                                {{ $tblEspecialistas->apellido_segundo }}
                            </td>
                            <td>{{ optional($tblEspecialistas->TblDocumentoTipo)->nombre }} {{ $tblEspecialistas->documento }}</td>
                            <td>{{ $tblEspecialistas->celular1 }} {{ $tblEspecialistas->celular2 }}</td>
                            <td>{{ $tblEspecialistas->email }}</td>
                            <td>{{ $tblEspecialistas->codigo }}</td>
                            <td>{{ optional($tblEspecialistas->TblEspecialidad)->nombre }}</td>

                            <td>

                                <form method="POST" action="{!! route('tbl_especialistas.tbl_especialistas.destroy', $tblEspecialistas->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">

                                        <a href="{{ route('tbl_especialistas.tbl_especialistas.edit', $tblEspecialistas->id ) }}" class="btn btn-primary" title="Editar Especialistas">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Eliminar un Especialista" onclick="return confirm(&quot;Desea borrar el especialista.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">

        </div>
        
        @endif
    
    </div>
@endsection