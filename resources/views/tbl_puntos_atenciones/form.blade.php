<script src="{{url('/js/departamentos.js')}}"></script>
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
    <label for="nombre" class="col-md-2 control-label">Nombre</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ old('nombre', optional($tblPuntosAtencione)->nombre) }}" minlength="1" maxlength="200" required="true" "Enter nombre here...">
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_departamento') ? 'has-error' : '' }}">
    <label for="id_departamento" class="col-md-2 control-label">Departamento</label>
    <div class="col-md-10">
        <select class="form-control" id="id_departamento" name="id_departamento" required="true">
        	    <option value="" style="display: none;" {{ (!isset($tblPuntosAtencione->TblMunicipio->id_departamento) ?: '') == '' ? 'selected' : '' }} disabled selected>Seleccione el departamento</option>
        	@foreach ($TblDepartamentos as $key => $TblDepartamento)
			    <option value="{{ $key }}" {{ (isset($tblPuntosAtencione->TblMunicipio->id_departamento)?$tblPuntosAtencione->TblMunicipio->id_departamento:'') == $key ? 'selected' : '' }}>
			    	{{ $TblDepartamento }}
			    </option>
			@endforeach
        </select>
        {!! $errors->first('id_departamento', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_municipio') ? 'has-error' : '' }}">
    <label for="id_municipio" class="col-md-2 control-label">Municipio</label>
    <div class="col-md-10">
      <select class="form-control" id="id_municipio" name="id_municipio" required="true">
        	    <option value="" style="display: none;" {{ old('id_municipio', optional($tblPuntosAtencione)->id_municipio ?: '') == '' ? 'selected' : '' }} disabled selected>Primero seleccione un departamento</option>
        	@foreach ($TblMunicipios as $key => $TblMunicipio)
			    <option value="{{ $key }}" {{ old('id_municipio', optional($tblPuntosAtencione)->id_municipio) == $key ? 'selected' : '' }}>
			    	{{ $TblMunicipio }}
			    </option>
			@endforeach
        </select>
        {!! $errors->first('id_municipio', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="container-fluid">
    <div class="col-md-12">
        <div class="panel panel-primary">
        <div class="panel-heading">
        Servicios prestados en el punto de atencion
        </div>
        <div class="panel-body">
            <div class="form-group {{ $errors->has('id_municipio') ? 'has-error' : '' }}">
                @foreach($tblConsultas as $key => $tblConsulta)
                <div class="col-md-3">
                {{$tblConsulta}}    
                <table width="100%">
                        <tr>
                            <td>
                                <label for="servicios_{{ $key }}">
                                    <div class="onoffswitch">
                                        <input type="checkbox" name="servicios[{{ $key }}]" class="onoffswitch-checkbox" value="{{$key}}" id="servicios_{{ $key }}" {{isset($TblPuntosAtencionesXConsuta)?(in_array($key,$TblPuntosAtencionesXConsuta)?'checked':''):''}}>
                                        <label class="onoffswitch-label" for="servicios_{{ $key }}">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </label>
                            </td>
                            <td>
                            <input class="form form-control cantidad" type="number" min="0" id="cantidad_{{ $key }}" name="serviciosCantidad[{{ $key }}]" value="{{isset($TblPuntosAtencionesXConsutaMaximo[$key])?$TblPuntosAtencionesXConsutaMaximo[$key]:''}}"  {{!isset($TblPuntosAtencionesXConsutaMaximo[$key])?'readonly="readonly"':''}}>
                            </td>
                        </tr>
                    </table>
                </div>
                @endforeach
            </div>
        </div>
        </div>
</div>
<script>
function validateCheck()
{
    $('.cantidad').each(function(index,value)
    {
        if($(value).attr('readonly')=== undefined)
        {
            $(value).attr('required','required');
        }
        else{
            $(value).removeAttr('required');
        }
    })
}
$(function()
{
    $('.onoffswitch-checkbox').change(function() 
    {
        $('#cantidad_'+($(this).attr('id')).replace("servicios_", "")).attr('readonly', !($(this).is(":checked"))).val(''); 
        validateCheck();
    });
    validateCheck();
})
</script>