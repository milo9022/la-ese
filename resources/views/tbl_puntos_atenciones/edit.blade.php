@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >{{ !empty($title) ? $title : 'Punto de atenci&#243n' }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('tbl_puntos_atenciones.tbl_puntos_atencione.index') }}" class="btn btn-primary" title="Show All Tbl Puntos Atencione">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('tbl_puntos_atenciones.tbl_puntos_atencione.create') }}" class="btn btn-success" title="Crear  Puntos Atencione">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('tbl_puntos_atenciones.tbl_puntos_atencione.update', $tblPuntosAtencione->id) }}" id="edit_tbl_puntos_atencione_form" name="edit_tbl_puntos_atencione_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('tbl_puntos_atenciones.form', [
                                        'tblPuntosAtencione' => $tblPuntosAtencione,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-save"></i> Actualizar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection