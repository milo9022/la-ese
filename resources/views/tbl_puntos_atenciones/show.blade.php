@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 >{{ isset($title) ? $title : 'Tbl Puntos Atencione' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tbl_puntos_atenciones.tbl_puntos_atencione.destroy', $tblPuntosAtencione->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tbl_puntos_atenciones.tbl_puntos_atencione.index') }}" class="btn btn-primary" title="Show All Tbl Puntos Atencione">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tbl_puntos_atenciones.tbl_puntos_atencione.create') }}" class="btn btn-success" title="Crear  Puntos Atencione">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tbl_puntos_atenciones.tbl_puntos_atencione.edit', $tblPuntosAtencione->id ) }}" class="btn btn-primary" title="Edit Tbl Puntos Atencione">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Tbl Puntos Atencione" onclick="return confirm(&quot;Click Ok to delete Tbl Puntos Atencione.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre</dt>
            <dd>{{ $tblPuntosAtencione->nombre }}</dd>
            <dt>Id Municipio</dt>
            <dd>{{ optional($tblPuntosAtencione->TblMunicipio)->nombre }}</dd>

        </dl>

    </div>
</div>

@endsection