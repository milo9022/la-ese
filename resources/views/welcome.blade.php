<html lang="es">
	<head>
		<meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="viewport" content="maximum-scale=1.0,width=device-width,initial-scale=1.0,user-scalable=0">
		<title>Registre su cita medica</title>
		<script>var base="{{url('')}}";</script>


		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css">
		<link rel="stylesheet" href="{{url('/css/core.css')}}">
		<link rel="stylesheet" href="{{url('/css/images.0a827509b19560815767.css')}}" type="text/css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.css">

	<body>

		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/locale/es-us.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
        <style>
		.board-name{
			width:100% !important;
		}
		</style>
		<app></app>
        <script src="{{ mix('js/app.js') }}?v=<?= date('ymdhis');?>"></script>
    </body>
</html>