@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Dias No Habiles</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.create') }}" class="btn btn-success" title="Create New Tbl Dias No Habile">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($tblDiasNoHabiles) == 0)
            <div class="panel-body text-center">
                <h4>No Dias No Habiles no encontrados.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Fecha</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tblDiasNoHabiles as $tblDiasNoHabile)
                        <tr>
                            <td>{{ $tblDiasNoHabile->fecha }}</td>

                            <td>

                                <form method="POST" action="{!! route('tbl_dias_no_habiles.tbl_dias_no_habile.destroy', $tblDiasNoHabile->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.edit', $tblDiasNoHabile->id ) }}" class="btn btn-primary" title="Edit Tbl Dias No Habile">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Tbl Dias No Habile" onclick="return confirm(&quot;Click Ok to delete Tbl Dias No Habile.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $tblDiasNoHabiles->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection