@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">{{ !empty($title) ? $title : ' Dias No Habiles' }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.index') }}" class="btn btn-primary" title="Show All Tbl Dias No Habile">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.create') }}" class="btn btn-success" title="Create New Tbl Dias No Habile">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.update', $tblDiasNoHabile->id) }}" id="edit_tbl_dias_no_habile_form" name="edit_tbl_dias_no_habile_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('tbl_dias_no_habiles.form', [
                                        'tblDiasNoHabile' => $tblDiasNoHabile,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="actualizar">
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection