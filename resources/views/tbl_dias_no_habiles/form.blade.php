
<div class="form-group {{ $errors->has('fecha') ? 'has-error' : '' }}">
    <label for="fecha" class="col-md-2 control-label">Fecha</label>
    <div class="col-md-10">
        <input class="form-control fecha" name="fecha" type="text" id="fecha" value="{{ old('fecha', optional($tblDiasNoHabile)->fecha) }}" required="true" placeholder="ingrese la fecha">
        {!! $errors->first('fecha', '<p class="help-block">:message</p>') !!}
    </div>
</div>

