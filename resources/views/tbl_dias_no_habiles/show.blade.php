@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Tbl Dias No Habile' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tbl_dias_no_habiles.tbl_dias_no_habile.destroy', $tblDiasNoHabile->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.index') }}" class="btn btn-primary" title="Show All Tbl Dias No Habile">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.create') }}" class="btn btn-success" title="Create New Tbl Dias No Habile">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.edit', $tblDiasNoHabile->id ) }}" class="btn btn-primary" title="Edit Tbl Dias No Habile">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Tbl Dias No Habile" onclick="return confirm(&quot;Click Ok to delete Tbl Dias No Habile.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Fecha</dt>
            <dd>{{ $tblDiasNoHabile->fecha }}</dd>
            <dt>Created At</dt>
            <dd>{{ $tblDiasNoHabile->created_at }}</dd>
            <dt>Updated At</dt>
            <dd>{{ $tblDiasNoHabile->updated_at }}</dd>

        </dl>

    </div>
</div>

@endsection