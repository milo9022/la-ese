@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 class="mt-5 mb-5">Ingrese un dia</h4>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.index') }}" class="btn btn-primary" title="Show All Tbl Dias No Habile">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('tbl_dias_no_habiles.tbl_dias_no_habile.store') }}" accept-charset="UTF-8" id="create_tbl_dias_no_habile_form" name="create_tbl_dias_no_habile_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('tbl_dias_no_habiles.form', [
                                        'tblDiasNoHabile' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <input class="btn btn-primary" type="submit" value="Guardar">
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


