@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >{{ !empty($title) ? $title : 'Citas Estados' }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('tbl_citas_estados.tbl_citas_estados.index') }}" class="btn btn-primary" title="Show All Citas Estados">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('tbl_citas_estados.tbl_citas_estados.create') }}" class="btn btn-success" title="Crear  Citas Estados">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('tbl_citas_estados.tbl_citas_estados.update', $tblCitasEstados->id) }}" id="edit_tbl_citas_estados_form" name="edit_tbl_citas_estados_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('tbl_citas_estados.form', [
                                        'tblCitasEstados' => $tblCitasEstados,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <button class="btn btn-primary" type="submit">Actualizar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection