
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
    <label for="nombre" class="col-md-2 control-label">Nombre</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ old('nombre', optional($tblCitasEstados)->nombre) }}" maxlength="200" "Enter nombre here...">
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('orden') ? 'has-error' : '' }}">
    <label for="orden" class="col-md-2 control-label">Orden</label>
    <div class="col-md-10">
        <input class="form-control" name="orden" type="number" id="orden" value="{{ old('orden', optional($tblCitasEstados)->orden) }}" min="-2147483648" max="2147483647" "Enter orden here...">
        {!! $errors->first('orden', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('color') ? 'has-error' : '' }}">
    <label for="color" class="col-md-2 control-label">Color</label>
    <div class="col-md-10">
        <input class="form-control" name="color" type="text" id="color" value="{{ old('color', optional($tblCitasEstados)->color) }}" maxlength="20" "Enter color here...">
        {!! $errors->first('color', '<p class="help-block">:message</p>') !!}
    </div>
</div>

