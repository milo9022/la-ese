@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >Citas Estados</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_citas_estados.tbl_citas_estados.create') }}" class="btn btn-success" title="Crear  Citas Estados">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($tblCitasEstadosObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Citas Estados Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table id="table" class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Orden</th>
                            <th>Color</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tblCitasEstadosObjects as $tblCitasEstados)
                        <tr>
                            <td>{{ $tblCitasEstados->nombre }}</td>
                            <td>{{ $tblCitasEstados->orden }}</td>
                            <td>{{ $tblCitasEstados->color }}</td>

                            <td>

                                <form method="POST" action="{!! route('tbl_citas_estados.tbl_citas_estados.destroy', $tblCitasEstados->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('tbl_citas_estados.tbl_citas_estados.show', $tblCitasEstados->id ) }}" class="btn btn-info" title="Show Citas Estados">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('tbl_citas_estados.tbl_citas_estados.edit', $tblCitasEstados->id ) }}" class="btn btn-primary" title="Edit Citas Estados">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Citas Estados" onclick="return confirm(&quot;Click Ok to delete Citas Estados.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $tblCitasEstadosObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection