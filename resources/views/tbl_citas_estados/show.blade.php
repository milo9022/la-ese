@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 >{{ isset($title) ? $title : 'Citas Estados' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tbl_citas_estados.tbl_citas_estados.destroy', $tblCitasEstados->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tbl_citas_estados.tbl_citas_estados.index') }}" class="btn btn-primary" title="Show All Citas Estados">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tbl_citas_estados.tbl_citas_estados.create') }}" class="btn btn-success" title="Crear  Citas Estados">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tbl_citas_estados.tbl_citas_estados.edit', $tblCitasEstados->id ) }}" class="btn btn-primary" title="Edit Citas Estados">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Citas Estados" onclick="return confirm(&quot;Click Ok to delete Citas Estados.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre</dt>
            <dd>{{ $tblCitasEstados->nombre }}</dd>
            <dt>Orden</dt>
            <dd>{{ $tblCitasEstados->orden }}</dd>
            <dt>Color</dt>
            <dd>{{ $tblCitasEstados->color }}</dd>

        </dl>

    </div>
</div>

@endsection