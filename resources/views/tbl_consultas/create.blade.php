@extends('layouts.app')

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading clearfix">
            
            <span class="pull-left">
                <h4 >Crear  Consultas</h4>
            </span>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_consultas.tbl_consultas.index') }}" class="btn btn-primary" title="Show All Tbl Consultas">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>
            </div>

        </div>

        <div class="panel-body">
        
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('tbl_consultas.tbl_consultas.store') }}" accept-charset="UTF-8" id="create_tbl_consultas_form" name="create_tbl_consultas_form" class="form-horizontal">
            {{ csrf_field() }}
            @include ('tbl_consultas.form', [
                                        'tblConsultas' => null,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <button class="btn btn-primary" type="submit">Guardar</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

@endsection


