
<div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
    <label for="nombre" class="col-md-2 control-label">Nombre</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre" type="text" id="nombre" value="{{ old('nombre', optional($tblConsultas)->nombre) }}" minlength="1" maxlength="200" required="true" "Ingrese el nombre">
        {!! $errors->first('nombre', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_consulta_tipo') ? 'has-error' : '' }}">
    <label for="id_consulta_tipo" class="col-md-2 control-label">Consulta Tipo</label>
    <div class="col-md-10">
        <select class="form-control" id="id_consulta_tipo" name="id_consulta_tipo" required="true">
        	    <option value="" style="display: none;" {{ old('id_consulta_tipo', optional($tblConsultas)->id_consulta_tipo ?: '') == '' ? 'selected' : '' }} disabled selected>Ingrese si es consulta o programa</option>
        	@foreach ($TblConsultasTipos as $key => $TblConsultasTipo)
			    <option value="{{ $key }}" {{ old('id_consulta_tipo', optional($tblConsultas)->id_consulta_tipo) == $key ? 'selected' : '' }}>
			    	{{ $TblConsultasTipo }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_consulta_tipo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

