@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >Consultas y programas</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_consultas.tbl_consultas.create') }}" class="btn btn-success" title="Crear  Consultas">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($tblConsultasObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Tbl Consultas Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table id="table" class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Opciones</th>
                            <th>Nombre</th>
                            <th>Tipo</th>

                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tblConsultasObjects as $tblConsultas)
                        <tr>
                        <td>
                                <div class="dropdown">
								<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								Opciones
								<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
									<li>
                                        <a href="{{ route('tbl_consultas.tbl_consultas.edit', $tblConsultas->id ) }}">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar
                                        </a>
									</li>
									<li>
                                        <form method="POST" action="{!! route('tbl_consultas.tbl_consultas.destroy', $tblConsultas->id) !!}" accept-charset="UTF-8">
                                            <input name="_method" value="DELETE" type="hidden">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn-delete" title="Delete Tbl Consultas" onclick="return confirm(&quot;Click Ok to delete Tbl Consultas.&quot;)">
                                                <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar
                                            </button>
                                        </form>
									</li>
								</ul>
							    </div>
                            </td>
                            <td>{{ $tblConsultas->nombre }}</td>
                            <td>{{ optional($tblConsultas->TblConsultasTipo)->nombre }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $tblConsultasObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection