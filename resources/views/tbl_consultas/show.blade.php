@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 >{{ isset($title) ? $title : 'Tbl Consultas' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tbl_consultas.tbl_consultas.destroy', $tblConsultas->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tbl_consultas.tbl_consultas.index') }}" class="btn btn-primary" title="Show All Tbl Consultas">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tbl_consultas.tbl_consultas.create') }}" class="btn btn-success" title="Crear  Consultas">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tbl_consultas.tbl_consultas.edit', $tblConsultas->id ) }}" class="btn btn-primary" title="Edit Tbl Consultas">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Tbl Consultas" onclick="return confirm(&quot;Click Ok to delete Tbl Consultas.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre</dt>
            <dd>{{ $tblConsultas->nombre }}</dd>
            <dt>Id Consulta Tipo</dt>
            <dd>{{ optional($tblConsultas->TblConsultasTipo)->id }}</dd>

        </dl>

    </div>
</div>

@endsection