@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >Tbl Consultas Tipos</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_consultas_tipos.tbl_consultas_tipos.create') }}" class="btn btn-success" title="Crear  Consultas Tipos">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($tblConsultasTiposObjects) == 0)
            <div class="panel-body text-center">
                <h4>No Tbl Consultas Tipos Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table id="table" class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Nombre</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($tblConsultasTiposObjects as $tblConsultasTipos)
                        <tr>
                            <td>{{ $tblConsultasTipos->nombre }}</td>

                            <td>

                                <form method="POST" action="{!! route('tbl_consultas_tipos.tbl_consultas_tipos.destroy', $tblConsultasTipos->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('tbl_consultas_tipos.tbl_consultas_tipos.show', $tblConsultasTipos->id ) }}" class="btn btn-info" title="Show Tbl Consultas Tipos">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('tbl_consultas_tipos.tbl_consultas_tipos.edit', $tblConsultasTipos->id ) }}" class="btn btn-primary" title="Edit Tbl Consultas Tipos">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Tbl Consultas Tipos" onclick="return confirm(&quot;Click Ok to delete Tbl Consultas Tipos.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $tblConsultasTiposObjects->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection