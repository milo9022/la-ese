@extends('layouts.app')
@section('content')
<div class="panel panel-default">

<div class="panel-heading clearfix">
    
    <span class="pull-left">
        <h4 >Desabilitar sistema</h4>
    </span>

    <div class="btn-group btn-group-sm pull-right" role="group">
        <a href="{{ route('tbl_citas.tbl_citas.index') }}" class="btn btn-primary" title="Show All Citas">
            <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
        </a>
    </div>

</div>

<div class="panel-body">

    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form method="POST" action="{{ route('DesabilitarController.DesabilitarController.store') }}" accept-charset="UTF-8" id="DesabilitarController_store" name="DesabilitarController_store" class="form-horizontal">
        <div class="col-md-12">
        @csrf
        <label>Desabilitar sistema</label>
        <select class="form-control" name="desabilitar" id="desabilitar">
        <option value="1" {{$estado==1?'selected':''}}>Si</option>
        <option value="0" {{$estado!=1?'selected':''}}>No</option>
        </select>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <button class="btn btn-primary" type="submit">Guardar</button>
            </div>
        </div>

    </form>

</div>
</div>
@endsection