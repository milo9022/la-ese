@extends('layouts.app')
@section('content')
<script>
$(function()
{
    $('#search').click(function()
    {
        $.ajax({
            url:base+'/informes/Verpuntos',
            type:'POST',
            dataType:'json',
            data:{
                fecha_inicio:$('#inicio').val(),
                fecha_fin:$('#fin').val(),
                id_puntos_atencion:$('#punto').val()
                },
                success:function(data)
                {
                    if(data.validate)
                    {
                        location.href=data.url;
                    }
                    else{
                        alert('Error, no se pudo descargar el archivo');
                    }
                    console.log(data);
                }
        })
    })
})
</script>
@if(Session::has('success_message'))
<div class="alert alert-success">
	<span class="glyphicon glyphicon-ok"></span>
	{!! session('success_message') !!}
	<button type="button" class="close" data-dismiss="alert" aria-label="close">
	<span aria-hidden="true">&times;</span>
	</button>
</div>
@endif
<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<div class="pull-left">
			<h4 >Informe de puntos de atenci&#243n</h4>
		</div>
	</div>
	<div class="panel-body panel-body-with-table">
		<div class="row">
        <div class="col-md-4">
            <label for="">Puntos de atencion</label>
            <select  name="punto" class=" form form-control" id="punto">
                @foreach($puntos as $punto)
                <option value="{{$punto->id}}">{{$punto->nombre}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-4">
            <label for="">Fecha de inicio</label>
            <input type="text" name="inicio" class="fecha form form-control" id="inicio">
        </div>
        
        
        <div class="col-md-4">
            <label for="">Fecha de fin</label>
            <input type="text" name="fin" class="fecha form form-control" id="fin">
        </div>
        <div class="col-md-4">
<br>
        <button class="btn btn-success" id="search"><i class="fas fa-search"></i> Buscar</button>    
		</div>
		</div>
		</select>
		<div>

		</div>
	</div>
</div>
@endsection