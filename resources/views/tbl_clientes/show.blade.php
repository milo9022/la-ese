@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4>{{ isset($title) ? $title : 'Clientes' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tbl_clientes.tbl_clientes.destroy', $tblClientes->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tbl_clientes.tbl_clientes.index') }}" class="btn btn-primary" title="Show All Tbl Clientes">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tbl_clientes.tbl_clientes.create') }}" class="btn btn-success" title="Create New Tbl Clientes">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tbl_clientes.tbl_clientes.edit', $tblClientes->id ) }}" class="btn btn-primary" title="Edit Tbl Clientes">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Tbl Clientes" onclick="return confirm(&quot;Click Ok to delete Tbl Clientes.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Nombre Primero</dt>
            <dd>{{ $tblClientes->nombre_primero }}</dd>
            <dt>Nombre Segundo</dt>
            <dd>{{ $tblClientes->nombre_segundo }}</dd>
            <dt>Apellido Primero</dt>
            <dd>{{ $tblClientes->apellido_primero }}</dd>
            <dt>Apellido Segundo</dt>
            <dd>{{ $tblClientes->apellido_segundo }}</dd>
            <dt>Fecha Nacimiento</dt>
            <dd>{{ $tblClientes->fecha_nacimiento }}</dd>
            <dt>Id Documento Tipo</dt>
            <dd>{{ optional($tblClientes->TblDocumentoTipo)->id }}</dd>
            <dt>Documento</dt>
            <dd>{{ $tblClientes->documento }}</dd>

        </dl>

    </div>
</div>

@endsection