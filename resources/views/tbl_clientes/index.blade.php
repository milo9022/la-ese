@extends('layouts.app')

@section('content')
<script>
var base="{{url('')}}";
function buscar()
{
    console.log('123')
	$('#tables').dataTableServerSide({
		url:base+'/clientes/All/',
		columns:[
				{ 
					data: 'id',
					"render": function(data,data2,row) 
					{
                        var html ='<div class="dropdown">'+
							'	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">'+
							'	Opciones'+
							'	<span class="caret"></span>'+
							'	</button>'+
							'	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">'+
							'		<li>'+
                            '        <a href="'+base+'/clientes/show/'+data+'">'+
                            '                <span class="glyphicon glyphicon-open" aria-hidden="true"></span> Ver detalles'+
                            '            </a>'+
                            '        </li>'+
							'		<li>'+
                            '            <a href="'+base+'/clientes/'+data+'/edit">'+
                            '                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar'+
                            '            </a>'+
                            '        </li>'+
							'		<li>'+
                            '            <form method="POST" action="'+base+'/clientes/tbl_clientes/'+data+'" accept-charset="UTF-8">'+
                            '            <input name="_method" value="DELETE" type="hidden">'+
                            '            <input type="hidden" name="_token" value="Ai4fYSu0pZ0nCGczpqZQcqIuF13ZYvJahMIB7a3P">'+
                            '                    <button type="submit" class="btn-delete" onclick="return confirm(&quot;Click Ok to delete Clientes.&quot;)">'+
                            '                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Borrar'+
                            '                    </button>'+
                            '            </form>'+
							'		</li>'+
							'	</ul>'+
							'</div>';
                            return html;
                    }
                    
                },
                {data:'nombre_primero','render':function(data, data2,row)
                    {
                        var html='';
                        html=html+(row.nombre_primero==null?'':row.nombre_primero+' ');
                        html=html+(row.nombre_segundo==null?'':row.nombre_segundo+' ');
                        return html;
                        
                    }
                },
                {
                    data:'apellido_primero','render':function(data,data2,row)
                    {
                        var html='';
                        html=html+(row.apellido_primero==null?'':row.apellido_primero+' ');
                        html=html+(row.apellido_segundo==null?'':row.apellido_segundo+' ');
                        return html;
                    }
                },
                {data:'fecha_nacimiento'},
                {data:'tbl_documento_tipos.nombre'},
                {data:'documento'},
            ]
	});
}
$(function()
{
	buscar();
	//$('#filtro').change(function()
	//{
	//	buscar();
	//});
});
</script>
    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4>Clientes</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('tbl_clientes.tbl_clientes.create') }}" class="btn btn-success" title="Create New Clientes">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($tblClientesObjects) == 0)
            <div class="panel-body text-center">
                <h4>No hay clientes registrados.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="">


            </div>
        </div>
        <table id="tables" class="table table-striped ">
            <thead>
                <tr>
                    <th>Opciones</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Fecha de nacimiento</th>
                    <th>Tipo de documento</th>
                    <th>Documento</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        
        @endif
    
    </div>
@endsection