@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
  
        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4>{{ !empty($title) ? $title : 'Clientes' }}</h4>
            </div>
            <div class="btn-group btn-group-sm pull-right" role="group">

                <a href="{{ route('tbl_clientes.tbl_clientes.index') }}" class="btn btn-primary" title="Show All Tbl Clientes">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>

                <a href="{{ route('tbl_clientes.tbl_clientes.create') }}" class="btn btn-success" title="Create New Tbl Clientes">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>

            </div>
        </div>

        <div class="panel-body">

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form method="POST" action="{{ route('tbl_clientes.tbl_clientes.update', $tblClientes->id) }}" id="edit_tbl_clientes_form" name="edit_tbl_clientes_form" accept-charset="UTF-8" class="form-horizontal">
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">
            @include ('tbl_clientes.form', [
                                        'tblClientes' => $tblClientes,
                                      ])

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-save"></i> Guardar</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

@endsection