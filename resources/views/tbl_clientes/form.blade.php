
<div class="form-group {{ $errors->has('nombre_primero') ? 'has-error' : '' }}">
    <label for="nombre_primero" class="col-md-2 control-label">Nombre Primero</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre_primero" type="text" id="nombre_primero" value="{{ old('nombre_primero', optional($tblClientes)->nombre_primero) }}" minlength="1" maxlength="200" required data-title="Enter nombre primero here...">
        {!! $errors->first('nombre_primero', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('nombre_segundo') ? 'has-error' : '' }}">
    <label for="nombre_segundo" class="col-md-2 control-label">Nombre Segundo</label>
    <div class="col-md-10">
        <input class="form-control" name="nombre_segundo" type="text" id="nombre_segundo" value="{{ old('nombre_segundo', optional($tblClientes)->nombre_segundo) }}" maxlength="200" data-title="Enter nombre segundo here...">
        {!! $errors->first('nombre_segundo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellido_primero') ? 'has-error' : '' }}">
    <label for="apellido_primero" class="col-md-2 control-label">Apellido Primero</label>
    <div class="col-md-10">
        <input class="form-control" name="apellido_primero" type="text" id="apellido_primero" value="{{ old('apellido_primero', optional($tblClientes)->apellido_primero) }}" minlength="1" maxlength="200" required data-title="Enter apellido primero here...">
        {!! $errors->first('apellido_primero', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('apellido_segundo') ? 'has-error' : '' }}">
    <label for="apellido_segundo" class="col-md-2 control-label">Apellido Segundo</label>
    <div class="col-md-10">
        <input class="form-control" name="apellido_segundo" type="text" id="apellido_segundo" value="{{ old('apellido_segundo', optional($tblClientes)->apellido_segundo) }}" maxlength="200" data-title="Enter apellido segundo here...">
        {!! $errors->first('apellido_segundo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('fecha_nacimiento') ? 'has-error' : '' }}">
    <label for="fecha_nacimiento" class="col-md-2 control-label">Fecha Nacimiento</label>
    <div class="col-md-10">
        <input class="form-control fecha" name="fecha_nacimiento" type="text" id="fecha_nacimiento" value="{{ old('fecha_nacimiento', optional($tblClientes)->fecha_nacimiento) }}" data-title="Enter fecha nacimiento here...">
        {!! $errors->first('fecha_nacimiento', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_documento_tipo') ? 'has-error' : '' }}">
    <label for="id_documento_tipo" class="col-md-2 control-label">Tipo de documento</label>
    <div class="col-md-10">
        <select class="form-control" id="id_documento_tipo" name="id_documento_tipo">
        	    <option value="" style="display: none;" {{ old('id_documento_tipo', optional($tblClientes)->id_documento_tipo ?: '') == '' ? 'selected' : '' }} disabled selected>Selecione el tipo de documento</option>
        	@foreach ($TblDocumentoTipos as $key => $TblDocumentoTipo)
			    <option value="{{ $key }}" {{ old('id_documento_tipo', optional($tblClientes)->id_documento_tipo) == $key ? 'selected' : '' }}>
			    	{{ $TblDocumentoTipo }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('id_documento_tipo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('documento') ? 'has-error' : '' }}">
    <label for="documento" class="col-md-2 control-label">Documento</label>
    <div class="col-md-10">
        <input class="form-control" name="documento" type="text" min="1" id="documento" value="{{ old('documento', optional($tblClientes)->documento) }}" minlength="1" maxlength="200" required data-title="Enter documento here...">
        {!! $errors->first('documento', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('id_eps') ? 'has-error' : '' }}">
    <label for="id_eps" class="col-md-2 control-label">EPS</label>
    <div class="col-md-10">


    <select class="form-control" id="id_eps" name="id_eps" required>
        	    <option value="" style="display: none;" {{ old('id_eps', optional($tblClientes)->id_eps ?: '') == '' ? 'selected' : '' }} disabled selected>Selecione la EPS</option>
        	@foreach ($TblEps as $key => $TblEp)
			    <option value="{{ $key }}" {{ old('id_eps', optional($tblClientes)->id_eps) == $key ? 'selected' : '' }}>
			    	{{ $TblEp }}
			    </option>
			@endforeach
</select>
        {!! $errors->first('id_eps', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('celular1') ? 'has-error' : '' }}">
    <label for="celular1" class="col-md-2 control-label">Celular 1</label>
    <div class="col-md-10">
        <input class="form-control" name="celular1" type="text" min="1" id="celular1" value="{{ old('celular1', optional($tblClientes)->celular1) }}" minlength="1" maxlength="200" required data-title="Enter celular1 here...">
        {!! $errors->first('celular1', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('celular2') ? 'has-error' : '' }}">
    <label for="celular2" class="col-md-2 control-label">Celular 2</label>
    <div class="col-md-10">
        <input class="form-control" name="celular2" type="text" min="1" id="celular2" value="{{ old('celular2', optional($tblClientes)->celular2) }}" minlength="1" maxlength="200" data-title="Enter celular2 here...">
        {!! $errors->first('celular2', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    <label for="email" class="col-md-2 control-label">Email</label>
    <div class="col-md-10">
        <input class="form-control" name="email" type="email" min="1" id="email" value="{{ old('email', optional($tblClientes)->email) }}" minlength="1" maxlength="200" data-title="Ingrese el email del paciente">
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

