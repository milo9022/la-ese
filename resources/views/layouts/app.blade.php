
<!DOCTYPE html>
<html>
<head>
<meta name="csrf-token" content="{{ csrf_token() }}">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="icon" href="{{url('/img/icono.png')}}">
  <title>Citas medicas</title>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="//fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/fontawesome.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" type="text/css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/froala-editor/3.0.0/css/froala_editor.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">    
    <link rel="stylesheet" href="{{asset('css/jquery-clockpicker.min.css')}}">
    <link rel="stylesheet" href="{{url('css/style2.css')}}">
    <link rel="stylesheet" href="{{url('css/skin.css')}}">
    
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/jquery-ui.min.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script src="{{asset('js/jquery.validate.min.js')}}"></script>
	  <script src="{{asset('js/jquery.validate.es.js')}}"></script>
    <script src="{{asset('js/jquery-clockpicker.min.js')}}"></script>
    <script src="////cdnjs.cloudflare.com/ajax/libs/froala-editor/3.0.0/js/froala_editor.min.js"></script>
    <script src="{{url('/js/functions.js')}}"></script>
    <script>var base="{{url('')}}";</script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		
    <!-- Styles -->
    <link href="//fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">





  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
@auth
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="{{url('')}}" class="logo">
      <span class="logo-lg"><b>Sistema</b> Citas medicas</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
          <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-shield"></i> 
          ({{isset(Auth::user()->roles[0]->description)?Auth::user()->roles[0]->description:''}}) 
          {{Auth::user()->nombre_primero}}
          {{Auth::user()->apellido_segundo}}
          </a>
          <ul class="dropdown-menu">
          @if(Auth::user()->hasRole('admin'))
            <li><a href="{{url('logs')}}"><i class="fas fa-exclamation-triangle"></i> Lote de errores</a></li>
            <li><a href="javascript:limpiar()"><i class="fas fa-broom"></i> Borrar cache</a></li>
            <li role="separator" class="divider"></li>
            <script>
            function limpiar()
            {
              $.ajax({
                url: base+'/limpiar',
                dataType:'json',
                success:function(data)
                {
                  alert('Registros limpios');
                }
              });
            }
            </script>
          @endif
          <li><a href="{{route('users.users.change')}}"><i class="fas fa-user-lock"></i>    Cambiar contraseña</a></li>
            <li><a href="{{ route('exit')}}"><i class="fas fa-sign-out-alt"></i> Cerrar sesi&#243n</a></li>
            
          </ul>
        </li>
          </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{url('img/icon.png').'?v='.date('YmdHis')}}" class="img-circle" alt="User Image">
        </div>

        <div class="pull-left info">
          <p>{{Auth::user()->nombre_primero}}
          {{Auth::user()->apellido_primero}}
          </p>
        </div>
      </div>
      <div class="user-panel">
        <div class="pull-left image">
        <form action="{{ route('tbl_citas.tbl_citas.buscar') }}" method="post" >
        <div class="input-group">
      {{ csrf_field() }}
      <input type="text" name="codigo" class="form-control" placeholder="Buscar cita" required>
      <span class="input-group-btn">
        <button class="btn btn-default" type="submit"><i class="fas fa-search"></i></button>
      </span>
        </form>
    </div><!-- /input-group -->
        </div>

      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      @if(Auth::user()->hasRole('admin') ||Auth::user()->hasRole('facturador') ||Auth::user()->hasRole('supervision'))
          <li class="{{ Request::is('citas') ? 'active' : '' }}"><a href="{{url('citas')}}">Citas</a></li>
        @endif
        @if(Auth::user()->hasRole('admin'))
          <li class="{{ Request::is('carga') ? 'active' : '' }}"><a href="{{url('carga')}}">Carga archivos</a></li>
        @endif
        @if(Auth::user()->hasRole('admin'))
          <li class="{{ Request::is('diasnohabiles') ? 'active' : '' }}"><a href="{{url('diasnohabiles')}}">Inhabilitar dias</a></li>
        @endif
        @if(Auth::user()->hasRole('admin'))
          <li class="{{ Request::is('configuracion/enviaremail') ? 'active' : '' }}"><a href="{{url('configuracion/enviaremail')}}">Mensaje defecto email</a></li>
        @endif
        
        @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('supervision'))
                <li class="{{ Request::is('desabilitar') ? 'active' : '' }}"><a href="{{url('desabilitar')}}">Desabilitar plataforma</a></li>
            @endif
        @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('supervision'))
                <li class="{{ Request::is('mensajes') ? 'active' : '' }}"><a href="{{url('mensajes')}}">Mensajes</a></li>
            @endif
            @if(Auth::user()->hasRole('admin'))
                <li class="{{ Request::is('puntos_atencion') ? 'active' : '' }}"><a href="{{url('puntos_atencion')}}">Puntos de atenci&#243n</a></li>
            @endif
            
            @if(Auth::user()->hasRole('admin')  )
                <li class="{{ Request::is('programas') ? 'active' : '' }}"><a href="{{url('programas')}}">Programas y consultas</a></li>
            @endif
            @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('facturador') ||Auth::user()->hasRole('supervision')   )
                <li class="{{ Request::is('clientes') ? 'active' : '' }}"><a href="{{url('clientes')}}">Clientes</a></li>
            @endif
            @if(Auth::user()->hasRole('admin'))
                <li class="{{ Request::is('usuarios') ? 'active' : '' }}"><a href="{{url('usuarios')}}">Usuarios</a></li>
            @endif
            @if(Auth::user()->hasRole('admin'))
            <li class="{{ Request::is('especialistas') ? 'active' : '' }}"><a href="{{url('especialistas')}}">Especialistas</a></li>
            @endif
            @if(Auth::user()->hasRole('admin')||Auth::user()->hasRole('supervision') )
          <li class="dropdown{{ Request::is('informes/puntos') ? ' active' : '' }}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-angle-down"></i> Informes
            </a>
            <ul class="dropdown-menu">
                <li class="{{ Request::is('informes/puntos') ? 'active' : '' }}"><a href="{{ route('informepuntos' )}}">Informe puntos de atencion</a></li>
             </ul>
          </li>
          @endif

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
    </section>

    <!-- Main content -->
    <section class="content">
    @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
    @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->

      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post" >
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
@endauth
</body>
</html>
