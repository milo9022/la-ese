@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 >Users</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('users.users.create') }}" class="btn btn-success" title="Create New Users">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($usersObjects) == 0)
            <div class="panel-body text-center">
                <h4>No hay usuarios en el sistema</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table id="table" class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Perfil </th>
                            <th>Nombres </th>
                            <th>Apellidos</th>
                            <th>Documento</th>
                            <th>Punto de Atencion asignado</th>
                            <th>Correo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($usersObjects as $users)
                        <tr>
                            <td>{{ $users->roles_description}}</td>
                            <td>{{ $users->nombre_primero }} {{ $users->nombre_segundo }} </td>
                            <td>{{ $users->apellido_primero }} {{ $users->apellido_segundo }}</td>
                            <td>{{ $users->documento }}</td>
                            <td>{{ $users->nombre_punto_atencion }}</td>
                            <td>{{ $users->email }}</td>
                            @if(Auth::user()->hasRole('admin'))
                            <td>
                                <form method="POST" action="{!! route('users.users.destroy', $users->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('users.users.edit', $users->id ) }}" class="btn btn-primary" title="Editar usuarios">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Borrar usuario" onclick="return confirm(&quot;Este usuario va a ser eliminado. Aceptar para continuar.&quot;)">
                                            <span class="fas fa-user-slash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                            </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        @endif
    
    </div>
@endsection