require('./bootstrap')

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);


import Registro from './view/registro/form'
import App from './view/template/registro'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/index/registro',
            name: 'registro',
            component: Registro,
            meta: { title: 'Registro' }
        }]
    });
const app = new Vue({
    el: 'app',
    components: { App },
    router,
});